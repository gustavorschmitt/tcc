package com.netprecision.sqlMaker.entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.netprecision.sqlMaker.annotations.Column;
import com.netprecision.sqlMaker.mapped.ColumnMapped;
import com.netprecision.sqlMaker.mapped.OrderMapped;
import com.netprecision.sqlMaker.mapped.Restricao;
import com.netprecision.sqlMaker.mapped.TableMapped;
import com.netprecision.sqlMaker.type.FunctionType;
import com.netprecision.sqlMaker.type.JoinType;
import com.netprecision.sqlMaker.type.SortOrder;
import com.netprecision.sqlMaker.utils.SqlMakerReflection;

public abstract class EntitySqlMaker<T extends EntitySqlMaker<?>> {
	public EntitySqlMaker() {
	}

	public abstract Long getCdId();

	public abstract void setCdId(Long cdId);

	@Override
	public int hashCode() {
		return getCdId().intValue();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || ((EntitySqlMaker<?>) obj).getCdId() == null
				|| getCdId() == null)
			return false;
		return getCdId().longValue() == ((EntitySqlMaker<?>) obj).getCdId()
				.longValue();
	}

	public final String getEntityName() {
		return this.getClass().getSimpleName().replace("Dto", "");
	}

	@Override
	public final String toString() {
		try {
			StringBuilder sb = new StringBuilder("EntityName: "
					+ getEntityName() + ";\n");

			for (Field field : this.getClass().getSuperclass()
					.getDeclaredFields()) {
				Column column = field.getAnnotation(Column.class);
				if (column != null)
					sb.append(field.getName())
							.append(": ")
							.append(SqlMakerReflection.get(this,
									field.getName())).append("; ");
			}

			return sb.toString();
		} catch (Exception e) {
			System.err.println("EntitySqlMaker.toString(Object): "
					+ getEntityName());
			e.printStackTrace();
			return null;
		}
	}

	/*************************************************************************************************************************************************************************************************/
	/** Atributos e metodos utilizados para montar a PAGINACAO da consutla SQL ***********************************************************************************************************************/
	/*************************************************************************************************************************************************************************************************/

	private Long nrRegistrosPagina;

	private Long nrPaginaAtual;

	public Long getNrRegistrosPagina() {
		return nrRegistrosPagina;
	}

	public void setNrRegistrosPagina(Long nrRegistrosPagina) {
		this.nrRegistrosPagina = nrRegistrosPagina;
	}

	public Long getNrPaginaAtual() {
		return nrPaginaAtual;
	}

	public void setNrPaginaAtual(Long nrPaginaAtual) {
		this.nrPaginaAtual = nrPaginaAtual;
	}

	/*************************************************************************************************************************************************************************************************/
	/** Atributos e metodos utilizados para montar a consulta SQL ************************************************************************************************************************************/
	/*************************************************************************************************************************************************************************************************/

	private List<TableMapped> tables = new ArrayList<TableMapped>();

	private List<ColumnMapped> columns = new ArrayList<ColumnMapped>();

	private List<Restricao> restricoes = new ArrayList<Restricao>();

	private List<OrderMapped> ordenacao = new ArrayList<OrderMapped>();

	private Boolean groupBy = false;

	public void limparRestricao() {
		restricoes = new ArrayList<Restricao>();
	}

	public void limparOrdenacao() {
		ordenacao = new ArrayList<OrderMapped>();
	}

	public void limparColunas() {
		tables = new ArrayList<TableMapped>();
		columns = new ArrayList<ColumnMapped>();
	}

	public TableMapped getTable(String pathEntity) {
		TableMapped table = new TableMapped();
		table.setPathEntity(pathEntity);
		int index = tables.indexOf(table);
		if (index > -1)
			return tables.get(index);

		table.setNmAlias("t" + tables.size());

		if (tables.size() == 0 || tables.size() == 1) {
			tables.add(table);
			return table;
		}

		for (int i = 1; i < tables.size(); i++) {
			TableMapped mapped = tables.get(i);

			int atual = org.springframework.util.StringUtils
					.countOccurrencesOf(mapped.getPathEntity(), ".");
			int novo = org.springframework.util.StringUtils.countOccurrencesOf(
					table.getPathEntity(), ".");

			if (atual > novo) {
				tables.add(i, table);
				return table;
			}
		}

		tables.add(table);
		return table;
	}

	public ColumnMapped getColumn(String pathAtributo) {
		ColumnMapped column = new ColumnMapped();
		column.setPathAtributo(pathAtributo);
		int index = columns.indexOf(column);
		if (index > 0)
			return columns.get(index);

		column.setNmAlias("c" + columns.size());
		columns.add(column);
		return column;
	}

	public ColumnMapped getColumnByAlias(String nmAlias) {
		for (ColumnMapped column : columns)
			if (column.getNmAlias().equals(nmAlias))
				return column;

		return null;
	}

	public OrderMapped getOrdem(String pathAtributo) {
		OrderMapped order = new OrderMapped();
		order.setPathAtributo(pathAtributo);
		int index = ordenacao.indexOf(order);
		if (index > 0)
			return ordenacao.get(index);

		order.setType(SortOrder.ASCENDING);
		ordenacao.add(order);
		return order;
	}

	public void setFields(String... arrayFields) {
		tables = new ArrayList<TableMapped>();
		columns = new ArrayList<ColumnMapped>();

		for (String value : arrayFields) {
			// TableMapped table = value.contains(".") ?
			// getTable(value.substring(0, value.lastIndexOf("."))) :
			// getTable("");
			String table = value.contains(".") ? getTable(
					value.substring(0, value.lastIndexOf("."))).getPathEntity()
					: "";

			if (value.contains("*")) {
				if (value.contains("."))
					preparaFieldsWildcard(this,
							value.substring(0, value.lastIndexOf(".")));
				else
					preparaFieldsWildcard(this, "");
			} else {
				ColumnMapped column = getColumn(value);
				column.setPathEntity(table);
			}
		}
	}

	public void addFields(String... arrayFields) {
		for (String value : arrayFields) {
			if (value.contains("*")) {
				if (value.contains("."))
					preparaFieldsWildcard(this,
							value.substring(0, value.lastIndexOf(".")));
				else
					preparaFieldsWildcard(this, "");
			} else {
				String table = value.contains(".") ? getTable(
						value.substring(0, value.lastIndexOf(".")))
						.getPathEntity() : "";
				ColumnMapped column = getColumn(value);
				column.setPathEntity(table);
			}
		}
	}

	private void preparaFieldsWildcard(Object obj, String path) {
		try {
			if (path != null && path.trim().length() > 0)
				obj = SqlMakerReflection.newInstance(obj, path);
		} catch (Exception e) {
			System.out.println("GenericBeanDao.preparaFieldsWildcard");
			e.printStackTrace();
		}

		path = path == null || path.trim().length() == 0 ? "" : path + ".";

		for (Field field : SqlMakerReflection.getClass(obj).getDeclaredFields()) {
			Column column = field.getAnnotation(Column.class);
			if (column == null)
				continue;

			addFields(path + column.atributo());
		}
	}

	public void addJoin(String nmEntityPath) {
		TableMapped table = getTable(nmEntityPath);
		table.setInner(JoinType.INNER);
	}

	public void addJoin(String nmEntityPath, JoinType type) {
		TableMapped table = getTable(nmEntityPath);
		table.setInner(type);
	}

	public void addJoin(String nmEntityPath, JoinType type, Restricao restricao) {
		TableMapped table = getTable(nmEntityPath);
		table.setInner(type);
		table.setRestricao(restricao);
	}

	public void removeJoin(String nmEntityPath) {
		tables.remove(getTable(nmEntityPath));
	}

	public void addOrdem(String pathAtributo) {
		getOrdem(pathAtributo);
	}

	public void addOrdem(String pathAtributo, SortOrder type) {
		OrderMapped order = getOrdem(pathAtributo);
		order.setType(type);
	}

	public void copyOrdem(List<OrderMapped> newOrdenacao) {
		ordenacao = new ArrayList<OrderMapped>();
		for (OrderMapped mapped : newOrdenacao)
			ordenacao.add(mapped);
	}

	public void addProjecaoArrayToString(String pathAtributo, String pathDestino) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setNmAlias(pathDestino);
		column.setFunctionType(FunctionType.ARRAY_TO_STRING);
		column.setPathEntity(table);
	}

	public void addProjecaoArrayToString(String pathAtributo1,
			String pathAtributo2, String pathDestino) {
		String table = pathAtributo1.contains(".") ? getTable(
				pathAtributo1.substring(0, pathAtributo1.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo1);
		column.setNmAlias(pathDestino);
		column.setFunctionType(FunctionType.ARRAY_TO_STRING_DISTINCT);
		column.setPathEntity(table);

		String tableAux = pathAtributo2.contains(".") ? getTable(
				pathAtributo2.substring(0, pathAtributo2.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped columnAux = getColumn(pathAtributo2);
		columnAux.setNmAlias(pathDestino);
		columnAux.setFunctionType(FunctionType.ARRAY_TO_STRING_DISTINCT);
		columnAux.setPathEntity(tableAux);

		column.setNmColunaAux(columnAux.getNmColuna());
		column.setPathAtributoAux(columnAux.getPathAtributo());
		column.setPathEntityAux(columnAux.getPathEntity());

		getColumns().remove(columnAux);
	}

	public void addProjecaoContar(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.COUNT);
		column.setPathEntity(table);
	}

	public void addProjecaoDistinto(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.DISTINCT);
		column.setPathEntity(table);
	}

	public void addProjecaoContarDistinto(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.COUNT_DISTINCT);
		column.setPathEntity(table);
	}

	public void addProjecaoMax(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.MAX);
		column.setPathEntity(table);
	}

	public void addProjecaoMin(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.MIN);
		column.setPathEntity(table);
	}

	public void addProjecaoSum(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.SUM);
		column.setPathEntity(table);
	}

	public void addProjecaoTruncarDataDia(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.DATE_TRUNC_DAY);
		column.setPathEntity(table);
	}

	public void addProjecaoTruncarDataMes(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.DATE_TRUNC_MONTH);
		column.setPathEntity(table);
	}

	public void addProjecaoTruncarDataAno(String pathAtributo) {
		String table = pathAtributo.contains(".") ? getTable(
				pathAtributo.substring(0, pathAtributo.lastIndexOf(".")))
				.getPathEntity() : "";
		ColumnMapped column = getColumn(pathAtributo);
		column.setFunctionType(FunctionType.DATE_TRUNC_YEAR);
		column.setPathEntity(table);
	}

	public void addProjecaoSQL(String sql, String alias) {
		ColumnMapped column = new ColumnMapped();
		column.setFunctionType(FunctionType.SQL);
		column.setNmColuna(sql);
		column.setNmAlias(alias);
		columns.add(column);
	}

	public void removeRestricao(String pathAtributo) {
		for (Restricao r : restricoes)
			if (r.getPathAtributo() != null
					&& r.getPathAtributo().equals(pathAtributo)) {
				restricoes.remove(r);
				break;
			}
	}

	public List<TableMapped> getTables() {
		return tables;
	}

	public void setTables(List<TableMapped> tables) {
		this.tables = tables;
	}

	public List<ColumnMapped> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnMapped> columns) {
		this.columns = columns;
	}

	public List<Restricao> getRestricoes() {
		return restricoes;
	}

	public void setRestricoes(List<Restricao> restricoes) {
		this.restricoes = restricoes;
	}

	public List<OrderMapped> getOrdenacao() {
		return ordenacao;
	}

	public void setOrdenacao(List<OrderMapped> ordenacao) {
		this.ordenacao = ordenacao;
	}

	public void addRestricao(Restricao restricao) {
		restricoes.add(restricao);
	}

	public Boolean getGroupBy() {
		return groupBy;
	}

	public void enableGroupBy() {
		this.groupBy = true;
	}

	public void disableGroupBy() {
		this.groupBy = false;
	}
}