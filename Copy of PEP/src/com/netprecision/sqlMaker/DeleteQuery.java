package com.netprecision.sqlMaker;

import com.netprecision.sqlMaker.entity.EntitySqlMaker;

public class DeleteQuery extends Query {
	public static String create(EntitySqlMaker<?> bean) throws Exception {
		montarTables(bean, false);
		montarColunas(bean, false);
		montarWhere(bean, bean, "");
		montarRestricoes(bean, false);

		StringBuilder query = new StringBuilder("DELETE ");

		FROM(query, bean, false);
		WHERE(query, bean);
		return query.toString();
	}
}