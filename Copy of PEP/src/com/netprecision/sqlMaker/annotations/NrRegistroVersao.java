package com.netprecision.sqlMaker.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface NrRegistroVersao {
	String coluna() default "nr_registro_versao";

	String alias() default "nrRegistroVersao";
}