package com.netprecision.sqlMaker;

import com.netprecision.sqlMaker.entity.EntitySqlMaker;
import com.netprecision.sqlMaker.mapped.ColumnMapped;
import com.netprecision.sqlMaker.mapped.OrderMapped;
import com.netprecision.sqlMaker.type.FunctionType;
import com.netprecision.sqlMaker.type.SortOrder;

public class SelectQuery extends Query {

	public static String createSelect(EntitySqlMaker<?> bean) throws Exception {
		return createSelect(bean, false);
	}

	public static String createSubQuery(EntitySqlMaker<?> bean)
			throws Exception {
		return createSelect(bean, true);
	}

	private static String createSelect(EntitySqlMaker<?> bean, boolean subQuery)
			throws Exception {
		StringBuilder query = new StringBuilder("SELECT ");
		montarTables(bean, subQuery);
		montarColunas(bean, subQuery);
		montarWhere(bean, bean, "");
		montarRestricoes(bean, subQuery);
		montarOrdenacao(bean, subQuery);

		SELECT_COLUMNS(query, bean);
		FROM(query, bean, subQuery);
		WHERE(query, bean);
		SELECT_GROUPBY(query, bean, subQuery);
		SELECT_ORDER(query, bean);
		SELECT_LIMIT_OFFSET(query, bean);
		return query.toString();
	}

	public static String createCount(EntitySqlMaker<?> bean) throws Exception {
		return createCount(bean, false);
	}

	public static String createCountSubQuery(EntitySqlMaker<?> bean)
			throws Exception {
		return createCount(bean, true);
	}

	public static String createCount(EntitySqlMaker<?> bean, boolean subQuery)
			throws Exception {
		StringBuilder query = new StringBuilder(
				"SELECT count(distinct(r.cd_id)) as c0 ");
		montarTables(bean, subQuery);
		montarWhere(bean, bean, "");
		montarRestricoes(bean, subQuery);
		montarOrdenacao(bean, subQuery);

		FROM(query, bean, subQuery);
		WHERE(query, bean);
		return query.toString();
	}

	private static void SELECT_COLUMNS(StringBuilder query,
			EntitySqlMaker<?> bean) {
		StringBuilder select = new StringBuilder();

		for (ColumnMapped column : bean.getColumns())
			if (column.getFunctionType() != FunctionType.NONE
					&& column.getFunctionType() != FunctionType.ARRAY_TO_STRING) {
				String temp = column.getNmColuna() + " as "
						+ column.getNmAlias();
				if (!select.toString().contains(temp))
					select.append(temp + ", ");
			}

		for (ColumnMapped column : bean.getColumns())
			if (column.getFunctionType() == FunctionType.SQL) {
				String temp = column.getNmColuna() + " as "
						+ column.getNmAlias();
				if (!select.toString().contains(temp))
					select.append(temp + ", ");
			}

		for (ColumnMapped column : bean.getColumns())
			if (column.getFunctionType() == FunctionType.ARRAY_TO_STRING) {
				String temp = column.getNmColuna() + " as "
						+ column.getNmAlias();
				if (!select.toString().contains(temp))
					select.append(temp + ", ");
			}

		for (ColumnMapped column : bean.getColumns())
			if (column.getFunctionType() == FunctionType.NONE)
				select.append(column.getNmColuna() + " as "
						+ column.getNmAlias() + ", ");

		query.append(select.substring(0, select.length() - 2) + " ");
	}

	private static void SELECT_GROUPBY(StringBuilder query,
			EntitySqlMaker<?> bean, boolean subQuery) {
		if (subQuery || !bean.getGroupBy())
			return;

		StringBuilder groupBy = new StringBuilder("GROUP BY ");
		for (ColumnMapped column : bean.getColumns())
			if (column.getFunctionType() == FunctionType.NONE)
				groupBy.append(column.getNmColuna() + ", ");

		if (groupBy.length() > 9)
			query.append(groupBy.substring(0, groupBy.length() - 2) + " ");
	}

	private static void SELECT_ORDER(StringBuilder query, EntitySqlMaker<?> bean) {
		if (bean.getOrdenacao() == null || bean.getOrdenacao().size() == 0)
			return;

		StringBuilder orderBy = new StringBuilder();
		orderBy.append("ORDER BY ");
		for (OrderMapped order : bean.getOrdenacao())
			orderBy.append(order.getNmColuna()
					+ " "
					+ (order.getType() == SortOrder.DESCENDING ? "DESC" : "ASC")
					+ ", ");

		query.append(orderBy.substring(0, orderBy.length() - 2) + " ");
	}

	private static void SELECT_LIMIT_OFFSET(StringBuilder query,
			EntitySqlMaker<?> bean) {
		StringBuilder limitOffset = new StringBuilder("");
		if (bean.getNrRegistrosPagina() != null
				&& bean.getNrRegistrosPagina() > 0)
			limitOffset.append("LIMIT " + bean.getNrRegistrosPagina() + " ");

		if (bean.getNrPaginaAtual() != null && bean.getNrPaginaAtual() > 0)
			limitOffset.append("OFFSET " + bean.getNrPaginaAtual());

		query.append(limitOffset);
	}
}