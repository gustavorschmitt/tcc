package com.netprecision.sqlMaker.mapped;

import com.netprecision.sqlMaker.type.SortOrder;

public class OrderMapped {
	private String nmColuna;

	private String pathAtributo;

	private SortOrder type;

	public OrderMapped() {
	}

	public String getNmColuna() {
		return nmColuna;
	}

	public void setNmColuna(String nmColuna) {
		this.nmColuna = nmColuna;
	}

	public String getPathAtributo() {
		return pathAtributo;
	}

	public void setPathAtributo(String pathAtributo) {
		this.pathAtributo = pathAtributo;
	}

	public SortOrder getType() {
		return type;
	}

	public void setType(SortOrder type) {
		this.type = type;
	}

	public String getPathEntity() {
		if (pathAtributo == null)
			return "";
		if (pathAtributo.contains("."))
			return pathAtributo.substring(0, pathAtributo.lastIndexOf("."));
		return "";
	}

	public boolean equals(Object obj) {
		if (obj != null
				&& this.pathAtributo.equals(((OrderMapped) obj)
						.getPathAtributo()))
			return true;
		return false;
	}

	public static OrderMapped newOrder(String atributo, SortOrder type) {
		OrderMapped restricao = new OrderMapped();
		restricao.setPathAtributo(atributo);
		restricao.setType(type);
		return restricao;
	}

	public static OrderMapped asc(String atributo) {
		return newOrder(atributo, SortOrder.ASCENDING);
	}

	public static OrderMapped desc(String atributo) {
		return newOrder(atributo, SortOrder.DESCENDING);
	}
}