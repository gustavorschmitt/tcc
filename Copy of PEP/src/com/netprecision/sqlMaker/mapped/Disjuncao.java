package com.netprecision.sqlMaker.mapped;

import java.util.ArrayList;
import java.util.List;

public class Disjuncao {
	private List<Restricao> list = new ArrayList<Restricao>();

	public List<Restricao> getList() {
		return list;
	}

	public void setList(List<Restricao> list) {
		this.list = list;
	}

	public void add(Restricao restricao) {
		list.add(restricao);
	}
}