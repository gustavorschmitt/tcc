package com.netprecision.sqlMaker;

import java.lang.reflect.Field;
import java.util.List;

import com.netprecision.sqlMaker.annotations.Column;
import com.netprecision.sqlMaker.entity.EntitySqlMaker;
import com.netprecision.sqlMaker.utils.SqlMakerReflection;

public class InsertQuery extends Query {
	public static String create(EntitySqlMaker<?> bean) throws Exception {
		StringBuilder query = new StringBuilder();
		Class<?> classBean = SqlMakerReflection.getClass(bean);

		INSERT_INTO(query, classBean);
		List<Column> listColumns = getListColumns(classBean);
		INSERT_SET(query, listColumns);
		INSERT_VALUES(query, listColumns, bean, classBean);
		return query.toString();
	}

	private static void INSERT_INTO(StringBuilder query, Class<?> classBean) {
		query.append("INSERT INTO " + getTable(classBean) + " ");
	}

	private static void INSERT_SET(StringBuilder query, List<Column> listColumns) {
		query.append("(" + getColumns(listColumns) + ") ");
	}

	private static void INSERT_VALUES(StringBuilder query,
			List<Column> listColumns, Object bean, Class<?> classBean)
			throws Exception {
		StringBuilder values = new StringBuilder();
		for (Column column : listColumns) {
			Field field = SqlMakerReflection.getField(bean, column.atributo());
			values.append(getFormatedValueToQuery(field.getType()
					.getSimpleName().toUpperCase(),
					SqlMakerReflection.get(bean, column.atributo()))
					+ ",");
		}
		query.append("VALUES (" + values.substring(0, values.length() - 1)
				+ ") ");
	}
}