package com.netprecision.sqlMaker.type;

public enum SortOrder {
	ASCENDING, DESCENDING
}
