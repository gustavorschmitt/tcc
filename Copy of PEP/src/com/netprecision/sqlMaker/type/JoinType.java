package com.netprecision.sqlMaker.type;

public enum JoinType {
	INNER, LEFT, RIGHT, FULL
}
