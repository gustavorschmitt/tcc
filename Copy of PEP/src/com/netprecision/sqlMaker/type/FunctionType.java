package com.netprecision.sqlMaker.type;

public enum FunctionType {
	SQL, // coluna especifica
	ARRAY_TO_STRING, ARRAY_TO_STRING_DISTINCT, // transforma um array de valores
												// em um string
	NONE, // nenhum tipo definido
	COUNT, // contar
	DISTINCT, // distinct
	COUNT_DISTINCT, // contar os distintos
	MAX, MIN, // maximo e minimo
	SUM, // somatoria
	DATE_TRUNC_DAY, DATE_TRUNC_MONTH, DATE_TRUNC_YEAR // 'truncador' de data,
														// dever� passar o que
														// ser� truncado 'dia'
														// ou 'mes' ou 'ano'
}