package com.netprecision.sqlMaker;

import java.lang.reflect.Field;
import java.util.List;

import com.netprecision.sqlMaker.annotations.Column;
import com.netprecision.sqlMaker.entity.EntitySqlMaker;
import com.netprecision.sqlMaker.utils.SqlMakerReflection;

public class UpdateQuery extends Query {
	public static String create(EntitySqlMaker<?> bean) throws Exception {
		StringBuilder query = new StringBuilder();
		Class<?> classBean = SqlMakerReflection.getClass(bean);

		UPDATE_FROM(query, classBean);
		List<Column> listColumns = getListColumns(classBean);
		UPDATE_SET(query, listColumns, bean);
		UPDATE_WHERE(query, bean);
		return query.toString();
	}

	private static void UPDATE_FROM(StringBuilder query, Class<?> classBean) {
		query.append("UPDATE " + getTable(classBean) + " SET ");
	}

	private static void UPDATE_SET(StringBuilder query,
			List<Column> listColumns, EntitySqlMaker<?> bean) throws Exception {
		StringBuilder values = new StringBuilder();
		for (Column column : listColumns)
			if (column.updatable() && !column.atributo().equals("cdId")) {
				Field field = SqlMakerReflection.getField(bean,
						column.atributo());
				values.append(column.coluna()
						+ " = "
						+ getFormatedValueToQuery(field.getType()
								.getSimpleName().toUpperCase(),
								SqlMakerReflection.get(bean, column.atributo()))
						+ ",");
			}

		query.append(values.substring(0, values.length() - 1) + " ");
	}

	private static void UPDATE_WHERE(StringBuilder query, EntitySqlMaker<?> bean)
			throws Exception {
		query.append("WHERE 1=1 ");

		Long cdId = SqlMakerReflection.getLong(bean, "cdId");
		if (cdId != null)
			query.append(" and cd_id = " + cdId + " ");

		// for(Restricao restricao : bean.getRestricoes())
		// query.append("and " + restricao.getRestricaoSql() + " ");
	}
}