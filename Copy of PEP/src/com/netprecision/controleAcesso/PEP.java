package com.netprecision.controleAcesso;

import java.util.HashMap;

import com.netprecision.RESTful.utils.FAKE_SESSION;

public class PEP {

	public static boolean temPermissaoVisualizarDocumento(String nmUsuario, String nmDocumento) throws Exception {

		String request = criarRequisicao(nmUsuario, nmDocumento, "Visualizar");

		HashMap<String, String> hashMap = new HashMap<String, String>();
		String requestByte = "";
		byte[] bytes = request.getBytes();
		for (int i = 0; i < bytes.length; i++) {
			byte b = bytes[i];
			requestByte += b + ";";
		}

		hashMap.put("pepRequest", requestByte);

		String response = ConexaoHTTP.sendGet(FAKE_SESSION.URL_PDP + "/avaliar", hashMap);
		return response.equals("PERMIT");
	}

	public static String criarRequisicao(String nmUsuario, String nmDocumento, String nmPermissao) {
		String request = null;
		try {
			request = ConexaoHTTP.sendGet(FAKE_SESSION.URL_PEP + "/js/IIA001Request.xml", null);
			request = substituir(request, new String[] { nmUsuario, nmDocumento, nmPermissao });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request;
	}

	public static String substituir(String msg, String... params) {
		if (params != null)
			for (int i = 0; i < params.length; i++)
				msg = msg.replace("{" + i + "}", params[i] != null ? params[i] : "");
		return msg;

	}
}
