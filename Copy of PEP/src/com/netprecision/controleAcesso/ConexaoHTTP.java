package com.netprecision.controleAcesso;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class ConexaoHTTP {
	private static final String USER_AGENT = "Mozilla/5.0";

	// HTTP GET request
	public static String sendGet(String url, Map<String, String> argumentos) throws Exception {
		StringBuilder urlCompleta = new StringBuilder(url);
		if (argumentos != null) {
			Iterator<Entry<String, String>> iterator = argumentos.entrySet().iterator();
			urlCompleta.append("?");
			while (iterator.hasNext()) {
				Entry<String, String> entidade = iterator.next();
				urlCompleta.append(entidade.getKey() + "=" + entidade.getValue() + "&");
			}
		}
		url = urlCompleta.toString().replace(" ", "%20");

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		return response.toString();
	}

}
