package com.netprecision.generic.utils;

import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.usuario.mbean.UsuarioMBInterface;

public class MBUtils {

	public static UsuarioMBInterface getUsuarioMB() {
		return (UsuarioMBInterface) CustomApplicationContextAware
				.getBean("usuarioMB");
	}

}