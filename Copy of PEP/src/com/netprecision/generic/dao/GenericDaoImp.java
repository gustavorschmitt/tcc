package com.netprecision.generic.dao;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.generic.entity.GenericBean;
import com.netprecision.sqlMaker.DeleteQuery;
import com.netprecision.sqlMaker.InsertQuery;
import com.netprecision.sqlMaker.SelectQuery;
import com.netprecision.sqlMaker.UpdateQuery;
import com.netprecision.sqlMaker.mapped.ColumnMapped;
import com.netprecision.sqlMaker.utils.SqlMakerReflection;

@Transactional(readOnly = false)
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public abstract class GenericDaoImp<D extends GenericBean<?>> implements
		GenericDaoInterface<D> {
	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public JdbcTemplate getJdbcTemplate() {
		return new JdbcTemplate(
				(DriverManagerDataSource) CustomApplicationContextAware
						.getBean("dataSource"));
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D salvar(D entity) throws Exception {
		final String query = InsertQuery.create(entity);
		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(
					Connection connection) throws SQLException {
				return connection.prepareStatement(query,
						new String[] { "cd_id" });
			}
		}, keyHolder);

		Long generatedId = new Long(keyHolder.getKey().longValue());
		entity.setCdId(generatedId);
		return entity;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public final void salvar(List<D> collection) throws Exception {
		if (collection == null)
			return;

		for (D dto : collection)
			salvar(dto);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D atualizar(D entity) throws Exception {
		getJdbcTemplate().execute(UpdateQuery.create(entity));
		return entity;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public final void atualizar(List<D> collection) throws Exception {
		if (collection == null)
			return;

		for (D dto : collection)
			atualizar(dto);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public boolean excluir(D entity) throws Exception {
		if (entity == null)
			return false;
		getJdbcTemplate().execute(DeleteQuery.create(entity));
		return true;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public final void excluir(List<D> collection) throws Exception {
		if (collection == null)
			return;

		for (D obj : collection)
			excluir(obj);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D recuperar(D entity) throws Exception {
		if (entity == null)
			return null;

		entity.setNrPaginaAtual(0L);
		entity.setNrRegistrosPagina(1L);
		List<D> lista = transformarResultado(entity,
				executarSqlSelect(SelectQuery.createSelect(entity)));
		return lista == null ? null : (lista.size() > 0 ? lista.get(0) : null);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public List<D> listar(D entity) throws Exception {
		List<D> list = transformarResultado(entity,
				executarSqlSelect(SelectQuery.createSelect(entity)));
		entity.limparColunas();
		entity.limparRestricao();
		return list;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public Long contarRegistros(D entity) throws Exception {
		return executarSqlSelectContar(SelectQuery.createCount(entity));
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final List<Map<String, Object>> executarSqlSelect(
			StringBuilder query) {
		return executarSqlSelect(query.toString());
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final List<Map<String, Object>> executarSqlSelect(String query) {
		System.out.println("Teste => " + query);
		return getJdbcTemplate().queryForList(query);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final Long executarSqlCountSelect(StringBuilder query) {
		return executarSqlSelectContar(query.toString());
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final Long executarSqlSelectContar(String query) {
		try {
			System.out.println("Teste => " + query);
			return getJdbcTemplate().queryForObject(query, Long.class);
		} catch (EmptyResultDataAccessException e) {
			return 0L;
		} catch (Exception e) {
			System.out.println(query);
			e.printStackTrace();
			return 0L;
		}
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final List<Long> executarSqlListaLong(StringBuilder query) {
		return executarSqlListaLong(query.toString());
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final List<Long> executarSqlListaLong(String query) {
		return getJdbcTemplate().queryForList(query, Long.class);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final void executarComando(StringBuilder query) {
		executarComando(query.toString());
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final void executarComando(String query) {
		getJdbcTemplate().execute(query);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected final List<D> transformarResultado(D entity,
			List<Map<String, Object>> resultList) throws Exception {
		List<D> returnList = new ArrayList<D>();

		for (Map<String, Object> map : resultList) {
			@SuppressWarnings("unchecked")
			D novo = (D) SqlMakerReflection.newInstance(entity);
			for (String key : map.keySet()) {
				ColumnMapped column = entity.getColumnByAlias(key);
				if (column == null) {
					try {
						SqlMakerReflection.set(novo, key, map.get(key));
					} catch (Exception e) {
						throw new Exception(
								"alias nao encontrado no mapeamento \n"
										+ e.getMessage());
					}
				} else {
					Object value = map.get(key);
					if (value instanceof String) {
						String stringValue = (String) value;
						stringValue = stringValue.replaceAll("&#39;", "'");
						stringValue = stringValue.replaceAll("&#34;", "\"");
						stringValue = stringValue.replaceAll("&#37;", "%");
						stringValue = stringValue.replaceAll("&#92;", "\\");
						value = stringValue;
					}
					if (column.getPathAtributo().contains("."))
						montarArvore(novo, column.getPathAtributo(), value);
					else
						SqlMakerReflection.set(novo, column.getPathAtributo(),
								value);
				}
			}
			returnList.add(novo);
		}
		return returnList;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	private final void montarArvore(Object entity, String path, Object value)
			throws Exception {
		// escritorio.nmEscritorio
		String nmNewEntity = path.substring(0, path.indexOf("."));
		nmNewEntity = nmNewEntity.substring(0, 1).toUpperCase()
				+ nmNewEntity.substring(1);
		path = path.substring(path.indexOf(".") + 1);

		try {
			// Obt�m o objeto que cont�m o atributo que ser� setado
			Object obj = SqlMakerReflection.get(entity, nmNewEntity);

			if (obj == null) {
				Method get = entity.getClass().getMethod("get" + nmNewEntity,
						new Class[] {});
				entity.getClass()
						.getMethod("set" + nmNewEntity,
								new Class[] { get.getReturnType() })
						.invoke(entity, get.getReturnType().newInstance());
				obj = SqlMakerReflection.get(entity, nmNewEntity);
			}

			if (path.contains("."))
				montarArvore(obj, path, value);
			else
				SqlMakerReflection.set(obj, path, value);
		} catch (Exception e) {
			throw e;
		}
	}
}