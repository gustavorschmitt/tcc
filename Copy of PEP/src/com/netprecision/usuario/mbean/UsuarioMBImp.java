package com.netprecision.usuario.mbean;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.generic.mbean.GenericMBImp;
import com.netprecision.usuario.dao.UsuarioDaoInterface;
import com.netprecision.usuario.entity.dto.UsuarioDto;

@Service("usuarioMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class UsuarioMBImp extends GenericMBImp<UsuarioDto> implements
		UsuarioMBInterface {
	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 *
	 * @return UsuarioDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Override
	public UsuarioDaoInterface getDaoBean() {
		return (UsuarioDaoInterface) CustomApplicationContextAware
				.getBean("usuarioDao");
	}

	/**
	 * valida se o email � �nico na tabela de usuario
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=622641">NKC-93-R5</a>
	 * 
	 * @param dsEmail
	 *            para ser usado na consulta
	 * 
	 * @return se for unico retorna true, caso contrario false
	 */

}