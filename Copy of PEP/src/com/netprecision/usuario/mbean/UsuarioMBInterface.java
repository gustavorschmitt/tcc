package com.netprecision.usuario.mbean;

import com.netprecision.generic.mbean.GenericMBInterface;
import com.netprecision.usuario.entity.dto.UsuarioDto;

public interface UsuarioMBInterface extends GenericMBInterface<UsuarioDto> {

}