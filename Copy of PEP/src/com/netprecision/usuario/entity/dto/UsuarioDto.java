package com.netprecision.usuario.entity.dto;

import java.io.Serializable;

import com.netprecision.usuario.entity.Usuario;

public class UsuarioDto extends Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Atributos auxiliares
	 * *****************************************************
	 * *********************
	 * *****************************************************
	 * *****************************************
	 */

	private String dsSenhaConfirmacao;

	/*
	 * Construtores
	 * *************************************************************
	 * *************
	 * *************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public UsuarioDto() {
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public UsuarioDto(Long cdId) {
		setCdId(cdId);
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public UsuarioDto(String dsEmail, Integer flSituacao) {
		setDsEmail(dsEmail);
		setFlSituacao(flSituacao);
	}

	/*
	 * Factories
	 * ****************************************************************
	 * **********
	 * ****************************************************************
	 * *****************************************
	 */

	/*
	 * Metodos auxiliares para VISUALIZAR no JSP
	 * ********************************
	 * ******************************************
	 * *************************************************************************
	 */
	/*
	 * Metodos auxiliares para APLICAR SEGURANCA no JSP
	 * *************************
	 * *************************************************
	 * ******************************************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=622641">NKC-93-R2</a>
	 */
	public String getDsSenhaConfirmacao() {
		return dsSenhaConfirmacao;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=622641">NKC-93-R2</a>
	 */
	public void setDsSenhaConfirmacao(String dsSenhaConfirmacao) {
		this.dsSenhaConfirmacao = dsSenhaConfirmacao;
	}

}