package com.netprecision.usuario.dao;

import com.netprecision.generic.dao.GenericDaoInterface;
import com.netprecision.usuario.entity.dto.UsuarioDto;

public interface UsuarioDaoInterface extends GenericDaoInterface<UsuarioDto> {
}