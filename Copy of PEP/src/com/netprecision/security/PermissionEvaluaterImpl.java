package com.netprecision.security;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component(value = "permissionEvaluaterImpl")
public class PermissionEvaluaterImpl implements PermissionEvaluator {

	@Override
	public boolean hasPermission(Authentication arg0, Object arg1, Object arg2) {
		if (arg2 instanceof HttpServletRequest) {
			HttpServletRequest request = (HttpServletRequest) arg2;
		}
		return false;
	}

	@Override
	public boolean hasPermission(Authentication arg0, Serializable arg1,
			String arg2, Object arg3) {
		return false;
	}

	public boolean hasPermission(Authentication arg0, Object arg1, Object arg2,
			Object arg3) {
		if (arg2 instanceof HttpServletRequest) {
			HttpServletRequest request = (HttpServletRequest) arg2;
		}
		return false;
	}

	public boolean hasPermission(Object arg3) {
		return false;
	}

	public boolean hasPermission(Object arg3, Object arg4) {
		return false;
	}

}
