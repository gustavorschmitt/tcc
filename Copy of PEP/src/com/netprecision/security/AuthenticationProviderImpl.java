package com.netprecision.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component(value = "authProviderImpl")
public class AuthenticationProviderImpl implements AuthenticationProvider {
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {

		// String user = (String) authentication.getPrincipal();
		// String password = (String) authentication.getCredentials();
		//
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		//
		// UsernamePasswordAuthenticationToken authenticationToken = null;
		//
		// authenticationToken = new UsernamePasswordAuthenticationToken(user,
		// password, authorities);
		//
		// return authenticationToken;
		//
		// UsuarioDto usuarioDto = new UsuarioDto();
		// usuarioDto.setDsEmail((String) authentication.getName());
		// UsuarioDto usuario = null;
		// try
		// {
		// usuario = MBUtils.getUsuarioMB().recuperar(usuarioDto);
		// }
		// catch(ViolacaoAcessoException e)
		// {
		// System.out.println("Login invalido");
		// e.printStackTrace();
		// }
		// if(usuario.getDsSenha().equals(CreateSha.SHA2((String)
		// authentication.getCredentials())))
		// {
		return new UsernamePasswordAuthenticationToken(
				authentication.getName(), authentication.getCredentials(),
				authorities);

		// throw new BadCredentialsException("Bad Credentials");
	}

	@Override
	public boolean supports(Class<?> authentication) {
		boolean supports = authentication
				.isAssignableFrom(UsernamePasswordAuthenticationToken.class);
		return supports;
	}
	// @Override
	// public Authentication authenticate(Authentication authentication)
	// throws AuthenticationException
	// {
	// System.out.println("CustomAuthenticationProvider");
	// String name = authentication.getName();
	// String password = authentication.getCredentials().toString();
	// System.out.println(name + "  " + password);
	//
	// // use the credentials to try to authenticate against the third party
	// system
	//
	// List<GrantedAuthority> grantedAuths = new ArrayList<>();
	// return new UsernamePasswordAuthenticationToken(name, password,
	// grantedAuths);
	//
	// }
	//
	// @Override
	// public boolean supports(Class<?> arg0)
	// {
	// return
	// (UsernamePasswordAuthenticationToken.class.isAssignableFrom(arg0));
	// }

}
