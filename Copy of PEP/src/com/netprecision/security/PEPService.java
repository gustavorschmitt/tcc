package com.netprecision.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netprecision.controleAcesso.PEP;

@RestController
public class PEPService {

	@RequestMapping(value = "/temPermissaoVisualizar")
	public String temPermissaoVisualizar(HttpSession session, HttpServletRequest request) throws Exception {

		return PEP.temPermissaoVisualizarDocumento(request.getParameter("nmUsuario"), request.getParameter("nmDocumento")) + "";

	}
}
