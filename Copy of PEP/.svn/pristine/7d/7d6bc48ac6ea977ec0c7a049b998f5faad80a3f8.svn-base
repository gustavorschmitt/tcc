package com.netprecision.arquivo.mbean;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.netprecision.RESTful.utils.FAKE_SESSION;
import com.netprecision.arquivo.dao.ArquivoComentarioDaoInterface;
import com.netprecision.arquivo.entity.dto.ArquivoComentarioDto;
import com.netprecision.arquivo.entity.dto.ArquivoDto;
import com.netprecision.arquivo.entity.dto.ArquivoTemporarioDto;
import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.generic.entity.Validator;
import com.netprecision.generic.mbean.GenericMBImp;
import com.netprecision.generic.utils.JSPUtils;
import com.netprecision.generic.utils.MBUtils;
import com.netprecision.historico.util.REGISTRADOR;
import com.netprecision.sqlMaker.type.SortOrder;

@Service("arquivoComentarioMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class ArquivoComentarioMBImp extends GenericMBImp<ArquivoComentarioDto> implements ArquivoComentarioMBInterface
{
    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R2</a>
     *
     * @return ArquivoComentarioDaoInterface instancia retirada do pool de objetos do Spring
     */
    @Override
    public ArquivoComentarioDaoInterface getDaoBean()
    {
        return (ArquivoComentarioDaoInterface) CustomApplicationContextAware.getBean("arquivoComentarioDao");
    }

    /**
     * Metodo que lista os arquivos do comentario
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R2</a>
     * 
     * @param fkComentario
     *            sera usado como filtro para montar a consulta
     * 
     * @return lista de arquivos encontrados
     */
    @Override
    public List<ArquivoComentarioDto> listarArquivoComentario(Long fkComentario)
        throws Exception
    {
        ArquivoComentarioDto avd = new ArquivoComentarioDto();
        avd.setFkComentario(fkComentario);
        avd.setFields("cdId", "arquivo.*");
        avd.addOrdem("arquivo.dtCriacao", SortOrder.DESCENDING);
        return super.listar(avd);
    }

    /**
     * Metodo que salva a referencia entre os arquivos anexados e a versao do documento.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R2</a>
     * 
     * @param fkComentario
     *           comentario a qual sera vinculado os arquivos que serao salvos
     * 
     * @param listArquivo
     *            lista de arquivos que serao salvos na estrutura temporaria
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public void salvar(HttpSession session, Long fkComentario, List<ArquivoDto> listArquivo)
        throws Exception
    {
        for(ArquivoDto arquivo : listArquivo)
        {
            ArquivoComentarioDto comentarioVersaoDocumento = new ArquivoComentarioDto();
            comentarioVersaoDocumento.setFkArquivo(arquivo.getCdId());
            comentarioVersaoDocumento.setFkComentario(fkComentario);
            MBUtils.getArquivoComentarioMB().salvar(comentarioVersaoDocumento);
            REGISTRADOR.anexarArquivoVersaoDocumento(session, fkComentario, comentarioVersaoDocumento.getCdId());
        }
    }

    /**
     * Metodo monta o caminho de onde o arquivo vai ficar dentro da estrutura de pastas do servidor
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R2</a>
     *
     * @param session
     *            sessao ativa do usuario logado
     * @param request
     *            requisicao que o usuario enviou para o servidor contendo os arquivos anexos
     * @param idGuia
     *            identificador da guia que o usuario esta usando (tras informacoes como por exemplo, fkAmbiente)
     * 
     * @return caminho no filesystem onde se encontra o arquivo
     */
    @Override
    public String montarCaminhoStringArquivoComentario(HttpSession session, MultipartHttpServletRequest request, String idGuia)
    {
        if(Validator.isValid(request.getParameter("fkComentario")))
            return this.montarCaminhoArquivoComentario(JSPUtils.getFkAmbienteDaGuia(request, idGuia), new Long(request.getParameter("fkDocumento")), new Long(request.getParameter("fkComentario")),
                    new Long(request.getParameter("fkVersaoDocumento")));
        else
            return MBUtils.getArquivoTemporarioMB().montarCaminhoArquivoTemporario(session, request.getParameter("idTempPath"));
    }

    /**
     * Metodo monta o caminho de onde o arquivo vai ficar dentro da estrutura de pastas do servidor
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R2</a>
     *
     * @param fkAmbiente
     *            ambiente a qual se refere o arquivo
     * @param fkDocumento
     *            documento a qual se refere o arquivo
     * @param fkComentario
     *            versao a qual se refere o arquivo
     * 
     * @return caminho no filesystem onde se encontra o arquivo
     */
    @Override
    public String montarCaminhoArquivoComentario(Long fkAmbiente, Long fkDocumento, Long fkComentario, Long fkVersaoDocumento)
    {
        String path = FAKE_SESSION.FILE_SYSTEM_PATH
            + "ambiente"
            + File.separatorChar
            + fkAmbiente
            + File.separatorChar
            + "documento"
            + File.separatorChar
            + fkDocumento
            + File.separatorChar
            + fkVersaoDocumento
            + File.separatorChar
            + fkComentario
            + File.separatorChar;

        File filePath = new File(path);
        filePath.mkdirs();
        filePath = null;
        return path;
    }

    /**
     * Metodo para mover os arquivos da estrutura temporaria para dentro da estrutura permanente <br />
     * Esse procedimento eh feito quando existem arquivos para um documento que ainda nao foi salvo no banco, <br />
     * entao quando o documento � salvo, os arquivos tem que sair da estrutura temporaria e ir para dentro da <br />
     * estrutura de pastas referente a versao do documento em questao
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R6</a>
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R5    </a>
     * 
     * @param fkDocumento
     *            sera usado para montar o caminho
     * 
     * @param fkComentario
     *            sera usado para montar o caminho
     * 
     * @param idTempPath
     *            identificador da guia aberta, para que o usuario possa usar 2 guias para cadastrar documentos e uma guia nao interfira nos arquivos anexos da outra
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void moverArquivoTemporarioParaPermanente(HttpSession session, Long fkDocumento, Long fkVersaoDocumento, Long fkComentario, String idTempPath)
        throws Exception
    {
        List<ArquivoTemporarioDto> listArquivoTemporario = MBUtils.getArquivoTemporarioMB().listar(JSPUtils.getFkUsuarioLogado(session), ArquivoTemporarioDto.TIPO_ARQUIVO_COMENTARIO, idTempPath);
        for(ArquivoTemporarioDto arquivoTemporario : listArquivoTemporario)
        {
            ArquivoComentarioDto arquivoComentario = new ArquivoComentarioDto();
            arquivoComentario.setFkArquivo(arquivoTemporario.getArquivo().getCdId());
            arquivoComentario.setFkComentario(fkComentario);
            super.salvar(arquivoComentario);
            REGISTRADOR.anexarArquivoComentario(session, fkVersaoDocumento, fkComentario, arquivoComentario.getCdId());

            File arquivoOrigem = new File(arquivoTemporario.getArquivo().getDsCaminho() + arquivoTemporario.getArquivo().getCdId().toString());
            File arquivoDestino = new File(montarCaminhoArquivoComentario(arquivoTemporario.getArquivo().getFkAmbiente(), fkDocumento, fkComentario, fkVersaoDocumento));

            boolean moveuArquivo = arquivoOrigem.renameTo(new File(arquivoDestino, arquivoTemporario.getArquivo().getCdId().toString()));
            if(moveuArquivo)
            {
                String path = montarCaminhoArquivoComentario(arquivoTemporario.getArquivo().getFkAmbiente(), fkDocumento, fkComentario, fkVersaoDocumento);
                ArquivoDto arquivo = new ArquivoDto();
                arquivo.setCdId(arquivoTemporario.getArquivo().getCdId());
                arquivo = MBUtils.getArquivoMB().recuperar(arquivo);
                arquivo.setDsCaminho(path.replace("\\", "\\\\"));
                MBUtils.getArquivoMB().atualizar(arquivo);
                MBUtils.getArquivoTemporarioMB().excluir(new ArquivoTemporarioDto(arquivoTemporario.getCdId()));
                new File(arquivoTemporario.getArquivo().getDsCaminho()).delete();
            }
            else
                throw new Exception("Nao foi possivel mover o arquivo");
        }
    }

}