package com.netprecision.generic.entity;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.netprecision.sqlMaker.annotations.Column;
import com.netprecision.sqlMaker.utils.SqlMakerReflection;

public class Validator
{
    /* CONSTANTES ********************************************************************************************************************************************************************************** */

    // TODO i18n verificar possibilidade de colocar o REGEX no banco
    public static final String REGEX_EMAIL    = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

    // TODO i18n verificar possibilidade de colocar o REGEX no banco
    public static final String REGEX_TELEFONE = "^(\\s*\\d+\\s*)*";

    /* Metodos auxiliares validacao do atributo **************************************************************************************************************************************************** */

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isNull(Object value)
    {
        return value == null;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isNotNull(Object value)
    {
        return value != null;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(GenericBean<?> entity, String nmField)
    {
        try
        {
            return isValid(SqlMakerReflection.get(entity, nmField));
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return false;
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Object object)
    {
        if(object instanceof String)
            return isValid((String) object);
        else if(object instanceof Long)
            return isValid((Long) object);
        else if(object instanceof Integer)
            return isValid((Integer) object);
        else if(object instanceof Short)
            return isValid((Short) object);
        else if(object instanceof Date)
            return isValid((Date) object);
        else if(object instanceof Double)
            return isValid((Double) object);
        else if(object instanceof Float)
            return isValid((Float) object);
        else if(object instanceof Boolean)
            return isValid((Boolean) object);
        else
            return false;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Collection<?> collection)
    {
        return collection != null && !collection.isEmpty();
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Integer value)
    {
        return !isNull(value) && value != 0;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Short value)
    {
        return !isNull(value) && value != 0;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Long value)
    {
        return !isNull((Object) value) && value.intValue() > 0;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Double value)
    {
        return !isNull(value) && value != 0.0d;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Float value)
    {
        return !isNull(value) && value != 0.0f;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Boolean value)
    {
        return !isNull(value);
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(String value)
    {
        return !isNull(value) && value.trim().length() > 0;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static boolean isValid(Date value)
    {
        return !isNull(value) && value.getTime() > 0l;
    }

    /* Metodos auxiliares validacao pela anotacao @column ****************************************************************************************************************************************** */

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static Set<MensagemSistema> validateByColumnAnnotation(GenericBean<?> entity)
    {
        Set<MensagemSistema> setSystemMessage = new HashSet<MensagemSistema>();
        for(Field field : entity.getClass().getDeclaredFields())
            setSystemMessage.addAll(validateByColumnAnnotation(entity, field));
        return setSystemMessage;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static Set<MensagemSistema> validateByColumnAnnotation(GenericBean<?> entity, Field field)
    {
        Set<MensagemSistema> setSystemMessage = new HashSet<MensagemSistema>();
        setSystemMessage.add(validateMinLength(entity, field, field.getAnnotation(Column.class)));
        setSystemMessage.add(validateMaxLength(entity, field, field.getAnnotation(Column.class)));
        setSystemMessage.add(validateMinValue(entity, field, field.getAnnotation(Column.class)));
        setSystemMessage.add(validateMaxValue(entity, field, field.getAnnotation(Column.class)));
        setSystemMessage.add(validateMask(entity, field, field.getAnnotation(Column.class)));
        setSystemMessage.remove(null);
        return setSystemMessage;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    protected static MensagemSistema validateMinLength(GenericBean<?> entity, Field field, Column column)
    {
        try
        {
            if(field.getType() != String.class)
                return null;

            removeWhiteSpace(entity, field.getName());
            String valor = (String) SqlMakerReflection.get(entity, field.getName());
            if(column.minLength() != 0 && isValid(valor) && valor.length() < column.minLength())
                return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMinimoCaracteres", "" + column.minLength());

            return null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return null;
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    protected static MensagemSistema validateMaxLength(GenericBean<?> entity, Field field, Column column)
    {
        try
        {
            if(field.getType() != String.class)
                return null;

            removeWhiteSpace(entity, field.getName());
            String valor = (String) SqlMakerReflection.get(entity, field.getName());
            if(column.maxLength() > 0 && isValid(valor) && valor.length() > column.maxLength())
                return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMaximoCaracteres", "" + column.maxLength());

            return null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return null;
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static MensagemSistema validateMaxValue(GenericBean<?> entity, Field field, Column column)
    {
        try
        {
            if(field.getType() == java.lang.Short.class)
            {
                Short valor = (Short) SqlMakerReflection.get(entity, field.getName());
                if(column.maxValue() > 0 && isValid(valor) && valor > column.maxValue())
                    return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMaximoValor");
            }
            else if(field.getType() == java.lang.Integer.class)
            {
                Integer valor = (Integer) SqlMakerReflection.get(entity, field.getName());
                if(column.maxValue() > 0 && isValid(valor) && valor > column.maxValue())
                    return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMaximoValor");
            }

            return null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return null;
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static MensagemSistema validateMinValue(GenericBean<?> entity, Field field, Column column)
    {
        try
        {
            if(field.getType() == java.lang.Short.class)
            {
                Short valor = (Short) SqlMakerReflection.get(entity, field.getName());
                if(column.minValue() != 0 && isValid(valor) && valor < column.minValue())
                    return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMinimoValor");
            }
            else if(field.getType() == java.lang.Integer.class)
            {
                Integer valor = (Integer) SqlMakerReflection.get(entity, field.getName());
                if(column.minValue() != 0 && isValid(valor) && valor < column.minValue())
                    return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMinimoValor");
            }

            return null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return null;
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static MensagemSistema validateMask(GenericBean<?> entity, Field field, Column column)
    {
        try
        {
            if(!isValid(column.mascara()) || !isValid(entity, field.getName()))
                return null;

            if(!isValidMask((String) SqlMakerReflection.get(entity, field.getName()), column.mascara()))
                return MensagemSistema.ErrorFactory(column.message(), "erro_validadorMascara");

            return null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return null;
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    private static boolean isValidMask(String value, String mask)
    {
        if(!isValid(value) || !isValid(mask))
            return false;

        String expression = mask;
        if(expression.equals("email"))
            expression = REGEX_EMAIL;
        else if(expression.equals("telefone"))
            expression = REGEX_TELEFONE;

        CharSequence inputStr = value;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        return matcher.matches();
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static MensagemSistema validateRequired(GenericBean<?> entity, String nmAtributo)
    {
        try
        {
            Field field = SqlMakerReflection.getField(entity, nmAtributo);
            return (!isValid(SqlMakerReflection.get(entity, nmAtributo))) ? MensagemSistema.ErrorFactory(field.getAnnotation(Column.class).message(), "erro_validadorObrigatorio") : null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            return null;
        }
    }

    /* metodos de VALIDACAO que gera ALTERACAO do valor ******************************************************************************************************************************************** */

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static void setNullIfInvalid(GenericBean<?> entity, String nmAtributo)
    {
        try
        {
            Object value = SqlMakerReflection.get(entity, nmAtributo);
            if(!isValid(value))
                SqlMakerReflection.set(entity, nmAtributo, null);
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
        }
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public static void removeWhiteSpace(GenericBean<?> entity, String nmAtributo)
    {
        try
        {
            String value = (String) SqlMakerReflection.get(entity, nmAtributo);
            value = isValid(value) ? value.trim() : null;
            SqlMakerReflection.set(entity, nmAtributo, value);
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
        }
    }
}