package com.netprecision.arquivo.entity.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.netprecision.arquivo.entity.dto.ArquivoDto;
import com.netprecision.generic.entity.Validator;

@JsonIgnoreProperties({ "bytes" })
public class ArquivoJSON extends ArquivoDto
{
    private static final long serialVersionUID = 1L;

    /* Atributos auxiliares ************************************************************************************************************************************************************************ */

    private final String      mensagem;

    private final Long        fkVersaoDocumento;

    /* Construtores ******************************************************************************************************************************************************************************** */

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R1</a>
     */
    public ArquivoJSON(String mensagem, ArquivoDto arquivo, Long fkVersaoDocumento)
    {
        super();
        this.mensagem = mensagem;
        super.converterDados(this, arquivo);
        this.fkVersaoDocumento = Validator.isValid(fkVersaoDocumento) ? fkVersaoDocumento : 0L;
    }

    /* Metodos auxiliares ************************************************************************************************************************************************************************** */

    /* Get's and Set's ***************************************************************************************************************************************************************************** */

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R1</a>
     */
    public String getMensagem()
    {
        return mensagem;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R1</a>
     */
    public Long getFkVersaoDocumento()
    {
        return fkVersaoDocumento;
    }
}