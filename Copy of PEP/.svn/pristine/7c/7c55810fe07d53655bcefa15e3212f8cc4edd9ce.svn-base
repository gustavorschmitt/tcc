package com.netprecision.historico.mbean;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.generic.mbean.GenericMBImp;
import com.netprecision.historico.dao.HistoricoAcaoDaoInterface;
import com.netprecision.historico.entity.dto.HistoricoAcaoDto;
import com.netprecision.sqlMaker.type.JoinType;
import com.netprecision.sqlMaker.type.SortOrder;

@Service("historicoAcaoMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class HistoricoAcaoMBImp extends GenericMBImp<HistoricoAcaoDto> implements HistoricoAcaoMBInterface
{
    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     *
     * @return HistoricoAcaoDaoInterface instancia retirada do pool de objetos do Spring
     */
    @Override
    public HistoricoAcaoDaoInterface getDaoBean()
    {
        return (HistoricoAcaoDaoInterface) CustomApplicationContextAware.getBean("historicoAcaoDao");
    }

    /**
     * Metodo que lista os historicos da acao referente a uma unica versao do documento.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R8</a>
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R5</a>
     *
     * @return List<HistoricoAcaoDto> lista contendo os historicos filtrados
     */
    @Override
    public List<HistoricoAcaoDto> listarPelaVersaoDocumento(Long fkVersaoDocumento)
        throws Exception
    {
        HistoricoAcaoDto dto = new HistoricoAcaoDto();
        dto.addFields("*", "usuario.nmUsuario", "acao.dsChave", "arquivoVersaoDocumento.arquivo.nmArquivo");
        dto.addJoin("arquivoVersaoDocumento", JoinType.LEFT);
        dto.addJoin("arquivoVersaoDocumento.arquivo", JoinType.LEFT);
        dto.setFkVersaoDocumento(fkVersaoDocumento);
        dto.addOrdem("dtHistorico", SortOrder.DESCENDING);
        return super.listar(dto);
    }
}