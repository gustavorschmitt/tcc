package com.netprecision.historico.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.netprecision.arquivo.entity.ArquivoComentario;
import com.netprecision.arquivo.entity.ArquivoVersaoDocumento;
import com.netprecision.generic.entity.GenericBean;
import com.netprecision.newKeepControl.documento.entity.Comentario;
import com.netprecision.newKeepControl.documento.entity.VersaoDocumento;
import com.netprecision.sqlMaker.annotations.Column;
import com.netprecision.sqlMaker.annotations.Join;
import com.netprecision.sqlMaker.annotations.Table;
import com.netprecision.usuario.entity.Usuario;

/**
 * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737148">HistoricoAcao</a>
 */
@Table(schema = "public", nome = "historico_acao")
public class HistoricoAcao extends GenericBean<HistoricoAcao> implements Serializable
{
    private static final long      serialVersionUID = 1L;

    /* Atributos da tabela ************************************************************************************************************************************************************************* */

    @Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
    private Long                   cdId;

    @Column(coluna = "dt_historico", atributo = "dtHistorico", updatable = false)
    private Timestamp              dtHistorico;

    @Column(coluna = "fk_usuario", atributo = "fkUsuario", updatable = false)
    private Long                   fkUsuario;

    @Column(coluna = "fk_versao_documento", atributo = "fkVersaoDocumento", updatable = false)
    private Long                   fkVersaoDocumento;

    @Column(coluna = "fk_arquivo_versao_documento", atributo = "fkArquivoVersaoDocumento", updatable = false)
    private Long                   fkArquivoVersaoDocumento;

    @Column(coluna = "fk_arquivo_comentario", atributo = "fkArquivoComentario", updatable = false)
    private Long                   fkArquivoComentario;

    @Column(coluna = "fk_comentario", atributo = "fkComentario", updatable = false)
    private Long                   fkComentario;

    @Column(coluna = "fk_acao", atributo = "fkAcao", updatable = false)
    private Long                   fkAcao;

    /* Referencias cruzadas ************************************************************************************************************************************************************************ */

    @Join(foreignKey = "fkUsuario")
    private Usuario                usuario;

    @Join(foreignKey = "fkVersaoDocumento")
    private VersaoDocumento        versaoDocumento;

    @Join(foreignKey = "fkArquivoVersaoDocumento")
    private ArquivoVersaoDocumento arquivoVersaoDocumento;

    @Join(foreignKey = "fkArquivoComentario")
    private ArquivoComentario      arquivoComentario;

    @Join(foreignKey = "fkComentario")
    private Comentario             comentario;

    @Join(foreignKey = "fkAcao")
    private Acao                   acao;

    /* Atributos de projecoes ********************************************************************************************************************************************************************** */

    /* Construtor ********************************************************************************************************************************************************************************** */

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public HistoricoAcao()
    {
    }

    /* Gets and Sets ******************************************************************************************************************************************************************************* */

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    @Override
    public Long getCdId()
    {
        return cdId;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    @Override
    public void setCdId(Long cdId)
    {
        this.cdId = cdId;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public Timestamp getDtHistorico()
    {
        return dtHistorico;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setDtHistorico(Timestamp dtHistorico)
    {
        this.dtHistorico = dtHistorico;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public Long getFkUsuario()
    {
        return fkUsuario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setFkUsuario(Long fkUsuario)
    {
        this.fkUsuario = fkUsuario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public Long getFkVersaoDocumento()
    {
        return fkVersaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setFkVersaoDocumento(Long fkVersaoDocumento)
    {
        this.fkVersaoDocumento = fkVersaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public Long getFkAcao()
    {
        return fkAcao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setFkAcao(Long fkAcao)
    {
        this.fkAcao = fkAcao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setAcao(Acao acao)
    {
        this.acao = acao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public Acao getAcao()
    {
        return acao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public Usuario getUsuario()
    {
        return usuario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setUsuario(Usuario usuario)
    {
        this.usuario = usuario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public VersaoDocumento getVersaoDocumento()
    {
        return versaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R5</a>
     */
    public void setVersaoDocumento(VersaoDocumento versaoDocumento)
    {
        this.versaoDocumento = versaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R5</a>
     */
    public Long getFkArquivoVersaoDocumento()
    {
        return fkArquivoVersaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R5</a>
     */
    public void setFkArquivoVersaoDocumento(Long fkArquivoVersaoDocumento)
    {
        this.fkArquivoVersaoDocumento = fkArquivoVersaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R5</a>
     */
    public ArquivoVersaoDocumento getArquivoVersaoDocumento()
    {
        return arquivoVersaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R5</a>
     */
    public void setArquivoVersaoDocumento(ArquivoVersaoDocumento arquivoVersaoDocumento)
    {
        this.arquivoVersaoDocumento = arquivoVersaoDocumento;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public Long getFkArquivoComentario()
    {
        return fkArquivoComentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public void setFkArquivoComentario(Long fkArquivoComentario)
    {
        this.fkArquivoComentario = fkArquivoComentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public Long getFkComentario()
    {
        return fkComentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public void setFkComentario(Long fkComentario)
    {
        this.fkComentario = fkComentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public ArquivoComentario getArquivoComentario()
    {
        return arquivoComentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public void setArquivoComentario(ArquivoComentario arquivoComentario)
    {
        this.arquivoComentario = arquivoComentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public Comentario getComentario()
    {
        return comentario;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R11</a>
     */
    public void setComentario(Comentario comentario)
    {
        this.comentario = comentario;
    }
}