package com.netprecision.ambiente.respostaConvite.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.netprecision.ambiente.ambiente.entity.Ambiente;
import com.netprecision.ambiente.convite.entity.Convite;
import com.netprecision.generic.entity.GenericBean;
import com.netprecision.sqlMaker.annotations.Column;
import com.netprecision.sqlMaker.annotations.Join;
import com.netprecision.sqlMaker.annotations.Table;
import com.netprecision.usuario.entity.Usuario;

/**
 * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737221">Situacao</a>
 */
@Table(schema = "public", nome = "resposta_convite")
public class RespostaConvite extends GenericBean<RespostaConvite> implements Serializable
{
    private static final long serialVersionUID = 1L;

    /* Atributos da tabela ************************************************************************************************************************************************************************* */

    @Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
    private Long              cdId;

    @Column(coluna = "dt_resposta_convite", atributo = "dtRespostaConvite")
    private Timestamp         dtRespostaConvite;

    @Column(coluna = "tx_observacao", atributo = "txObservacao")
    private String            txObservacao;

    @Column(coluna = "fl_situacao", atributo = "flSituacao")
    private Integer           flSituacao;

    @Column(coluna = "ds_codigo", atributo = "dsCodigo")
    private String            dsCodigo;

    @Column(coluna = "fk_convite", atributo = "fkConvite", updatable = false)
    private Long              fkConvite;

    @Column(coluna = "fk_convidado", atributo = "fkConvidado", updatable = false)
    private Long              fkConvidado;

    @Column(coluna = "fk_ambiente", atributo = "fkAmbiente", updatable = false)
    private Long              fkAmbiente;

    /* Referencias cruzadas ************************************************************************************************************************************************************************ */

    @Join(foreignKey = "fkConvite")
    private Convite           convite;

    @Join(foreignKey = "fkConvidado")
    private Usuario           convidado;

    @Join(foreignKey = "fkAmbiente")
    private Ambiente          ambiente;

    /* Atributos de projecoes ********************************************************************************************************************************************************************** */

    /* Construtor ********************************************************************************************************************************************************************************** */

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public RespostaConvite()
    {
    }

    /* Gets and Sets ******************************************************************************************************************************************************************************* */

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Long getCdId()
    {
        return cdId;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setCdId(Long cdId)
    {
        this.cdId = cdId;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Timestamp getDtRespostaConvite()
    {
        return dtRespostaConvite;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setDtRespostaConvite(Timestamp dtRespostaConvite)
    {
        this.dtRespostaConvite = dtRespostaConvite;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public String getTxObservacao()
    {
        return txObservacao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setTxObservacao(String txObservacao)
    {
        this.txObservacao = txObservacao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Integer getFlSituacao()
    {
        return flSituacao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setFlSituacao(Integer flSituacao)
    {
        this.flSituacao = flSituacao;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Long getFkConvite()
    {
        return fkConvite;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setFkConvite(Long fkConvite)
    {
        this.fkConvite = fkConvite;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Long getFkConvidado()
    {
        return fkConvidado;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setFkConvidado(Long fkConvidado)
    {
        this.fkConvidado = fkConvidado;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Convite getConvite()
    {
        return convite;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setConvite(Convite convite)
    {
        this.convite = convite;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public Usuario getConvidado()
    {
        return convidado;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     */
    public void setConvidado(Usuario convidado)
    {
        this.convidado = convidado;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     */
    public String getDsCodigo()
    {
        return dsCodigo;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     */
    public void setDsCodigo(String dsCodigo)
    {
        this.dsCodigo = dsCodigo;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     */
    public Long getFkAmbiente()
    {
        return fkAmbiente;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     */
    public void setFkAmbiente(Long fkAmbiente)
    {
        this.fkAmbiente = fkAmbiente;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     */
    public Ambiente getAmbiente()
    {
        return ambiente;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     */
    public void setAmbiente(Ambiente ambiente)
    {
        this.ambiente = ambiente;
    }
}