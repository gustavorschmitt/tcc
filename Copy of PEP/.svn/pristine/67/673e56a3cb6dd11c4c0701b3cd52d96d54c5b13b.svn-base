package com.netprecision.arquivo.mbean;

import java.io.FileOutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.netprecision.arquivo.dao.ArquivoDaoInterface;
import com.netprecision.arquivo.entity.dto.ArquivoDto;
import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.generic.mbean.GenericMBImp;
import com.netprecision.generic.utils.JSPUtils;
import com.netprecision.generic.utils.MBUtils;

@Service("arquivoMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class ArquivoMBImp extends GenericMBImp<ArquivoDto> implements ArquivoMBInterface
{
    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R1</a>
     *
     * @return ArquivoDaoInterface instancia retirada do pool de objetos do Spring
     */
    @Override
    public ArquivoDaoInterface getDaoBean()
    {
        return (ArquivoDaoInterface) CustomApplicationContextAware.getBean("arquivoDao");
    }

    /**
     * Metodo que salva os arquivos anexados pelo usuario no disco (servidor) e retorna
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R1</a>
     *
     * @param request
     *            requisicao que o usuario enviou para o servidor contendo os arquivos anexos
     * @param idGuia
     *            identificador da guia que o usuario esta usando (tras informacoes como por exemplo, fkAmbiente)
     * 
     * @return lista de objeto de arquivos salvos
     */
    @Override
    public List<ArquivoDto> salvar(MultipartHttpServletRequest request, String idGuia, String dsCaminho)
    {
        List<ArquivoDto> list = new ArrayList<ArquivoDto>(2);
        Iterator<String> itr = request.getFileNames();
        MultipartFile mpf = null;

        while(itr.hasNext())
        {
            mpf = request.getFile(itr.next());

            ArquivoDto arquivo = new ArquivoDto();
            arquivo.setNmArquivo(mpf.getOriginalFilename());
            arquivo.setNrTamanho(mpf.getSize());
            arquivo.setNmTipoConteudo(mpf.getContentType());
            arquivo.setDtCriacao(new Timestamp(new java.util.Date().getTime()));
            arquivo.setDsCaminho(dsCaminho);
            arquivo.setFkAmbiente(JSPUtils.getFkAmbienteDaGuia(request, idGuia));

            try
            {
                arquivo.setDsCaminho(arquivo.getDsCaminho().replace("\\", "\\\\"));
                arquivo = MBUtils.getArquivoMB().salvar(arquivo);
                arquivo.setDsCaminho(arquivo.getDsCaminho() + arquivo.getCdId());

                // grava arquivo no disco
                FileCopyUtils.copy(mpf.getBytes(), new FileOutputStream(arquivo.getDsCaminho().replace("\\\\", "\\")));

                list.add(arquivo);
            }
            catch(Exception e)
            {
                // TODO Logger.error.internalMethod
                e.printStackTrace();
            }
        }

        return list;
    }
}