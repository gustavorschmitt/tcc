package com.netprecision.notificacao.notificacaoUsuario.mbean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.netprecision.ambiente.ambienteUsuario.entity.dto.AmbienteUsuarioDto;
import com.netprecision.ambiente.respostaConvite.entity.dto.RespostaConviteDto;
import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.enumeration.EntidadeNotificacao;
import com.netprecision.enumeration.Perfil;
import com.netprecision.generic.entity.Validator;
import com.netprecision.generic.mbean.GenericMBImp;
import com.netprecision.generic.utils.MBUtils;
import com.netprecision.notificacao.notificacaoUsuario.dao.NotificacaoUsuarioDaoInterface;
import com.netprecision.notificacao.notificacaoUsuario.entity.dto.NotificacaoUsuarioDto;
import com.netprecision.sqlMaker.mapped.Restricao;
import com.netprecision.sqlMaker.type.JoinType;

@Service("notificacaoUsuarioMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class NotificacaoUsuarioMBImp extends GenericMBImp<NotificacaoUsuarioDto> implements NotificacaoUsuarioMBInterface
{

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R2</a>
     *
     * @return AcaoDaoInterface instancia retirada do pool de objetos do Spring
     */
    @Override
    protected NotificacaoUsuarioDaoInterface getDaoBean()
    {
        return (NotificacaoUsuarioDaoInterface) CustomApplicationContextAware.getBean("notificacaoUsuarioDao");
    }

    /**
     * gera a notificacaoUsuario para os convidados do convite
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R3</a>
     * 
     * @param fkAmbiente
     *            para salvar na notificacaoUsuario
     * @param fkAmbfkNotificacaoiente
     *            para salvar na notificacaoUsuario
     * @param listaRespostaConvite
     *            lista de respostaConvite que serao geradas notificaoUsuario
     */
    @Override
    public void gerarNotificacaoUsuarioConvite(Long fkAmbiente, Long fkNotificacao, List<RespostaConviteDto> listaRespostaConvite)
    {
        try
        {
            if(!Validator.isValid(fkNotificacao) && Validator.isValid(listaRespostaConvite))
                throw new IllegalArgumentException();

            ArrayList<NotificacaoUsuarioDto> listaNotificacao = new ArrayList<NotificacaoUsuarioDto>();
            for(RespostaConviteDto respostaConvite : listaRespostaConvite)
            {
                NotificacaoUsuarioDto montarNotificacaoUsuario = montarNotificacaoUsuario(fkAmbiente, fkNotificacao, respostaConvite.getFkConvidado());
                listaNotificacao.add(montarNotificacaoUsuario);
            }
            salvar(listaNotificacao);
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
        }
    }

    /**
     * Metodo que recebe uma notificacao e uma versao de documento para procurar quem tem acesso na ACL para visualizar o documento e gerar suas devidas notificacoes
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R7</a>
     * 
     * @param fkCadastrante
     *            referente ao usuario q cadastrou o comentario
     * @param fkNotificacao
     *            referente a notificacao que esta sendo gerada
     * @param fkVersaoDocumento
     *            referente a versao do documento que foi comentada
     */
    public void gerarNotificacaoUsuarioCadastrouComentario(Long fkCadastrante, Long fkAmbiente, Long fkNotificacao, Long fkVersaoDocumento)
        throws Exception
    {
        List<NotificacaoUsuarioDto> listNotificados = new ArrayList<NotificacaoUsuarioDto>();
        Set<Long> set = MBUtils.getControleAcessoMB().listarUsuariosComPermissaoParaNotificacao(fkCadastrante, fkVersaoDocumento);

        for(Long vlLong : set)
            listNotificados.add(montarNotificacaoUsuario(fkAmbiente, fkNotificacao, vlLong));

        super.salvar(listNotificados);
    }

    /**
     * Metodo que gera a notificacaoUsuario para as respostas dos convites feitos. <br />
     * Somente os Administradores do ambiente sao notificados da resposta.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R3</a>
     * 
     * @param fkNotificacao
     *            para utilizar na consulta
     * @param fkAmbiente
     *            para utilizar na consulta
     */
    @Override
    public void gerarNotificacaoUsuarioRespostaConvite(Long fkNotificacao, Long fkAmbiente)
        throws Exception
    {
        if(!Validator.isValid(fkNotificacao) && Validator.isValid(fkAmbiente))
            throw new IllegalArgumentException();

        List<AmbienteUsuarioDto> listAmbienteUsuario = MBUtils.getAmbienteUsuarioMB().listarUsuariosDoAmbientePeloPerfil(fkAmbiente, Perfil.ADMINISTRADOR.id);
        List<NotificacaoUsuarioDto> listNotificacaoUsuario = new ArrayList<NotificacaoUsuarioDto>(2);
        for(AmbienteUsuarioDto ambienteUsuario : listAmbienteUsuario)
            listNotificacaoUsuario.add(montarNotificacaoUsuario(fkAmbiente, fkNotificacao, ambienteUsuario.getFkUsuario()));
        super.salvar(listNotificacaoUsuario);
    }

    /**
     * monta a entidade NotificacaoUsuario com os parametros informados
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R3</a>
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R1</a>
     * 
     * @param fkNotificacao
     *            para colocar na entidade
     * @param fkNotificado
     *            para colocar na entidade
     */
    private NotificacaoUsuarioDto montarNotificacaoUsuario(Long fkAmbiente, Long fkNotificacao, Long fkNotificado)
    {
        NotificacaoUsuarioDto notificacaoUsuario = new NotificacaoUsuarioDto();
        notificacaoUsuario.setFkAmbiente(fkAmbiente);
        notificacaoUsuario.setFkNotificacao(fkNotificacao);
        notificacaoUsuario.setFkNotificado(fkNotificado);
        notificacaoUsuario.setFlImportante(false);
        return notificacaoUsuario;
    }

    /**
     * Contar a quantidade de notificacoes que o usuario tem para aquela projeto, notificacoes vinculadas a documentos
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R1</a>
     * 
     * @param fkAmbiente
     *            que sera usado como filtro
     * @param fkUsuario
     *            que sera usado como filtro
     * @return quantidade total de registros encontrados
     */
    public Long contarNotificacoesReferenteDocumento(Long fkAmbiente, Long fkUsuario)
    {
        try
        {
            NotificacaoUsuarioDto notificacaoUsuario = new NotificacaoUsuarioDto();
            notificacaoUsuario.setFields("cdId", "notificacao.cdId");
            notificacaoUsuario.setFkAmbiente(fkAmbiente);
            notificacaoUsuario.setFkNotificado(fkUsuario);
            notificacaoUsuario.addRestricao(Restricao.in("notificacao.nrEntidadeNotificada", new Long[] { new Long(EntidadeNotificacao.COMENTARIO.id) }));
            notificacaoUsuario.addRestricao(Restricao.isNull("dtLido"));
            return super.contarRegistros(notificacaoUsuario);
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
            return 0L;
        }
    }

    /**
     * Contar a quantidade de notificacoes que o usuario tem para aquela projeto, notificacoes vinculadas a convite e resposta do convite
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R1</a>
     * 
     * @param fkAmbiente
     *            que sera usado como filtro
     * @param fkUsuario
     *            que sera usado como filtro
     * @return quantidade total de registros encontrados
     */
    public Long contarNotificacoesReferenteConvite(Long fkAmbiente, Long fkUsuario)
    {
        try
        {
            NotificacaoUsuarioDto notificacaoUsuario = new NotificacaoUsuarioDto();
            notificacaoUsuario.setFields("cdId", "notificacao.cdId");
            notificacaoUsuario.setFkNotificado(fkUsuario);
            notificacaoUsuario.addRestricao(Restricao
                    .in("notificacao.nrEntidadeNotificada", new Long[] { new Long(EntidadeNotificacao.CONVITE.id), new Long(EntidadeNotificacao.RESPOSTA_CONVITE.id) }));
            notificacaoUsuario.addRestricao(Restricao.isNull("dtLido"));
            return super.contarRegistros(notificacaoUsuario);
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
            return 0L;
        }
    }

    /**
     * Metodo que seta as notificacoes do usuario como listadas, quando o usuario lista-las na tela.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R15</a>
     * 
     * @param fkUsuarioLogado
     *            referente ao usuario que esta logado e listando as notificacoes
     * @param fkAmbiente
     *            referente ao ambiente que as notificacoes estao sendo listadas
     */
    public void atualizarNotificacaoUsuarioNaoListadas(Long fkUsuarioLogado, Long fkAmbiente)
    {
        try
        {
            NotificacaoUsuarioDto filtro = new NotificacaoUsuarioDto();
            filtro.addFields("*");
            filtro.addRestricao(Restricao.in("notificacao.nrEntidadeNotificada", new Long[] { new Long(EntidadeNotificacao.COMENTARIO.id) }));
            filtro.addRestricao(Restricao.equal("fkNotificado", fkUsuarioLogado));
            filtro.addRestricao(Restricao.equal("fkAmbiente", fkAmbiente));
            filtro.addRestricao(Restricao.isNull("dtListado"));
            filtro.addJoin("notificacao", JoinType.INNER);
            List<NotificacaoUsuarioDto> listNotificacao = super.listar(filtro);
            for(NotificacaoUsuarioDto forDto : listNotificacao)
                forDto.setDtListado(new Timestamp(new java.util.Date().getTime()));
            super.atualizar(listNotificacao);
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
        }
    }

    /**
     * Metodo que marca todas as notificacoes do usuario como lidas.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R11</a>
     * 
     * @param fkUsuario
     *            referente ao usuario que esta logado e listando as notificacoes
     * @param fkAmbiente
     *            referente ao ambiente que as notificacoes estao sendo listadas
     * 
     * @return lista de notificacoesUsuario que foram marcados como lidas
     */
    public List<NotificacaoUsuarioDto> marcarTodasNotificacaoUsuarioComoLida(Long fkUsuario, Long fkAmbiente)
    {
        try
        {
            NotificacaoUsuarioDto dto = new NotificacaoUsuarioDto();
            dto.setFkAmbiente(fkAmbiente);
            dto.setFkNotificado(fkUsuario);
            List<NotificacaoUsuarioDto> listNU = super.listar(dto);
            for(NotificacaoUsuarioDto forDto : listNU)
                forDto.setDtLido(new Timestamp(new java.util.Date().getTime()));

            super.atualizar(listNU);
            return listNU;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo que marca UMA as notificacao do usuario como lida.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R10</a>
     * 
     * @param cdId
     *            referente a notificacao do usuario que esta sendo marcada como lida
     * 
     * @return data em que foi marcado como lida
     */
    public String marcarNotificacaoUsuarioComoLida(Long cdId)
    {
        try
        {
            NotificacaoUsuarioDto dto = super.recuperar(cdId);
            dto.setDtLido(new Timestamp(new java.util.Date().getTime()));
            dto = super.atualizar(dto);
            return dto != null ? dto.getDtLidoString() : null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo que marca UMA as notificacao do usuario como importante.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R12</a>
     * 
     * @param cdId
     *            referente a notificacao do usuario que esta sendo marcada como lida
     * 
     * @return foi marcada como importante? (sim / nao)
     */
    public boolean marcarNotificacaoUsuarioImportante(Long cdId)
    {
        try
        {
            NotificacaoUsuarioDto dto = super.recuperar(cdId);
            dto.setFlImportante(!dto.getFlImportante());
            dto = super.atualizar(dto);
            return dto != null;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
            return false;
        }
    }
}