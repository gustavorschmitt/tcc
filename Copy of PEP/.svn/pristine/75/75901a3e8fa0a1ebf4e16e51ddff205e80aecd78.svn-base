package com.netprecision.ambiente.respostaConvite.mbean;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.netprecision.ambiente.respostaConvite.dao.RespostaConviteDaoInterface;
import com.netprecision.ambiente.respostaConvite.entity.dto.RespostaConviteDto;
import com.netprecision.config.CustomApplicationContextAware;
import com.netprecision.enumeration.Situacao;
import com.netprecision.generic.entity.Validator;
import com.netprecision.generic.exception.ViolacaoAcessoException;
import com.netprecision.generic.mbean.GenericMBImp;
import com.netprecision.generic.utils.MBUtils;
import com.netprecision.sqlMaker.mapped.Restricao;
import com.netprecision.sqlMaker.type.SortOrder;

@Service("respostaConviteMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class RespostaConviteMBImp extends GenericMBImp<RespostaConviteDto> implements RespostaConviteMBInterface
{
    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R4</a>
     *
     * @return RespostaConviteDaoInterface instancia retirada do pool de objetos do Spring
     */
    @Override
    protected RespostaConviteDaoInterface getDaoBean()
    {
        return (RespostaConviteDaoInterface) CustomApplicationContextAware.getBean("respostaConviteDao");
    }

    /**
     * Metodo que valida se o usuario bloqueou o ambiente
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R8</a>
     *
     * @param fkUsuario
     *            referencia ao usuario que sera testado
     * @param fkAmbiente
     *            referencia ao ambiente que sera testado
     *
     * @return true=bloqueou / false=nao bloqueou
     */
    @Override
    public boolean validarUsuarioBloqueouAmbiente(Long fkUsuario, Long fkAmbiente)
    {
        try
        {
            RespostaConviteDto respostaConviteDto = new RespostaConviteDto();
            respostaConviteDto.setFkConvidado(fkUsuario);
            respostaConviteDto.setFkAmbiente(fkAmbiente);
            respostaConviteDto.addRestricao(Restricao.in("flSituacao", Arrays.asList(Situacao.BLOQUEADO.id, Situacao.BLOQUEADO_ATRAVES_OUTRO_CONVITE.id)));
            return contarRegistros(respostaConviteDto) > 0;
        }
        catch(Exception e)
        {
            // TODO Logger.error.internalMethod
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Metodo que lista as respostas dos convites para um determinado ambiente
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R7</a>
     *
     * @param fkAmbiente
     *            referencia ao ambiente para filtro
     *
     * @return lista de respostas de convite encontradas no filtro
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public List<RespostaConviteDto> listar(Long fkAmbiente)
        throws Exception
    {
        RespostaConviteDto respostaConviteDto = new RespostaConviteDto();
        respostaConviteDto.setFields("flSituacao", "dtRespostaConvite", "convite.dtCadastramento", "convite.anfitriao.nmUsuario", "convidado.nmUsuario", "convidado.dsEmail");
        respostaConviteDto.addJoin("convite");
        respostaConviteDto.addJoin("convite.anfitriao");
        respostaConviteDto.addJoin("convidado");
        respostaConviteDto.setFkAmbiente(fkAmbiente);
        respostaConviteDto.addOrdem("convite.dtCadastramento", SortOrder.DESCENDING);

        return MBUtils.getRespostaConviteMB().listar(respostaConviteDto);
    }

    /**
     * Metodo que atualiza a situacao dos demais convites do usuario
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R12</a>
     *
     * @param txObservacao
     *            texto que o usuario ira digitar na tela para ser gravado junto a resposta do convite
     * @param respostaConvite
     *            referencia a resposta do convite que o usuario esta respondendo (o usuario pode ter +1 convite para o ambiente)
     * @param situacaoFiltro
     *            situacao atual das repostas do convite
     * @param situacaoAtualizada
     *            situacao que sera usada para atualizar as demais respostas do convite
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public void atualizarSituacaoOutrosConvitesPendentes(String txObservacao, RespostaConviteDto respostaConvite, Situacao situacaoFiltro, Situacao situacaoAtualizada)
        throws Exception
    {
        RespostaConviteDto respostaConviteDtoFiltro = new RespostaConviteDto();
        respostaConviteDtoFiltro.setFields("cdId", "flSituacao", "dtRespostaConvite", "dsCodigo");
        respostaConviteDtoFiltro.setFkAmbiente(respostaConvite.getFkAmbiente());
        respostaConviteDtoFiltro.setFkConvidado(respostaConvite.getFkConvidado());
        if(situacaoFiltro != null)
            respostaConviteDtoFiltro.addRestricao(Restricao.equal("flSituacao", situacaoFiltro.id));
        respostaConviteDtoFiltro.addRestricao(Restricao.notEqual("cdId", respostaConvite.getCdId()));
        List<RespostaConviteDto> listRespostaConviteDto = MBUtils.getRespostaConviteMB().listar(respostaConviteDtoFiltro);

        if(Validator.isValid(listRespostaConviteDto))
        {
            for(RespostaConviteDto respostaConviteDtoInterno : listRespostaConviteDto)
            {
                respostaConviteDtoInterno.setFlSituacao(situacaoAtualizada.id);
                respostaConviteDtoInterno.setTxObservacao(txObservacao);
                respostaConviteDtoInterno.setDtRespostaConvite(respostaConvite.getDtRespostaConvite());
                MBUtils.getRespostaConviteMB().atualizar(respostaConviteDtoInterno);
            }
        }
    }

    /**
     * Metodo que atualiza a situacao do convite pelo dsCodigo
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10, NKC-96-R12</a>
     *
     * @param dsCodigo
     *            codigo de identificacao do convite
     * 
     * @param situacao
     *            situacao que sera atualizada no convite
     *
     * @return objeto atualizado com as informacoes armazenadas
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public RespostaConviteDto atualizarSituacaoConvite(String dsCodigo, Situacao situacao)
        throws Exception
    {
        return atualizarSituacaoEObservacaoConvite(dsCodigo, situacao, null);
    }

    /**
     * Metodo que atualiza a situacao e observacao do convite pelo dsCodigo
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R12</a>
     *
     * @param dsCodigo
     *            codigo de identificacao do convite
     * 
     * @param situacao
     *            situacao que sera atualizada no convite
     * 
     * @param txObservacao
     *            observacao que sera atualizada no convite
     *
     * @return objeto atualizado com as informacoes armazenadas
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public RespostaConviteDto atualizarSituacaoEObservacaoConvite(String dsCodigo, Situacao situacao, String txObservacao)
        throws Exception
    {
        RespostaConviteDto respostaConvite = new RespostaConviteDto();
        respostaConvite.addRestricao(Restricao.equal("dsCodigo", dsCodigo));
        respostaConvite = MBUtils.getRespostaConviteMB().recuperar(respostaConvite);
        respostaConvite.setFlSituacao(situacao.id);
        respostaConvite.setDtRespostaConvite(new Timestamp(new Date().getTime()));
        if(Validator.isValid(txObservacao))
            respostaConvite.setTxObservacao(txObservacao);

        return MBUtils.getRespostaConviteMB().atualizar(respostaConvite);
    }

    /**
     * Metodo que processa um aceite de convite para participar do ambiente e atualiza a situacao dos demais convites do mesmo usuario para o ambiente
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R10</a>
     *
     * @param dsCodigo
     *            codigo de identificacao do convite
     * 
     * @return objeto atualizado com as informacoes armazenadas
     * 
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public RespostaConviteDto aceitarConvite(String dsCodigo)
        throws Exception
    {
        // Atualiza a situacao do convite
        RespostaConviteDto respostaConvite = this.atualizarSituacaoConvite(dsCodigo, Situacao.ACEITO);

        // Adiciona o usuario no ambiente
        MBUtils.getAmbienteUsuarioMB().salvar(respostaConvite.getFkAmbiente(), respostaConvite.getFkConvidado());

        // Atualiza demais convites
        atualizarSituacaoOutrosConvitesPendentes(null, respostaConvite, null, Situacao.ACEITO_ATRAVES_OUTRO_CONVITE);

        // gerar notificacao
        MBUtils.getNotificacaoMB().gerarNotificacaoAceitouConvite(respostaConvite);

        return respostaConvite;
    }

    /**
     * Metodo que salva a justificativa de recusa do convite e se o usuario bloqueou para nao receber mais convite deste ambiente
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R12</a>
     *
     * @param dsCodigo
     *            codigo de identificacao do convite
     * @param bloqueado
     *            se o usuario selecionou a opcao de bloquear convites
     * @param txObservacao
     *            texto contendo a justificativa do usuario para ter recusado ou bloqueado o convite
     * 
     * @exception ViolacaoAcessoException
     *                se houver a tentativa, falha, de acessar dados que o usuario nao tem permissao
     * @exception Exception
     *                se der algum erro eh lancado uma exception para o metodo que chamou este
     */
    @Override
    public void salvarConviteRecusado(String dsCodigo, boolean bloqueado, String txObservacao)
        throws ViolacaoAcessoException,
            Exception
    {
        Situacao situacao = bloqueado ? Situacao.BLOQUEADO : Situacao.RECUSADO;
        RespostaConviteDto respostaConvite = atualizarSituacaoEObservacaoConvite(dsCodigo, situacao, txObservacao);
        if(bloqueado)
        {
            atualizarSituacaoOutrosConvitesPendentes(txObservacao, respostaConvite, Situacao.PENDENTE, Situacao.BLOQUEADO_ATRAVES_OUTRO_CONVITE);
            // gerar notificacao
            MBUtils.getNotificacaoMB().gerarNotificacaoBloqueouConvite(respostaConvite);
        }
        else
        {
            // gerar notificacao
            MBUtils.getNotificacaoMB().gerarNotificacaoRecusouConvite(respostaConvite);
        }
    }

    /**
     * valida a partir dsCodigo se o usuario tem outros convites ativos nesse ambiente
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R11</a>
     *
     * @param dsCodigo
     *            codigo de identificacao do convite
     * 
     * @return true se o usuario possui outro convite ativo no ambiente
     */
    @Override
    public boolean validarConviteJaAceito(String dsCodigo)
    {
        try
        {
            RespostaConviteDto respostaConviteDto = new RespostaConviteDto();
            respostaConviteDto.setFields("flSituacao");
            respostaConviteDto.setFlSituacao(Situacao.ACEITO.id);

            RespostaConviteDto respostaConviteSubQueryFkConvidado = new RespostaConviteDto();
            respostaConviteSubQueryFkConvidado.setFields("fkConvidado");
            respostaConviteSubQueryFkConvidado.addRestricao(Restricao.equal("dsCodigo", dsCodigo));
            respostaConviteDto.addRestricao(Restricao.in("fkConvidado", Restricao.subQuery(respostaConviteSubQueryFkConvidado)));

            RespostaConviteDto respostaConviteSubQueryFkAmbiente = new RespostaConviteDto();
            respostaConviteSubQueryFkAmbiente.setFields("fkAmbiente");
            respostaConviteSubQueryFkAmbiente.addRestricao(Restricao.equal("dsCodigo", dsCodigo));
            respostaConviteDto.addRestricao(Restricao.in("fkAmbiente", Restricao.subQuery(respostaConviteSubQueryFkAmbiente)));

            return contarRegistros(respostaConviteDto) > 0;
        }
        catch(Exception e)
        {
            // TODO TODO Logger.error.internalMethod
            e.printStackTrace();
            return false;
        }

    }

    /**
     * valida a partir dsCodigo se o usuario esta ativo
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736903">NKC-125</a>
     *
     * @param dsCodigo
     *            codigo de identificacao da resposta convite
     * 
     * @return true se o usuario possui esta ativo
     */
    @Override
    public boolean validaUsuarioDoCodigoHashAtivo(String dsCodigo)
    {
        try
        {
            RespostaConviteDto respostaConviteDtoFiltro = new RespostaConviteDto();
            respostaConviteDtoFiltro.setFields("convidado.flSituacao");
            respostaConviteDtoFiltro.addJoin("convidado");
            respostaConviteDtoFiltro.addRestricao(Restricao.equal("dsCodigo", dsCodigo));
            RespostaConviteDto respostaConvite = recuperar(respostaConviteDtoFiltro);
            return respostaConvite.getConvidado().getFlSituacao().intValue() == Situacao.ATIVO.id.intValue();
        }
        catch(Exception e)
        {
            // TODO TODO Logger.error.internalMethod
            e.printStackTrace();
            return false;
        }
    }
}