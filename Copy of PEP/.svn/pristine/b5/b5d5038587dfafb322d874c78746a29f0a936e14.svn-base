package com.netprecision.generic.entity;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.netprecision.config.CustomMessageSource;
import com.netprecision.enumeration.Situacao;
import com.netprecision.sqlMaker.entity.EntitySqlMaker;
import com.netprecision.sqlMaker.utils.SqlMakerReflection;

public abstract class GenericBean<T extends GenericBean<?>> extends EntitySqlMaker<T>
{
    /**
     * Metodo que formata o campo data segundo a mascara DD/MM/YYYY HH:MM:SS
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     */
    public String getDataString(Date date)
    {
        return date != null ? new SimpleDateFormat(CustomMessageSource.resolverMensagem("sistema_mascara_dataHora")).format(date) : null;
    }

    /**
     * Metodo que formata o campo data segundo a mascara DD/MM HH:MM
     * 
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R3</a>
     */
    public String getDataSemAnoSemSegundoString(Date date)
    {
        return date != null ? new SimpleDateFormat(CustomMessageSource.resolverMensagem("sistema_mascara_dataHoraSemAnoSemSegundo")).format(date) : null;
    }

    /**
     * Metodo de injecao de dependencia do Spring, usado pela camada MB.
     * 
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     *
     * @return MensagemDaoInterface instancia retirada do pool de objetos do Spring
     */
    public Date adicionarHoraMinuto(Date data, int hour, int minute)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(data);
        calendar.add(Calendar.HOUR_OF_DAY, hour);
        calendar.add(Calendar.MINUTE, minute);
        return calendar.getTime();
    }

    /**
     * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
     */
    public String getAtivoInativoString(Boolean flSituacao)
    {
        return CustomMessageSource.resolverMensagem(flSituacao ? "sistema_situacao_ativo" : "sistema_situacao_inativo");
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R2</a>
     */
    public String getDoubleEmString(Double valor, String dsChaveFormato)
    {
        if(Validator.isValid(valor))
        {
            DecimalFormat decimalFormat = new DecimalFormat(CustomMessageSource.resolverMensagem(dsChaveFormato));
            return decimalFormat.format(valor);
        }
        return null;
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736714">NKC-84-R4</a>
     */
    public void converterDados(GenericBean<?> json, GenericBean<?> entity)
    {
        try
        {
            if(entity != null)
            {
                for(Field field : entity.getClass().getSuperclass().getDeclaredFields())
                    if(!Modifier.toString(field.getModifiers()).contains("final"))
                        SqlMakerReflection.set(json, field.getName(), SqlMakerReflection.get(entity, field.getName()));
                for(Field field : entity.getClass().getDeclaredFields())
                    if(!Modifier.toString(field.getModifiers()).contains("final"))
                        SqlMakerReflection.set(json, field.getName(), SqlMakerReflection.get(entity, field.getName()));
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114159">NKC-96-R7</a>
     * @see <a href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114250">NKC-99-R6</a>
     */
    public String resolverFlSituacao(Integer flSituacao)
    {
        return flSituacao != null ? Situacao.getValueById(flSituacao).dsChaveString : null;
    }
}