package com.tcc.usuario.dao;

import com.tcc.generic.dao.GenericDaoInterface;
import com.tcc.usuario.entity.dto.UsuarioDto;

public interface UsuarioDaoInterface extends GenericDaoInterface<UsuarioDto> {
}