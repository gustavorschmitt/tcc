package com.tcc.usuario.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcc.controleAcesso.PDPImpl;

@RestController
public class PDPService {

	@RequestMapping(value = "/avaliar")
	public String avaliar(HttpSession session, HttpServletRequest request) throws Exception {

		String pepRequest = request.getParameter("pepRequest");
		String pdpPolicy = request.getParameter("pdpPolicy");

		String[] split = pepRequest.split(";");
		byte b[] = new byte[split.length + 50];
		for (int i = 0; i < split.length; i++) {
			b[i] = new Byte(split[i]).byteValue();
		}
		pepRequest = new String(b);
		String avaliacao = "false";
		if (pdpPolicy == null) {
			avaliacao = PDPImpl.avaliar(pepRequest);
		} else
			avaliacao = PDPImpl.avaliar(pepRequest, pdpPolicy);
		return avaliacao;
	}

	@RequestMapping(value = "/avaliar/{pepRequest}")
	public String avaliarPepRequest(HttpSession session, HttpServletRequest request, @PathVariable String pepRequest) throws Exception {

		String pdpPolicy = request.getParameter("pdpPolicy");
		if (pdpPolicy == null) {
			PDPImpl.avaliar(pepRequest);
		} else
			PDPImpl.avaliar(pepRequest, pdpPolicy);
		return "true";
	}
}
