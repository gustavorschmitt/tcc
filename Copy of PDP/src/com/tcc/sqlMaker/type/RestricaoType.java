package com.tcc.sqlMaker.type;

public enum RestricaoType {
	IGUAL, IGUAL_ATRIBUTO, // equal, equalProperty
	DIFERENTE, DIFERENTE_ATRIBUTO, // notEqual, notEqualProperty
	MAIOR, MAIOR_ATRIBUTO, MAIOR_IGUAL, MAIOR_IGUAL_ATRIBUTO, // greater,
																// greaterProperty,
																// greaterThan,
																// greaterThanProperty
	MENOR, MENOR_ATRIBUTO, MENOR_IGUAL, MENOR_IGUAL_ATRIBUTO, // less,
																// lessProperty,
																// lessThan,
																// lessThanProperty
	AND, OR, // and, or
	IS_NULL, IS_NOT_NULL, // isNull, isNotNull
	IN, NOT_IN, // in, notIn
	LIKE, ILIKE, // like, ilike
	DISJUNCAO, CONJUNCAO, // disjuncao, conjuncao
	SUBQUERY, // subquery
	SUBQUERY_IS_NULL, // subquery IS NULL
	SUBQUERY_IN, SUBQUERY_NOT_IN, // subquery IN, NOT_IN
	SUBQUERY_EQUAL, SUBQUERY_EQUAL_PROPERTY, // subquery EQUAL, EQUAL_PROPERTY
	SUBQUERY_MENOR, SUBQUERY_MENOR_IGUAL, // subquery
	SUBQUERY_MAIOR, SUBQUERY_MAIOR_IGUAL, // subquery
	SQL // sql puro
}