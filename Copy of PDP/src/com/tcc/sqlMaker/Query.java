package com.tcc.sqlMaker;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tcc.generic.entity.Validator;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Join;
import com.tcc.sqlMaker.annotations.JoinMultiplo;
import com.tcc.sqlMaker.annotations.Table;
import com.tcc.sqlMaker.entity.EntitySqlMaker;
import com.tcc.sqlMaker.mapped.ColumnMapped;
import com.tcc.sqlMaker.mapped.OrderMapped;
import com.tcc.sqlMaker.mapped.Restricao;
import com.tcc.sqlMaker.mapped.TableMapped;
import com.tcc.sqlMaker.type.FunctionType;
import com.tcc.sqlMaker.type.MatchType;
import com.tcc.sqlMaker.utils.SqlMakerReflection;

abstract class Query {

	protected static List<Column> getListColumns(Class<?> classBean) {
		List<Column> colunas = new ArrayList<Column>();
		for (Field field : classBean.getDeclaredFields()) {
			Column column = field.getAnnotation(Column.class);
			if (column != null && !column.id())
				colunas.add(column);
		}
		return colunas;
	}

	protected static String getColumns(Class<?> classBean) {
		return getColumns(getListColumns(classBean));
	}

	protected static String getColumns(List<Column> colunas) {
		StringBuilder retorno = new StringBuilder();
		for (Column col : colunas)
			retorno.append(col.coluna() + ",");
		return retorno.substring(0, retorno.length() - 1);
	}

	protected static String getTable(Class<?> classBean) {
		Table table = classBean.getAnnotation(Table.class);
		return table.schema() + "." + table.nome();
	}

	protected static String getNmColuna(EntitySqlMaker<?> bean,
			String pathAtributo, String pathEntity, boolean subQuery)
			throws Exception {
		if (pathAtributo == null)
			return null;
		Field field = SqlMakerReflection.getField(bean, pathAtributo);
		Column annColumn = field.getAnnotation(Column.class);
		TableMapped t = new TableMapped();
		t.setPathEntity(pathEntity);
		int index = bean.getTables().indexOf(t);
		if (index == -1)
			return (subQuery ? "s" : "") + "r." + annColumn.coluna();
		return bean.getTable(pathEntity).getNmAlias() + "."
				+ annColumn.coluna();
	}

	protected static String getFormatedValueToQuery(Object value) {
		if (value instanceof String)
			return getFormatedValueToQuery("STRING", value);
		else if (value instanceof Long)
			return getFormatedValueToQuery("LONG", value);
		else if (value instanceof Integer)
			return getFormatedValueToQuery("INTEGER", value);
		else if (value instanceof Float)
			return getFormatedValueToQuery("FLOAT", value);
		else if (value instanceof Date)
			return getFormatedValueToQuery("DATE", value);
		else if (value instanceof Timestamp)
			return getFormatedValueToQuery("TIMESTAMP", value);
		else if (value instanceof Boolean)
			return getFormatedValueToQuery("BOOLEAN", value);
		return null;
	}

	protected static String getFormatedValueToQuery(String type, Object value) {
		if (value == null)
			return null;

		String retorno = null;
		if (type.equals("STRING"))
			retorno = "'" + formataStringSemCaracterEspecial((String) value)
					+ "'";
		else if (type.equals("LONG") || type.equals("INTEGER")
				|| type.equals("FLOAT"))
			retorno = value.toString();
		else if (type.equals("DATE") || type.equals("TIMESTAMP"))
			retorno = "'" + getTime(value) + "'";
		else if (type.equals("BOOLEAN"))
			retorno = ((Boolean) value).toString();

		return retorno;
	}

	private static String getTime(Object value) {
		Long time = null;
		if (value instanceof Date)
			time = ((Date) value).getTime();
		else if (value instanceof Timestamp)
			time = ((Timestamp) value).getTime();

		if (time != null)
			return new SimpleDateFormat("MM-dd-yyyy HH:mm:ss.SSS").format(time);
		else
			return null;
	}

	protected static void montarRestricoes(EntitySqlMaker<?> bean,
			boolean subQuery) throws Exception {
		try {
			for (Restricao restricao : bean.getRestricoes())
				restricao.setRestricaoSql(montarOperadorRestricao(restricao,
						bean, subQuery));
		} catch (Exception e) {
			System.out.println("SelectQuery.montarRestricoes:");
			e.printStackTrace();
		}
	}

	protected static void montarRestricoes(EntitySqlMaker<?> bean,
			List<Restricao> restricoes, boolean subQuery) throws Exception {
		try {
			for (Restricao restricao : restricoes)
				restricao.setRestricaoSql(montarOperadorRestricao(restricao,
						bean, subQuery));
		} catch (Exception e) {
			System.out.println("SelectQuery.montarRestricoes:");
			e.printStackTrace();
		}
	}

	protected static String montarOperadorRestricao(Restricao restricao,
			EntitySqlMaker<?> bean, boolean subQuery) throws Exception {
		restricao.setNmColuna(getNmColuna(bean, restricao.getPathAtributo(),
				restricao.getPathEntity(), subQuery));
		restricao.setNmColunaForeign(getNmColuna(bean,
				restricao.getPathAtributoForeign(),
				restricao.getPathEntityForeign(), subQuery));
		return montarOperadorRestricaoSwitch(restricao, bean, subQuery);
	}

	protected static String montarOperadorRestricaoSwitch(Restricao restricao,
			EntitySqlMaker<?> bean, boolean subQuery) throws Exception {
		switch (restricao.getType()) {
		case IS_NULL:
			return restricao.getNmColuna() + " is null";
		case IS_NOT_NULL:
			return restricao.getNmColuna() + " is not null";
		case DIFERENTE_ATRIBUTO:
			return restricao.getNmColuna() + "<>"
					+ restricao.getNmColunaForeign();
		case DIFERENTE:
			return restricao.getNmColuna() + "<>"
					+ getFormatedValueToQuery(restricao.getValue());
		case IGUAL_ATRIBUTO:
			return restricao.getNmColuna() + "="
					+ restricao.getNmColunaForeign();
		case IGUAL:
			return restricao.getNmColuna() + "="
					+ getFormatedValueToQuery(restricao.getValue());
		case MAIOR_ATRIBUTO:
			return restricao.getNmColuna() + ">"
					+ restricao.getNmColunaForeign();
		case MAIOR:
			return restricao.getNmColuna() + ">"
					+ getFormatedValueToQuery(restricao.getValue());
		case MAIOR_IGUAL_ATRIBUTO:
			return restricao.getNmColuna() + ">="
					+ restricao.getNmColunaForeign();
		case MAIOR_IGUAL:
			return restricao.getNmColuna() + ">="
					+ getFormatedValueToQuery(restricao.getValue());
		case MENOR_ATRIBUTO:
			return restricao.getNmColuna() + "<"
					+ restricao.getNmColunaForeign();
		case MENOR:
			return restricao.getNmColuna() + "<"
					+ getFormatedValueToQuery(restricao.getValue());
		case MENOR_IGUAL_ATRIBUTO:
			return restricao.getNmColuna() + "<="
					+ restricao.getNmColunaForeign();
		case MENOR_IGUAL:
			return restricao.getNmColuna() + "<="
					+ getFormatedValueToQuery(restricao.getValue());
		case AND:
			montarRestricaoRecursiva(restricao, bean, subQuery);
			return "(" + restricao.getRestricao1().getRestricaoSql() + " AND "
					+ restricao.getRestricao2().getRestricaoSql() + ")";
		case OR:
			montarRestricaoRecursiva(restricao, bean, subQuery);
			return "(" + restricao.getRestricao1().getRestricaoSql() + " OR "
					+ restricao.getRestricao2().getRestricaoSql() + ")";
		case IN:
			return restricao.getNmColuna() + " in (" + restricao.getValue()
					+ ")";
		case NOT_IN:
			return restricao.getNmColuna() + " not in (" + restricao.getValue()
					+ ")";
		case LIKE:
			return restricao.getNmColuna() + " like " + restricao.getValue();
		case ILIKE:
			return restricao.getNmColuna() + " ilike " + restricao.getValue();
		case DISJUNCAO:
			montarRestricoes(bean, restricao.getDisjuncao().getList(), subQuery);
			return montarDisjuncao(restricao.getDisjuncao().getList());
		case CONJUNCAO:
			montarRestricoes(bean, restricao.getConjuncao().getList(), subQuery);
			return montarConjuncao(restricao.getConjuncao().getList());
		case SQL:
			return restricao.getRestricaoSql();
		case SUBQUERY_IS_NULL:
			return montarSubQuery(restricao.getRestricaoSubQuery(), bean,
					subQuery) + " is null";
		case SUBQUERY_IN:
			return restricao.getNmColuna()
					+ " in ("
					+ montarSubQuery(restricao.getRestricaoSubQuery(), bean,
							subQuery) + ")";
		case SUBQUERY_NOT_IN:
			return restricao.getNmColuna()
					+ " not in ("
					+ montarSubQuery(restricao.getRestricaoSubQuery(), bean,
							subQuery) + ")";
		case SUBQUERY_EQUAL:
			return montarSubQuery(restricao.getRestricaoSubQuery(), bean,
					subQuery)
					+ "="
					+ getFormatedValueToQuery(restricao.getValue());
		case SUBQUERY_EQUAL_PROPERTY:
			return restricao.getNmColuna()
					+ "="
					+ montarSubQuery(restricao.getRestricaoSubQuery(), bean,
							subQuery);
		case SUBQUERY_MENOR:
			return montarSubQuery(restricao.getRestricaoSubQuery(), bean,
					subQuery)
					+ "<"
					+ getFormatedValueToQuery(restricao.getValue());
		case SUBQUERY_MENOR_IGUAL:
			return montarSubQuery(restricao.getRestricaoSubQuery(), bean,
					subQuery)
					+ "<="
					+ getFormatedValueToQuery(restricao.getValue());
		case SUBQUERY_MAIOR:
			return montarSubQuery(restricao.getRestricaoSubQuery(), bean,
					subQuery)
					+ ">"
					+ getFormatedValueToQuery(restricao.getValue());
		case SUBQUERY_MAIOR_IGUAL:
			return montarSubQuery(restricao.getRestricaoSubQuery(), bean,
					subQuery)
					+ ">="
					+ getFormatedValueToQuery(restricao.getValue());
		default:
			return null;
		}
	}

	protected static void montarWhere(EntitySqlMaker<?> beanRaiz,
			EntitySqlMaker<?> bean, String path) {
		try {
			if (bean == null)
				return;

			for (Field field : SqlMakerReflection.getClass(bean)
					.getDeclaredFields()) {
				if (field.getAnnotation(Column.class) == null
						&& (field.getAnnotation(Join.class) != null || field
								.getAnnotation(JoinMultiplo.class) != null))
					montarWhere(beanRaiz,
							(EntitySqlMaker<?>) SqlMakerReflection.get(bean,
									field.getName()),
							path.trim().length() == 0 ? field.getName() : path
									+ "." + field.getName());
				else {
					if (field.getAnnotation(Column.class) != null) {
						String nmAtributo = field.getName();
						Object value = SqlMakerReflection.get(bean, nmAtributo);

						if (value == null)
							continue;

						if (value.getClass() == String.class) {
							if (((String) value).trim().length() > 0)
								beanRaiz.addRestricao(Restricao
										.ilike((path.trim().length() == 0 ? ""
												: path + ".") + nmAtributo,
												formataStringSemCaracterEspecial((String) value),
												MatchType.QUALQUER_LUGAR));
							else {
								try {
									SqlMakerReflection.set(beanRaiz,
											nmAtributo, null);
								} catch (Exception e) {
								}
							}
						} else {
							if ((value.getClass() == Long.class && ((Long) value) == 0L)
									|| (value.getClass() == Integer.class && ((Integer) value) == 0)
									|| (value.getClass() == Float.class && ((Float) value) == 0f))
								continue;

							beanRaiz.addRestricao(Restricao.equal((path.trim()
									.length() == 0 ? "" : path + ".")
									+ nmAtributo, value));
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("SelectQuery.montarWhere:");
			e.printStackTrace();
		}
	}

	private static void montarRestricaoRecursiva(Restricao restricao,
			EntitySqlMaker<?> bean, boolean subQuery) throws Exception {
		restricao.getRestricao1().setRestricaoSql(
				montarOperadorRestricao(restricao.getRestricao1(), bean,
						subQuery));
		restricao.getRestricao2().setRestricaoSql(
				montarOperadorRestricao(restricao.getRestricao2(), bean,
						subQuery));
	}

	private static String montarDisjuncao(List<Restricao> restricoes)
			throws Exception {
		StringBuilder string = new StringBuilder("(1<>1 ");
		for (Restricao restricao : restricoes)
			string.append("or " + restricao.getRestricaoSql() + " ");
		string.append(") ");
		return string.toString();
	}

	private static String montarConjuncao(List<Restricao> restricoes)
			throws Exception {
		StringBuilder string = new StringBuilder("(1=1 ");
		for (Restricao restricao : restricoes)
			string.append("and " + restricao.getRestricaoSql() + " ");
		string.append(") ");
		return string.toString();
	}

	private static String montarSubQuery(Restricao restricao,
			EntitySqlMaker<?> bean, boolean subQuery) throws Exception {
		EntitySqlMaker<?> entitySubQuery = restricao.getEntitySubQuery();
		restricao = restricao.getRestricaoSubQuery();
		if (restricao != null) {
			restricao.setNmColuna("s"
					+ getNmColuna(entitySubQuery, restricao.getPathAtributo(),
							restricao.getPathEntity(), subQuery));
			restricao.setNmColunaForeign(getNmColuna(bean,
					restricao.getPathAtributoForeign(),
					restricao.getPathEntityForeign(), subQuery));
			entitySubQuery.addRestricao(Restricao
					.sql(montarOperadorRestricaoSwitch(restricao, bean,
							subQuery)));
		}

		return "(" + SelectQuery.createSubQuery(entitySubQuery) + ") ";
	}

	protected static void montarTables(EntitySqlMaker<?> bean, boolean subQuery) {
		try {
			for (TableMapped table : bean.getTables()) {
				Object obj = table.getPathEntity().isEmpty() ? bean
						: SqlMakerReflection.newInstance(bean,
								table.getPathEntity());
				Class<?> beanClass = SqlMakerReflection.getClass(obj);
				Table annTable = beanClass.getAnnotation(Table.class);
				table.setPathTable(annTable.schema() + "." + annTable.nome());

				if (subQuery)
					table.setNmAlias("s" + table.getNmAlias());
			}
		} catch (Exception e) {
			System.out.println("SelectQuery.montarTables:");
			e.printStackTrace();
		}
	}

	protected static void montarColunas(EntitySqlMaker<?> bean, boolean subQuery) {
		try {
			if (bean.getColumns() == null || bean.getColumns().size() == 0)
				bean.setFields("*");

			for (ColumnMapped column : bean.getColumns()) {
				if (column.getFunctionType() == FunctionType.SQL)
					continue;

				String nmColuna = getNmColuna(bean, column.getPathAtributo(),
						column.getPathEntity(), subQuery);

				switch (column.getFunctionType()) {
				case ARRAY_TO_STRING:
					column.setNmColuna("ARRAY_TO_STRING(ARRAY_AGG(" + nmColuna
							+ "),',')");
					break;
				case ARRAY_TO_STRING_DISTINCT:
					String nmColunaAux = getNmColuna(bean,
							column.getPathAtributoAux(),
							column.getPathEntityAux(), subQuery);
					column.setNmColuna("ARRAY_TO_STRING(ARRAY_AGG(distinct("
							+ nmColuna + ") || ' - ' || (case when "
							+ nmColunaAux + " is null then 'EA' else "
							+ nmColunaAux + " end) ),',')");
					break;
				case DISTINCT:
					column.setNmColuna("distinct(" + nmColuna + ")");
					break;
				case COUNT:
					column.setNmColuna("count(" + nmColuna + ")");
					break;
				case COUNT_DISTINCT:
					column.setNmColuna("count(distinct(" + nmColuna + "))");
					break;
				case MAX:
					column.setNmColuna("max(" + nmColuna + ")");
					break;
				case MIN:
					column.setNmColuna("min(" + nmColuna + ")");
					break;
				case SUM:
					column.setNmColuna("sum(" + nmColuna + ")");
					break;
				case DATE_TRUNC_DAY:
					column.setNmColuna("date_trunc('day', " + nmColuna + ")");
					break;
				case DATE_TRUNC_MONTH:
					column.setNmColuna("date_trunc('month', " + nmColuna + ")");
					break;
				case DATE_TRUNC_YEAR:
					column.setNmColuna("date_trunc('year', " + nmColuna + ")");
					break;
				default:
					column.setNmColuna(nmColuna);
					break;
				}
			}
		} catch (Exception e) {
			System.out.println("SelectQuery.montarColunas:");
			e.printStackTrace();
		}
	}

	protected static void montarOrdenacao(EntitySqlMaker<?> bean,
			boolean subQuery) {
		try {
			for (OrderMapped order : bean.getOrdenacao())
				order.setNmColuna(getNmColuna(bean, order.getPathAtributo(),
						order.getPathEntity(), subQuery));
		} catch (Exception e) {
			System.out.println("SelectQuery.montarOrdenacao:");
			e.printStackTrace();
		}
	}

	protected static void FROM(StringBuilder query, EntitySqlMaker<?> bean,
			boolean subQuery) throws Exception {
		Table table = SqlMakerReflection.getClass(bean).getAnnotation(
				Table.class);
		if (subQuery)
			query.append("FROM " + table.schema() + "." + table.nome()
					+ " as sr ");
		else
			query.append("FROM " + table.schema() + "." + table.nome()
					+ " as r ");
		FROM_INNER(query, bean, subQuery);
	}

	protected static void FROM_INNER(StringBuilder query,
			EntitySqlMaker<?> bean, boolean subQuery) throws Exception {
		for (int i = 0; i < bean.getTables().size(); i++) {
			TableMapped table = bean.getTables().get(i);
			if (table.getPathEntity() == null
					|| table.getPathEntity().isEmpty())
				continue;

			Join annJoin = SqlMakerReflection.getField(bean,
					table.getPathEntity()).getAnnotation(Join.class);
			JoinMultiplo annJoinMultiplo = SqlMakerReflection.getField(bean,
					table.getPathEntity()).getAnnotation(JoinMultiplo.class);

			switch (table.getInner()) {
			case INNER:
				query.append("INNER JOIN ");
				break;
			case LEFT:
				query.append("LEFT JOIN ");
				break;
			case FULL:
				query.append("FULL JOIN ");
				break;
			case RIGHT:
				query.append("RIGHT JOIN ");
				break;
			}

			Restricao restricaoJoin;

			if (annJoin != null) {
				String foreign = table.getPathEntity();
				String local = (foreign.contains(".") ? foreign.substring(0,
						foreign.lastIndexOf(".")) + "." : "")
						+ annJoin.foreignKey();
				foreign = table.getPathEntity() + "." + annJoin.foreignId();
				restricaoJoin = Restricao.equalProperty(local, foreign);
			} else {
				String foreign1 = table.getPathEntity();
				String local1 = (foreign1.contains(".") ? foreign1.substring(0,
						foreign1.lastIndexOf(".")) + "." : "")
						+ annJoinMultiplo.foreignKey1();
				foreign1 = table.getPathEntity() + "."
						+ annJoinMultiplo.foreignId1();
				String foreign2 = table.getPathEntity();
				String local2 = (foreign2.contains(".") ? foreign2.substring(0,
						foreign2.lastIndexOf(".")) + "." : "")
						+ annJoinMultiplo.foreignKey2();
				foreign2 = table.getPathEntity() + "."
						+ annJoinMultiplo.foreignId2();

				restricaoJoin = Restricao.and(
						Restricao.equalProperty(local1, foreign1),
						Restricao.equalProperty(local2, foreign2));
			}

			if (table.getRestricao() == null)
				table.setRestricao(restricaoJoin);
			else
				table.setRestricao(Restricao.and(restricaoJoin,
						table.getRestricao()));
			table.getRestricao().setRestricaoSql(
					montarOperadorRestricao(table.getRestricao(), bean,
							subQuery));
			query.append(table.getPathTable() + " as " + table.getNmAlias()
					+ " on " + table.getRestricao().getRestricaoSql() + " ");
		}
	}

	protected static void WHERE(StringBuilder query, EntitySqlMaker<?> bean) {

		if (Validator.isValid(bean.getRestricoes())) {
			StringBuilder where = new StringBuilder("WHERE ");

			for (Restricao restricao : bean.getRestricoes()) {
				if (!where.toString().contains(restricao.getRestricaoSql()))
					where.append(restricao.getRestricaoSql() + " AND ");
			}

			query.append(where.substring(0, where.length() - 4));
		}
	}

	private static String formataStringSemCaracterEspecial(String value) {
		String val = (String) value;
		val = val.replaceAll("\"", "&#34;");
		val = val.replaceAll("%", "&#37;");
		val = val.replaceAll("�", "\'");
		val = val.replaceAll("\'", "&#39;");
		return val;
	}
}