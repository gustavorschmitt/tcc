package com.tcc.sqlMaker.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface Column {
	// nome da coluna no banco de dados
	String coluna();

	// nome do atributo referente a coluna
	String atributo() default "";

	// dsChave da messageSource
	String message() default "";

	String mascara() default "";

	// numero maximo de caracter
	// -1 significa que nao ha limite
	int maxLength() default -1;

	// numero minimo de caracter
	int minLength() default 0;

	// valor maximo da variavel
	// -1 significa que nao ha limite
	int maxValue() default -1;

	// valor minimo da variavel
	int minValue() default 0;

	// se eh chave primaria no banco de dados
	boolean id() default false;

	// se pode ser atualizada no comando SQL UPDATE
	boolean updatable() default true;
}