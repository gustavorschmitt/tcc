package com.tcc.sqlMaker.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;

import com.tcc.sqlMaker.annotations.Table;

public class SqlMakerReflection {
	public static Object newInstance(Object objeto) throws Exception {
		return objeto.getClass().newInstance();
	}

	public static Object newInstance(Object objeto, String nmAtributo) throws Exception {
		if (objeto != null && nmAtributo != null && !nmAtributo.trim().equals("")) {
			Object obj = objeto;

			String[] atributos = nmAtributo.contains(".") ? nmAtributo.split("\\.") : new String[] { nmAtributo };
			Method get;
			for (String atributo : atributos) {
				get = obj.getClass().getMethod("get" + atributo.substring(0, 1).toUpperCase() + atributo.substring(1), new Class[] {});
				Object objTemp = get.invoke(obj, new Object[] {});
				if (objTemp == null) {
					Method set = obj.getClass().getMethod("set" + atributo.substring(0, 1).toUpperCase() + atributo.substring(1), new Class[] { get.getReturnType() });
					objTemp = get.getReturnType().newInstance();
					set.invoke(obj, objTemp);
				}
				obj = objTemp;
			}

			return obj;
		} else
			return null;
	}

	public static Object get(Object objeto, String nmAtributo) throws Exception {
		if (objeto != null && nmAtributo != null && !nmAtributo.trim().equals("")) {
			Object obj = objeto;

			String[] atributos = nmAtributo.contains(".") ? nmAtributo.split("\\.") : new String[] { nmAtributo };
			Method get;
			for (String atributo : atributos) {
				get = obj.getClass().getMethod("get" + atributo.substring(0, 1).toUpperCase() + atributo.substring(1), new Class[] {});
				obj = get.invoke(obj, new Object[] {});
			}

			return obj;
		} else
			return null;
	}

	public static Long getLong(Object entity, String atributo) throws Exception {
		return (Long) get(entity, atributo);
	}

	public static Field getField(Object objeto, String nmAtributo) {
		try {
			if (objeto != null && nmAtributo != null && !nmAtributo.trim().equals("")) {
				Object obj = objeto;
				String[] atributos = nmAtributo.contains(".") ? nmAtributo.split("\\.") : new String[] { nmAtributo };

				for (int i = 0; i < atributos.length - 1; i++) {
					Method get = obj.getClass().getMethod("get" + atributos[i].substring(0, 1).toUpperCase() + atributos[i].substring(1), new Class[] {});
					obj = get.invoke(obj, new Object[] {});
					if (obj == null)
						obj = get.getReturnType().newInstance();
				}

				return getClass(obj).getDeclaredField(atributos[atributos.length - 1]);
			} else
				return null;
		} catch (Exception e) {
			System.err.println("Field SqlMakerReflection.getField(Object, String)");
			e.printStackTrace();
			return null;
		}
	}

	public static Class<?> getClass(Object bean) {
		return bean.getClass().getAnnotation(Table.class) == null ? bean.getClass().getSuperclass() : bean.getClass();
	}

	public static void set(Object objeto, String nmAtributo, Object valor) throws Exception {
		if (objeto != null && nmAtributo != null && !nmAtributo.trim().equals("")) {
			try {
				// Percorre o caminho at� o objeto que cont�m atributo que deve
				// ser setado
				String atributo;
				Object obj = objeto;
				if (nmAtributo.contains(".")) {
					// Obt�m o objeto que cont�m o atributo que ser� setado
					obj = get(objeto, nmAtributo.substring(0, nmAtributo.lastIndexOf(".")));
					if (obj == null)
						obj = newInstance(objeto, nmAtributo.substring(0, nmAtributo.lastIndexOf(".")));
					atributo = nmAtributo.substring(nmAtributo.lastIndexOf(".") + 1);
				} else
					atributo = nmAtributo;

				atributo = atributo.substring(0, 1).toUpperCase() + atributo.substring(1);

				// Obt�m o m�todo e seta o valor no atributo
				Method get = obj.getClass().getMethod("get" + atributo, new Class[] {});
				obj.getClass().getMethod("set" + atributo, new Class[] { get.getReturnType() }).invoke(obj, converteObjectToReturnType(get.getReturnType(), valor));
			} catch (Exception e) {
				throw e;
			}
		}
	}

	private static Object converteObjectToReturnType(Class<?> clazz, Object value) {
		if (value == null)
			return null;

		if (clazz == String.class)
			return (String) value;
		else if (clazz == Long.class) {
			if (value.getClass() == String.class)
				return Long.parseLong((String) value);
			else if (value.getClass() == Integer.class)
				return ((Integer) value).longValue();
			else
				return (Long) value;
		} else if (clazz == Short.class) {
			if (value.getClass() == String.class)
				return Short.parseShort((String) value);
			else
				return (Short) value;
		} else if (clazz == Integer.class) {
			if (value.getClass() == String.class)
				return Integer.parseInt((String) value);
			else
				return (Integer) value;
		} else if (clazz == Float.class) {
			if (value.getClass() == Double.class)
				return ((Double) value).floatValue();
		} else if (clazz == Boolean.class) {
			if (value.getClass() == String.class)
				return Boolean.parseBoolean((String) value);
			else
				return (Boolean) value;
		}
		if (clazz == Double.class) {
			if (value.getClass() == BigDecimal.class) {
				return ((BigDecimal) value).doubleValue();
			}
		}
		return value;
	}
}