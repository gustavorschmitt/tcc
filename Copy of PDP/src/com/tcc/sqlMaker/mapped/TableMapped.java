package com.tcc.sqlMaker.mapped;

import com.tcc.sqlMaker.type.JoinType;

public class TableMapped {
	private String pathEntity;

	private String pathTable;

	private String nmAlias;

	private Restricao restricao;

	private JoinType inner = JoinType.INNER;

	public String getPathEntity() {
		return pathEntity;
	}

	public void setPathEntity(String pathEntity) {
		this.pathEntity = pathEntity;
	}

	public String getPathTable() {
		return pathTable;
	}

	public void setPathTable(String pathTable) {
		this.pathTable = pathTable;
	}

	public String getNmAlias() {
		return nmAlias;
	}

	public void setNmAlias(String nmAlias) {
		this.nmAlias = nmAlias;
	}

	public Restricao getRestricao() {
		return restricao;
	}

	public void setRestricao(Restricao restricao) {
		this.restricao = restricao;
	}

	public JoinType getInner() {
		return inner;
	}

	public void setInner(JoinType inner) {
		this.inner = inner;
	}

	public boolean equals(Object obj) {
		String value = null;
		if (obj != null && ((TableMapped) obj).getPathEntity() != null)
			value = ((TableMapped) obj).getPathEntity();
		if (value != null && this.pathEntity.equals(value))
			return true;
		return false;
	}
}