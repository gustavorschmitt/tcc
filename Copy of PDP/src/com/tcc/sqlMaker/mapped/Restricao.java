package com.tcc.sqlMaker.mapped;

import java.util.List;

import com.tcc.sqlMaker.entity.EntitySqlMaker;
import com.tcc.sqlMaker.type.MatchType;
import com.tcc.sqlMaker.type.RestricaoType;

public class Restricao {
	private Disjuncao disjuncao;

	private Conjuncao conjuncao;

	private String nmColuna;

	private String nmColunaForeign;

	private String pathAtributo;

	private String pathAtributoForeign;

	private Object value;

	private RestricaoType type;

	private Restricao restricao1;

	private Restricao restricao2;

	private String restricaoSql;

	private EntitySqlMaker<?> entitySubQuery;

	private Restricao restricaoSubQuery;

	private Restricao() {
	}

	public String getNmColuna() {
		return nmColuna;
	}

	public void setNmColuna(String nmColuna) {
		this.nmColuna = nmColuna;
	}

	public String getPathAtributo() {
		return pathAtributo;
	}

	public void setPathAtributo(String pathAtributo) {
		this.pathAtributo = pathAtributo;
	}

	public String getNmColunaForeign() {
		return nmColunaForeign;
	}

	public void setNmColunaForeign(String nmColunaForeign) {
		this.nmColunaForeign = nmColunaForeign;
	}

	public String getPathAtributoForeign() {
		return pathAtributoForeign;
	}

	public void setPathAtributoForeign(String pathAtributoForeign) {
		this.pathAtributoForeign = pathAtributoForeign;
	}

	public RestricaoType getType() {
		return type;
	}

	public void setType(RestricaoType type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Restricao getRestricao1() {
		return restricao1;
	}

	public void setRestricao1(Restricao restricao1) {
		this.restricao1 = restricao1;
	}

	public Restricao getRestricao2() {
		return restricao2;
	}

	public void setRestricao2(Restricao restricao2) {
		this.restricao2 = restricao2;
	}

	public Restricao getRestricaoSubQuery() {
		return restricaoSubQuery;
	}

	public void setRestricaoSubQuery(Restricao restricaoSubQuery) {
		this.restricaoSubQuery = restricaoSubQuery;
	}

	public String getRestricaoSql() {
		return restricaoSql;
	}

	public void setRestricaoSql(String restricaoSql) {
		this.restricaoSql = restricaoSql;
	}

	public String getPathEntity() {
		if (pathAtributo == null)
			return "";
		if (pathAtributo.contains("."))
			return pathAtributo.substring(0, pathAtributo.lastIndexOf("."));
		return "";
	}

	public String getPathEntityForeign() {
		if (pathAtributoForeign == null)
			return "";
		if (pathAtributoForeign.contains("."))
			return pathAtributoForeign.substring(0,
					pathAtributoForeign.lastIndexOf("."));
		return "";
	}

	public Disjuncao getDisjuncao() {
		return disjuncao;
	}

	public void setDisjuncao(Disjuncao disjuncao) {
		this.disjuncao = disjuncao;
	}

	public Conjuncao getConjuncao() {
		return conjuncao;
	}

	public void setConjuncao(Conjuncao conjuncao) {
		this.conjuncao = conjuncao;
	}

	public EntitySqlMaker<?> getEntitySubQuery() {
		return entitySubQuery;
	}

	public void setEntitySubQuery(EntitySqlMaker<?> entitySubQuery) {
		this.entitySubQuery = entitySubQuery;
	}

	public boolean equals(Object obj) {
		if (obj != null
				&& this.pathAtributo
						.equals(((Restricao) obj).getPathAtributo()))
			return true;
		return false;
	}

	// ******************************************************************************************************
	// //
	// ******************************************* newRestricao
	// ********************************************* //
	// ******************************************************************************************************
	// //

	private static Restricao newRestricao(String atributo,
			String atributoForeign, Object valor, RestricaoType type) {
		Restricao restricao = new Restricao();
		restricao.setPathAtributo(atributo);
		restricao.setPathAtributoForeign(atributoForeign);
		restricao.setType(type);
		restricao.setValue(valor);
		return restricao;
	}

	private static Restricao newRestricao(Restricao restricao1,
			Restricao restricao2, RestricaoType type) {
		Restricao restricao = new Restricao();
		restricao.setRestricao1(restricao1);
		restricao.setRestricao2(restricao2);
		restricao.setType(type);
		return restricao;
	}

	private static Restricao newRestricaoDisjuncao() {
		Restricao restricao = new Restricao();
		restricao.setDisjuncao(new Disjuncao());
		restricao.setType(RestricaoType.DISJUNCAO);
		return restricao;
	}

	private static Restricao newRestricaoConjuncao() {
		Restricao restricao = new Restricao();
		restricao.setConjuncao(new Conjuncao());
		restricao.setType(RestricaoType.CONJUNCAO);
		return restricao;
	}

	private static Restricao newRestricaoSql(String restricaoSql) {
		Restricao restricao = new Restricao();
		restricao.setRestricaoSql(restricaoSql);
		restricao.setType(RestricaoType.SQL);
		return restricao;
	}

	private static Restricao newRestricao(String atributo, Object value,
			RestricaoType restricaoType, Restricao restricaoSubQuery) {
		Restricao restricao = new Restricao();
		restricao.setPathAtributo(atributo);
		restricao.setValue(value);
		restricao.setRestricaoSubQuery(restricaoSubQuery);
		restricao.setType(restricaoType);
		return restricao;
	}

	private static Restricao newRestricaoSubQuery(
			EntitySqlMaker<?> entitySubQuery, Restricao restricaoSubQuery) {
		Restricao restricao = new Restricao();
		restricao.setEntitySubQuery(entitySubQuery);
		restricao.setRestricaoSubQuery(restricaoSubQuery);
		restricao.setType(RestricaoType.SUBQUERY);
		return restricao;
	}

	// ******************************************************************************************************
	// //
	// ******************************************* RestricaoType
	// ******************************************** //
	// ******************************************************************************************************
	// //

	public static Restricao equal(String atributo, Object valor) {
		return newRestricao(atributo, null, valor, RestricaoType.IGUAL);
	}

	public static Restricao equalProperty(String atributo,
			String atributoForeign) {
		return newRestricao(atributo, atributoForeign, null,
				RestricaoType.IGUAL_ATRIBUTO);
	}

	public static Restricao equal(Restricao restricaoSubQuery, Object value) {
		return newRestricao(null, value, RestricaoType.SUBQUERY_EQUAL,
				restricaoSubQuery);
	}

	public static Restricao equalProperty(Restricao restricaoSubQuery,
			String atributo) {
		return newRestricao(atributo, null,
				RestricaoType.SUBQUERY_EQUAL_PROPERTY, restricaoSubQuery);
	}

	public static Restricao notEqual(String atributo, Object valor) {
		return newRestricao(atributo, null, valor, RestricaoType.DIFERENTE);
	}

	public static Restricao notEqualProperty(String atributo,
			String atributoForeign) {
		return newRestricao(atributo, atributoForeign, null,
				RestricaoType.DIFERENTE_ATRIBUTO);
	}

	public static Restricao greater(String atributo, Object valor) {
		return newRestricao(atributo, null, valor, RestricaoType.MAIOR);
	}

	public static Restricao greaterProperty(String atributo,
			String atributoForeign) {
		return newRestricao(atributo, atributoForeign, null,
				RestricaoType.MAIOR_ATRIBUTO);
	}

	public static Restricao greater(Restricao restricaoSubQuery, Object value) {
		return newRestricao(null, value, RestricaoType.SUBQUERY_MAIOR,
				restricaoSubQuery);
	}

	public static Restricao less(String atributo, Object valor) {
		return newRestricao(atributo, null, valor, RestricaoType.MENOR);
	}

	public static Restricao lessProperty(String atributo, String atributoForeign) {
		return newRestricao(atributo, atributoForeign, null,
				RestricaoType.MENOR_ATRIBUTO);
	}

	public static Restricao less(Restricao restricaoSubQuery, Object value) {
		return newRestricao(null, value, RestricaoType.SUBQUERY_MENOR,
				restricaoSubQuery);
	}

	public static Restricao greaterThan(String atributo, Object valor) {
		return newRestricao(atributo, null, valor, RestricaoType.MAIOR_IGUAL);
	}

	public static Restricao greaterThanProperty(String atributo,
			String atributoForeign) {
		return newRestricao(atributo, atributoForeign, null,
				RestricaoType.MAIOR_IGUAL_ATRIBUTO);
	}

	public static Restricao greaterThan(Restricao restricaoSubQuery,
			Object value) {
		return newRestricao(null, value, RestricaoType.SUBQUERY_MAIOR_IGUAL,
				restricaoSubQuery);
	}

	public static Restricao lessThan(String atributo, Object valor) {
		return newRestricao(atributo, null, valor, RestricaoType.MENOR_IGUAL);
	}

	public static Restricao lessThanProperty(String atributo,
			String atributoForeign) {
		return newRestricao(atributo, atributoForeign, null,
				RestricaoType.MENOR_IGUAL_ATRIBUTO);
	}

	public static Restricao lessThan(Restricao restricaoSubQuery, Object value) {
		return newRestricao(null, value, RestricaoType.SUBQUERY_MENOR_IGUAL,
				restricaoSubQuery);
	}

	public static Restricao isNull(Restricao restricaoSubQuery) {
		return newRestricao(null, null, RestricaoType.SUBQUERY_IS_NULL,
				restricaoSubQuery);
	}

	public static Restricao isNull(String atributo) {
		return newRestricao(atributo, null, null, RestricaoType.IS_NULL);
	}

	public static Restricao isNotNull(String atributo) {
		return newRestricao(atributo, null, null, RestricaoType.IS_NOT_NULL);
	}

	public static Restricao and(Restricao restricao1, Restricao restricao2) {
		return newRestricao(restricao1, restricao2, RestricaoType.AND);
	}

	public static Restricao or(Restricao restricao1, Restricao restricao2) {
		return newRestricao(restricao1, restricao2, RestricaoType.OR);
	}

	public static Restricao in(String atributo, List<?> fks) {
		return newRestricao(atributo, null, montarIn(fks), RestricaoType.IN);
	}

	public static Restricao in(String atributo, Long[] fks) {
		return newRestricao(atributo, null, montarIn(fks), RestricaoType.IN);
	}

	public static Restricao in(String atributo, String[] fks) {
		return newRestricao(atributo, null, montarIn(fks), RestricaoType.IN);
	}

	public static Restricao in(String atributo, Restricao restricaoSubQuery) {
		return newRestricao(atributo, null, RestricaoType.SUBQUERY_IN,
				restricaoSubQuery);
	}

	public static Restricao notIn(String atributo, List<?> fks) {
		return newRestricao(atributo, null, montarIn(fks), RestricaoType.NOT_IN);
	}

	public static Restricao notIn(String atributo, Long[] fks) {
		return newRestricao(atributo, null, montarIn(fks), RestricaoType.NOT_IN);
	}

	public static Restricao notIn(String atributo, String[] fks) {
		return newRestricao(atributo, null, montarIn(fks), RestricaoType.NOT_IN);
	}

	public static Restricao notIn(String atributo, Restricao restricaoSubQuery) {
		return newRestricao(atributo, null, RestricaoType.SUBQUERY_NOT_IN,
				restricaoSubQuery);
	}

	public static Restricao like(String atributo, String value) {
		return newRestricao(atributo, null,
				montarMatch(value, MatchType.QUALQUER_LUGAR),
				RestricaoType.LIKE);
	}

	public static Restricao like(String atributo, String value,
			MatchType matchType) {
		return newRestricao(atributo, null, montarMatch(value, matchType),
				RestricaoType.LIKE);
	}

	public static Restricao ilike(String atributo, String value) {
		return newRestricao(atributo, null,
				montarMatch(value, MatchType.QUALQUER_LUGAR),
				RestricaoType.ILIKE);
	}

	public static Restricao ilike(String atributo, String value,
			MatchType matchType) {
		return newRestricao(atributo, null, montarMatch(value, matchType),
				RestricaoType.ILIKE);
	}

	public static Restricao sql(String restricaoSql) {
		return newRestricaoSql(restricaoSql);
	}

	public static Restricao subQuery(EntitySqlMaker<?> entitySubQuery) {
		return newRestricaoSubQuery(entitySubQuery, null);
	}

	public static Restricao subQuery(EntitySqlMaker<?> entitySubQuery,
			Restricao restricaoSubQuery) {
		return newRestricaoSubQuery(entitySubQuery, restricaoSubQuery);
	}

	private static String montarIn(Long[] fks) {
		String stringFksArea = "";
		if (fks == null || fks.length == 0)
			return "0";

		for (Long fkLong : fks)
			stringFksArea += fkLong + ", ";
		return stringFksArea.substring(0, stringFksArea.length() - 2);
	}

	private static String montarIn(String[] fks) {
		String stringFksArea = "";
		if (fks == null || fks.length == 0)
			return "'----'";

		for (String fkString : fks)
			stringFksArea += "'" + fkString + "', ";
		return stringFksArea.substring(0, stringFksArea.length() - 2);
	}

	private static String montarIn(List<?> fks) {
		String stringFksObj = "";
		if (fks == null || fks.size() == 0)
			return "'----'";

		for (Object fkObj : fks) {
			if (fkObj instanceof String)
				stringFksObj += "'" + fkObj + "', ";
			else
				stringFksObj += fkObj + ", ";
		}

		return stringFksObj.substring(0, stringFksObj.length() - 2);
	}

	private static String montarMatch(String value, MatchType matchType) {
		value = formataStringSemCaracterEspecial(value);
		switch (matchType) {
		case COMECANDO_COM:
			return "'" + value + "%'";
		case QUALQUER_LUGAR:
			return "'%" + value + "%'";
		case TERMINANDO_COM:
			return "'%" + value + "'";
		case EXATO:
			return "'" + value + "'";
		default:
			return "'" + value + "'";
		}
	}

	// ******************************************************************************************************
	// //
	// ********************************************* Disjuncao
	// ********************************************** //
	// ******************************************************************************************************
	// //

	public static Restricao disjuncao() {
		return newRestricaoDisjuncao();
	}

	public static Restricao conjuncao() {
		return newRestricaoConjuncao();
	}

	public Restricao add(Restricao restricao) {
		if (this.disjuncao != null)
			this.disjuncao.add(restricao);
		else if (this.conjuncao != null)
			this.conjuncao.add(restricao);
		return this;
	}

	private static String formataStringSemCaracterEspecial(String value) {
		String val = (String) value;
		val = val.replaceAll("\"", "&#34;");
		val = val.replaceAll("%", "&#37;");
		val = val.replaceAll("�", "\'");
		val = val.replaceAll("\'", "&#39;");
		return val;
	}
}