package com.tcc.config;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;

import javax.sql.DataSource;

import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;

import com.tcc.generic.entity.Validator;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class CustomMessageSource extends AbstractMessageSource implements
		BeanClassLoaderAware, Serializable {
	private static final long serialVersionUID = 1L;

	private Locale locale;

	private boolean clearCache = false;

	private JdbcTemplate jdbcTemplate;

	private String[] basenames = new String[0];

	private ClassLoader bundleClassLoader;

	private ClassLoader beanClassLoader = ClassUtils.getDefaultClassLoader();

	/**
	 * Cache to hold loaded ResourceBundles. This Map is keyed with the bundle
	 * basename, which holds a Map that is keyed with the Locale and in turn
	 * holds the ResourceBundle instances. This allows for very efficient hash
	 * lookups, significantly faster than the ResourceBundle class's own cache.
	 */
	private final Map cachedResourceBundles = new HashMap();

	/**
	 * Cache to hold already generated MessageFormats. This Map is keyed with
	 * the ResourceBundle, which holds a Map that is keyed with the message
	 * code, which in turn holds a Map that is keyed with the Locale and holds
	 * the MessageFormat values. This allows for very efficient hash lookups
	 * without concatenated keys.
	 * 
	 * @see #getMessageFormat
	 */
	private final Map cachedBundleMessageFormats = new HashMap();

	public CustomMessageSource() {
		super();
	}

	public CustomMessageSource(DataSource dataSource) {
		super();
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * Set a single basename, following {@link java.util.ResourceBundle}
	 * conventions: essentially, a fully-qualified classpath location. If it
	 * doesn't contain a package qualifier (such as <code>org.mypackage</code>),
	 * it will be resolved from the classpath root.
	 * <p>
	 * Messages will normally be held in the "/lib" or "/classes" directory of a
	 * web application's WAR structure. They can also be held in jar files on
	 * the class path.
	 * <p>
	 * Note that ResourceBundle names are effectively classpath locations: As a
	 * consequence, the JDK's standard ResourceBundle treats dots as package
	 * separators. This means that "test.theme" is effectively equivalent to
	 * "test/theme", just like it is for programmatic
	 * <code>java.util.ResourceBundle</code> usage.
	 * 
	 * @see #setBasenames
	 * @see java.util.ResourceBundle#getBundle(String)
	 */
	public void setBasename(String basename) {
		setBasenames(new String[] { basename });
	}

	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	/**
	 * Set an array of basenames, each following
	 * {@link java.util.ResourceBundle} conventions: essentially, a
	 * fully-qualified classpath location. If it doesn't contain a package
	 * qualifier (such as <code>org.mypackage</code>), it will be resolved from
	 * the classpath root.
	 * <p>
	 * The associated resource bundles will be checked sequentially when
	 * resolving a message code. Note that message definitions in a
	 * <i>previous</i> resource bundle will override ones in a later bundle, due
	 * to the sequential lookup.
	 * <p>
	 * Note that ResourceBundle names are effectively classpath locations: As a
	 * consequence, the JDK's standard ResourceBundle treats dots as package
	 * separators. This means that "test.theme" is effectively equivalent to
	 * "test/theme", just like it is for programmatic
	 * <code>java.util.ResourceBundle</code> usage.
	 * 
	 * @see #setBasename
	 * @see java.util.ResourceBundle#getBundle(String)
	 */
	public void setBasenames(String[] basenames) {
		if (basenames != null) {
			this.basenames = new String[basenames.length];
			for (int i = 0; i < basenames.length; i++) {
				String basename = basenames[i];
				Assert.hasText(basename, "Basename must not be empty");
				this.basenames[i] = basename.trim();
			}
		} else {
			this.basenames = new String[0];
		}
	}

	/**
	 * Set the ClassLoader to load resource bundles with.
	 * <p>
	 * Default is the containing BeanFactory's
	 * {@link org.springframework.beans.factory.BeanClassLoaderAware bean
	 * ClassLoader}, or the default ClassLoader determined by
	 * {@link org.springframework.util.ClassUtils#getDefaultClassLoader()} if
	 * not running within a BeanFactory.
	 */
	public void setBundleClassLoader(ClassLoader classLoader) {
		this.bundleClassLoader = classLoader;
	}

	/**
	 * Return the ClassLoader to load resource bundles with.
	 * <p>
	 * Default is the containing BeanFactory's bean ClassLoader.
	 * 
	 * @see #setBundleClassLoader
	 */
	protected ClassLoader getBundleClassLoader() {
		return (this.bundleClassLoader != null ? this.bundleClassLoader
				: this.beanClassLoader);
	}

	public void setBeanClassLoader(ClassLoader classLoader) {
		this.beanClassLoader = (classLoader != null ? classLoader : ClassUtils
				.getDefaultClassLoader());
	}

	/**
	 * Resolves the given message code as key in the registered resource
	 * bundles, returning the value found in the bundle as-is (without
	 * MessageFormat parsing).
	 */

	protected String resolveCodeWithoutArguments(String code,
			Locale localeBrowser) {
		loadLocale(localeBrowser);

		String result = null;
		Map bundle = getResourceBundle(this.basenames[0], locale);

		if (bundle != null)
			result = getStringOrNull(code, locale);

		return result;
	}

	/**
	 * Resolves the given message code as key in the registered resource
	 * bundles, using a cached MessageFormat instance per message code.
	 */

	protected MessageFormat resolveCode(String code, Locale localeBrowser) {
		loadLocale(localeBrowser);

		MessageFormat messageFormat = null;
		for (int i = 0; messageFormat == null && i < this.basenames.length; i++) {
			Map bundle = getResourceBundle(this.basenames[i], locale);
			if (bundle != null) {
				messageFormat = getMessageFormat(bundle, code, locale);
			}
		}
		return messageFormat;
	}

	/**
	 * Return a ResourceBundle for the given basename and code, fetching already
	 * generated MessageFormats from the cache.
	 * 
	 * @param basename
	 *            the basename of the ResourceBundle
	 * @param locale
	 *            the Locale to find the ResourceBundle for
	 * @return the resulting ResourceBundle, or <code>null</code> if none found
	 *         for the given basename and Locale
	 */
	protected Map getResourceBundle(String basename, Locale localeBrowser) {
		synchronized (this.cachedResourceBundles) {
			loadLocale(localeBrowser);

			if (this.clearCache) {
				this.clearCache = false;
				this.cachedResourceBundles.clear();
			}
			if (this.cachedResourceBundles == null
					|| this.cachedResourceBundles.isEmpty())
				this.cachedResourceBundles.putAll(doGetBundle(basename));

			return this.cachedResourceBundles;
		}
	}

	protected Map doGetBundle(String basename) throws MissingResourceException {
		List<Map<String, Object>> list = (List<Map<String, Object>>) jdbcTemplate
				.queryForList("Select * from " + basename);
		Map<String, Map> map = new HashMap<String, Map>();
		for (Map<String, Object> mapResultSet : list) {
			String dsChave = mapResultSet.get("ds_chave").toString();
			mapResultSet.remove("cd_id");
			mapResultSet.remove("ds_chave");
			map.put(dsChave, mapResultSet);
		}
		return map;
	}

	protected MessageFormat getMessageFormat(Map bundle, String code,
			Locale localeBrowser) throws MissingResourceException {
		synchronized (this.cachedBundleMessageFormats) {
			loadLocale(localeBrowser);

			if (this.clearCache) {
				this.clearCache = false;
				this.cachedBundleMessageFormats.clear();
			}
			Map codeMap = (Map) this.cachedBundleMessageFormats.get(bundle);
			Map localeMap = null;
			if (codeMap != null) {
				localeMap = (Map) codeMap.get(code);
				if (localeMap != null) {
					MessageFormat result = (MessageFormat) localeMap
							.get(locale);
					if (result != null) {
						return result;
					}
				}
			}

			String msg = getStringOrNull(code, locale);
			if (msg != null) {
				if (codeMap == null) {
					codeMap = new HashMap();
					this.cachedBundleMessageFormats.put(bundle, codeMap);
				}
				if (localeMap == null) {
					localeMap = new HashMap();
					codeMap.put(code, localeMap);
				}
				MessageFormat result = createMessageFormat(msg, locale);
				localeMap.put(locale, result);
				return result;
			}

			return null;
		}
	}

	private String getStringOrNull(String key, Locale localeBrowser) {
		try {
			loadLocale(localeBrowser);

			Map map = (Map) this.cachedResourceBundles.get(key);

			if (map != null && !map.isEmpty()) {
				String itemLocale = map.get("ds_item_"
						+ locale.toString().toLowerCase()) != null
						&& !map.get(
								"ds_item_" + locale.toString().toLowerCase())
								.equals("") ? map.get(
						"ds_item_" + locale.toString().toLowerCase())
						.toString() : null;

				String item = null;
				if (itemLocale == null || itemLocale.isEmpty()) {
					// TODO Guilherme - Quando o Usuario tiver na sessao, fazer
					// com que ele pegue o valor da sessao para por aqui
					// String localeDefaultProjetoLogado =
					// NPFWUtils.getCredencialStatic() != null
					// && NPFWUtils.getCredencialStatic().getProjetoLogado() !=
					// null
					// &&
					// NPFWUtils.getCredencialStatic().getProjetoLogado().getLocale()
					// != null ?
					// NPFWUtils.getCredencialStatic().getProjetoLogado().getLocale().getNmSigla()
					// : null;
					//
					// String itemDefault = localeDefaultProjetoLogado != null
					// && map.get("ds_item_" +
					// localeDefaultProjetoLogado.toLowerCase()) != null
					// && !map.get("ds_item_" +
					// localeDefaultProjetoLogado.toLowerCase()).equals("") ?
					// map.get("ds_item_" +
					// localeDefaultProjetoLogado.toLowerCase()).toString() :
					// map.get(
					// "ds_item_pt_br").toString();
					//
					// item = itemDefault.trim();

					item = map.get("ds_item_pt_br").toString().trim();
				} else
					item = itemLocale.trim();

				item = item.replaceAll("&#34;", "\"");
				item = item.replaceAll("&#37;", "%");
				item = item.replaceAll("&#39;", "\'");

				return item;
			}

			return null;
		} catch (MissingResourceException ex) {
			// Assume key not found
			// -> do NOT throw the exception to allow for checking parent
			// message source.
			return null;
		}
	}

	/**
	 * Show the configuration of this MessageSource.
	 */
	@Override
	public String toString() {
		return getClass().getName() + ": basenames=["
				+ StringUtils.arrayToCommaDelimitedString(this.basenames) + "]";
	}

	public void setClearCache(boolean clearCache) {
		this.clearCache = clearCache;
	}

	private void loadLocale(Locale localeBrowser) {
		locale = new Locale("pt_BR");
		// TODO Guilherme - Quando o Usuario tiver na sessao, fazer com que ele
		// pegue o valor da sessao para por aqui
		// if(localeBrowser != null || NPFWUtils.getDsIdiomaStatic() == null ||
		// NPFWUtils.getDsIdiomaStatic().trim().equals(""))
		// locale = localeBrowser;
		// else
		// locale = new Locale(NPFWUtils.getDsIdiomaStatic());
	}

	public static String resolverMensagem(String dsChave) {
		try {
			// TODO Guilherme - Quando o Usuario tiver na sessao, fazer com que
			// ele pegue o valor da sessao para por aqui
			return (Validator.isValid(dsChave)) ? CustomApplicationContextAware
					.getMessageSource().getMessage(dsChave, null,
							new Locale("pt_BR")) : null;
		} catch (Exception e) {
			return "Mensagem n�o encontrada no banco: " + dsChave;
		}
	}

	public static String resolverMensagem(String dsChave, String... params) {
		try {
			// TODO Guilherme - Quando o Usuario tiver na sessao, fazer com que
			// ele pegue o valor da sessao para por aqui
			String msg = null;
			if (Validator.isValid(dsChave)) {
				msg = CustomApplicationContextAware.getMessageSource()
						.getMessage(dsChave, null, new Locale("pt_BR"));
				if (params != null)
					for (int i = 0; i < params.length; i++)
						msg = msg.replace("{" + i + "}", params[i]);
			}
			return msg;
		} catch (Exception e) {
			return "Mensagem n�o encontrada no banco: " + dsChave;
		}
	}
}