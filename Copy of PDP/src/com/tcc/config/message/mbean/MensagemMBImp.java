package com.tcc.config.message.mbean;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.config.message.dao.MensagemDaoInterface;
import com.tcc.config.message.entity.dto.MensagemDto;
import com.tcc.generic.mbean.GenericMBImp;

@Service("mensagemMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class MensagemMBImp extends GenericMBImp<MensagemDto> implements
		MensagemMBInterface {
	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Override
	public MensagemDaoInterface getDaoBean() {
		return (MensagemDaoInterface) CustomApplicationContextAware
				.getBean("mensagemDao");
	}
}