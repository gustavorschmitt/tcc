package com.tcc.config.message.mbean;

import com.tcc.config.message.entity.dto.MensagemDto;
import com.tcc.generic.mbean.GenericMBInterface;

public interface MensagemMBInterface extends GenericMBInterface<MensagemDto> {
}