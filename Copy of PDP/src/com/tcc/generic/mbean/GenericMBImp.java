package com.tcc.generic.mbean;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tcc.generic.dao.GenericDaoInterface;
import com.tcc.generic.entity.GenericBean;
import com.tcc.generic.exception.NaoHaRegistroException;

public abstract class GenericMBImp<D extends GenericBean<?>> implements
		GenericMBInterface<D> {
	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	protected abstract GenericDaoInterface<D> getDaoBean();

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	private D newInstanceD(Long cdId) {
		D entity = newInstanceD();
		entity.setCdId(cdId);
		return entity;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	private D newInstanceD() {
		try {
			ParameterizedType paramType = (ParameterizedType) getClass()
					.getGenericSuperclass();
			@SuppressWarnings("unchecked")
			Class<D> clazz = (Class<D>) paramType.getActualTypeArguments()[0];
			return clazz.newInstance();
		} catch (Exception e) {
			System.err.println("GenericMBImpl.newInstanceD");
			return null;
		}
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public D salvar(D entity) throws Exception {
		return entity.getCdId() != null && entity.getCdId() != 0L ? getDaoBean()
				.atualizar(entity) : getDaoBean().salvar(entity);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void salvar(List<D> list) throws Exception {
		getDaoBean().salvar(list);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public D atualizar(D entity) throws Exception {
		return getDaoBean().atualizar(entity);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void atualizar(List<D> list) throws Exception {
		getDaoBean().atualizar(list);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean excluir(D entity) throws Exception {
		if (entity == null)
			return false;

		return getDaoBean().excluir(entity);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean excluir(Long cdId) throws Exception {
		return excluir(newInstanceD(cdId));
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void excluir(List<D> collection) throws Exception {
		getDaoBean().excluir(collection);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D recuperar(Long fkEntity) throws NaoHaRegistroException {
		D d = this.recuperar(this.newInstanceD(fkEntity));
		if (d == null)
			throw new NaoHaRegistroException();
		return d;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D recuperar(D entity) throws NaoHaRegistroException {
		try {
			D d = getDaoBean().recuperar(entity);
			if (d == null)
				throw new NaoHaRegistroException();
			return d;
		} catch (Exception e) {
			e.printStackTrace();
			throw new NaoHaRegistroException(e);
			
		}
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public final Long contarRegistros(D entity) throws Exception {
		return getDaoBean().contarRegistros(entity);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public final List<D> listar(D entity) throws Exception {
		return getDaoBean().listar(entity);
	}
}