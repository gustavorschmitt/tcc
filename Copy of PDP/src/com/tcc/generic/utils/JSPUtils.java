package com.tcc.generic.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class JSPUtils {
	/**
	 * Metodo que monta o endereco URL passando um pageContext, usado nos
	 * Services do sistema
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return String com a URL montada
	 */
	public static String getEnderecoUrl(HttpServletRequest request) {
		String getProtocol = request.getScheme();
		String getDomain = request.getServerName();
		String path = request.getContextPath();
		return getProtocol + "://" + getDomain + path;
	}

	/**
	 * Metodo que monta o endereco URL passando um pageContext, usado nas
	 * paginas JSP
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return String com a URL montada
	 */
	public static String getEnderecoUrl(PageContext pageContext) {
		return getEnderecoUrl((HttpServletRequest) pageContext.getRequest());
	}

	/**
	 * Metodo que monta o endereco URL passando um pageContext, usado nos
	 * Services do sistema. <br/>
	 * Este metodo adiciona o idGuia na url.
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R13</a>
	 *
	 * @return String com a URL montada
	 */
	public static String getEnderecoUrlComGuia(HttpServletRequest request) {
		String getProtocol = request.getScheme();
		String getDomain = request.getServerName();
		String path = request.getContextPath();
		return getProtocol + "://" + getDomain + path + "/"
				+ request.getAttribute("idGuia");
	}

	/**
	 * Metodo que monta o endereco URL passando um pageContext, usado nas
	 * paginas JSP. <br/>
	 * Este metodo adiciona o idGuia na url.
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737650">NKC-137-R13</a>
	 *
	 * @return String com a URL montada
	 */
	public static String getEnderecoUrlComGuia(PageContext pageContext) {
		return getEnderecoUrlComGuia((HttpServletRequest) pageContext
				.getRequest());
	}

	public static HttpServletRequest getUrl() {
		ServletRequestAttributes sra = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest req = sra.getRequest();
		return req;
	}
	public static String getEnderecoUrl() {
		HttpServletRequest request = getUrl();
		String getProtocol = request.getScheme();
		String getDomain = request.getServerName();
		String path = request.getContextPath();
		return getProtocol + "://" + getDomain + path;
	}
}