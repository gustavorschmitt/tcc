package com.tcc.generic.utils;

import org.herasaf.xacml.core.DDP.config.mbean.ConfigMBInterface;
import org.herasaf.xacml.core.DDP.peso.mbean.PesoMBInterface;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.usuario.mbean.UsuarioMBInterface;

public class MBUtils {

	public static UsuarioMBInterface getUsuarioMB() {
		return (UsuarioMBInterface) CustomApplicationContextAware
				.getBean("usuarioMB");
	}

	public static ConfigMBInterface getConfigMB() {
		return (ConfigMBInterface) CustomApplicationContextAware
				.getBean("configMB");
	}

	public static PesoMBInterface getPesoMB() {
		return (PesoMBInterface) CustomApplicationContextAware
				.getBean("pesoMB");
	}

}