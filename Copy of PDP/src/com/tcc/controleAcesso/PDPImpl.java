package com.tcc.controleAcesso;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import org.herasaf.xacml.core.DDP.politica.entity.dto.PoliticaDto;
import org.herasaf.xacml.core.DDP.politica.mbean.PoliticaMBInterface;
import org.herasaf.xacml.core.context.RequestCtx;
import org.herasaf.xacml.core.context.ResponseCtx;
import org.herasaf.xacml.core.policy.Evaluatable;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.utils.JSPUtils;
import com.tcc.usuario.service.ConexaoHTTP;

public class PDPImpl {

	public static String avaliar(String requestString, String politicaString) throws Exception {
		MyClass c = new MyClass();
		c.pdpInitMethodCustom();
		requestString = requestString.trim();

		System.out.println("AVALIACAO");
		System.out.println("*************************************");
		System.out.println(requestString);
		System.out.println("*************************************");
		System.out.println(politicaString);
		System.out.println("*************************************");

		List<PoliticaDto> listaPolitica = ((PoliticaMBInterface) CustomApplicationContextAware.getBean("politicaMB")).listar(new PoliticaDto());
		ArrayList<ByteArrayInputStream> listaPoliticaString = new ArrayList<ByteArrayInputStream>();
		for (PoliticaDto politicaDto : listaPolitica) {
			listaPoliticaString.add(new ByteArrayInputStream(politicaDto.getTxPolitica().getBytes()));
		}

		Evaluatable eval = c.unmarshalPolicy(listaPoliticaString);

		RequestCtx request = c.unmarshalRequest(new ByteArrayInputStream(requestString.getBytes()));

		c.deployPolicy(eval);
		ResponseCtx resposta = c.evaluateRequest(request);
		resposta.marshal(System.out);
		return resposta.getResponse().getResults().get(0).getDecision().toString();
	}

	public static String avaliar(String requestString) throws Exception {
		String policy = ConexaoHTTP.sendGet(JSPUtils.getEnderecoUrl() + "/js/IIA001Policy.xml", null);
		return avaliar(requestString, policy);
	}

}
