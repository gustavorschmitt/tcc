package com.tcc.controleAcesso;

import java.io.File;

import org.herasaf.xacml.core.context.RequestCtx;
import org.herasaf.xacml.core.context.ResponseCtx;
import org.herasaf.xacml.core.policy.Evaluatable;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gustavo
 */
public class TstHeras {

	private static final String POLICYFILE = "J:/Dropbox/Sistemas/Bolsa/XACML/conformance do heras/IIA001Policy.xml";
	private static final String REQUESTFILE = "j:/Dropbox/Sistemas/Bolsa/XACML/conformance do heras/IIA001Request.xml";

	// private static final String POLICYFILE =
	// "J:/Dropbox/Sistemas/TCC/Teste de caso/1/IIA001PolicyOriginal.xml";
	// private static final String REQUESTFILE =
	// "J:/Dropbox/Sistemas/TCC/Teste de caso/1/IIA001RequestOriginal.xml";

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) throws Exception {
		MyClass c = new MyClass();
		c.pdpInitMethodSimple();

		// Evaluatable eval = PolicyMarshaller.unmarshal(new File(POLICYFILE));
		// Evaluatable eval3 = PolicyMarshaller.unmarshal(new
		// File(POLICYFILE3));
		// Evaluatable eval4 = PolicyMarshaller.unmarshal(new
		// File(POLICYFILE4));
		// Evaluatable eval5 = PolicyMarshaller.unmarshal(new
		// File(POLICYFILE5));
		Evaluatable eval = c.unmarshalPolicy(new File(POLICYFILE));

		// RequestType request = RequestMarshaller.unmarshal(new
		// File(REQUESTFILE));
		RequestCtx request = c.unmarshalRequest(new File(REQUESTFILE));

		c.deployPolicy(eval);
		ResponseCtx resposta = c.evaluateRequest(request);
		resposta.marshal(System.out);
		// ResponseMarshaller.marshal(resposta, System.out);

		// System.out.println(c.repostas(resposta));

	}
}
