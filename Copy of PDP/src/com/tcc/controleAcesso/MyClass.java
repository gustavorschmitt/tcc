package com.tcc.controleAcesso;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.herasaf.xacml.core.SyntaxException;
import org.herasaf.xacml.core.api.PDP;
import org.herasaf.xacml.core.api.PolicyRepository;
import org.herasaf.xacml.core.api.PolicyRetrievalPoint;
import org.herasaf.xacml.core.api.UnorderedPolicyRepository;
import org.herasaf.xacml.core.context.RequestCtx;
import org.herasaf.xacml.core.context.RequestCtxFactory;
import org.herasaf.xacml.core.context.ResponseCtx;
import org.herasaf.xacml.core.policy.Evaluatable;
import org.herasaf.xacml.core.policy.EvaluatableID;
import org.herasaf.xacml.core.policy.PolicyMarshaller;
import org.herasaf.xacml.core.simplePDP.SimplePDPConfiguration;
import org.herasaf.xacml.core.simplePDP.SimplePDPFactory;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gustavo
 */
public class MyClass {

	public PDP simplePDP;
	private File f = new File("C:/IIB002Request2.xml");

	public void pdpInitMethodSimple() {
		/**
		 * Creates a simple PDP with a default root combining algorithm and a
		 * default policy repository. No PIP is used.
		 */
		simplePDP = SimplePDPFactory.getSimplePDP();
	}

	public void pdpInitMethodCustom() {
		SimplePDPConfiguration config = new SimplePDPConfiguration();

		simplePDP = SimplePDPFactory.getSimplePDP();
	}

	public void deployPolicy(Evaluatable policy) {

		/**
		 * Retrieve the policy repository and deploy the policy on it.
		 */
		PolicyRetrievalPoint repo = simplePDP.getPolicyRepository();
		UnorderedPolicyRepository repository = (UnorderedPolicyRepository) repo; // An
																					// OrderedPolicyRepository
																					// could
																					// be
																					// used
																					// as
																					// well
		repository.deploy(policy);
	}

	public void deployPolicies(List<Evaluatable> policies) {
		/**
		 * Deploy mulitple policies on the policy repository.
		 */
		PolicyRetrievalPoint repo = simplePDP.getPolicyRepository();
		UnorderedPolicyRepository repository = (UnorderedPolicyRepository) repo; // An
																					// OrderedPolicyRepository
																					// could
																					// be
																					// used
																					// as
																					// well
		repository.deploy(policies);
	}

	public void undeployPolicy(Evaluatable policy) {

		/**
		 * Retrieve the policy repository and undeploy the policy by its ID.
		 */
		PolicyRepository repo = (PolicyRepository) simplePDP.getPolicyRepository();
		repo.undeploy(policy.getId());

	}

	public void undeployPolicies(List<EvaluatableID> policyIds) {
		/**
		 * Undeploy multiple policies on the policy repository.
		 */
		PolicyRepository repo = (PolicyRepository) simplePDP.getPolicyRepository();
		repo.undeploy(policyIds);
	}

	public ResponseCtx evaluateRequest(RequestCtx request) {

		/**
		 * Evaluate the request using the simplePDP and retrieve the response
		 * from the PDP.
		 */
		ResponseCtx response = simplePDP.evaluate(request);
		return response;

	}

	public void marshalPolicy(Evaluatable policy) throws Exception {

		PolicyMarshaller.marshal(policy, System.out);

	}

	// public void marshalRequest(RequestType request) throws Exception {
	//
	// RequestMarshaller.marshal(request, System.out);
	//
	// }
	//
	// public String repostas(ResponseType r) {
	// File file = new File("C:/response.txt");
	// try {
	// ResponseMarshaller.marshal(r, file);
	// } catch (WritingException ex) {
	// System.out.println(ex.getMessage());
	// }
	//
	// return file.toString();
	//
	// }

	public RequestCtx unmarshalRequest(File arquivoq) throws SyntaxException {

		return RequestCtxFactory.unmarshal(arquivoq);

	}

	public RequestCtx unmarshalRequest(ByteArrayInputStream byteArrayInputStream) throws SyntaxException {

		return RequestCtxFactory.unmarshal(byteArrayInputStream);

	}

	public RequestCtx unmarshalRequest(InputStream input) throws SyntaxException {

		return RequestCtxFactory.unmarshal(input);

	}

	public Evaluatable unmarshalPolicy(File f) {
		Evaluatable eval = null;
		try {
			eval = PolicyMarshaller.unmarshal(f);
		} catch (SyntaxException ex) {
			System.out.println("Método unmarshallPolicy da classe MyClass");
			Logger.getLogger(MyClass.class.getName()).log(Level.SEVERE, null, ex);
		}
		return eval;

	}

	public Evaluatable unmarshalPolicy(ByteArrayInputStream byteArrayInputStream) {
		Evaluatable eval = null;
		try {
			eval = PolicyMarshaller.unmarshal(byteArrayInputStream);
		} catch (SyntaxException ex) {
			System.out.println("Método unmarshallPolicy da classe MyClass");
			Logger.getLogger(MyClass.class.getName()).log(Level.SEVERE, null, ex);
		}
		return eval;
	}

	public Evaluatable unmarshalPolicy(List<ByteArrayInputStream> listaByteArrayInputStream) {
		Evaluatable eval = null;
		try {
			for (ByteArrayInputStream byteArrayInputStream : listaByteArrayInputStream) {
				eval = PolicyMarshaller.unmarshal(byteArrayInputStream);
			}
		} catch (SyntaxException ex) {
			System.out.println("Método unmarshallPolicy da classe MyClass");
			Logger.getLogger(MyClass.class.getName()).log(Level.SEVERE, null, ex);
		}
		return eval;
	}
}
