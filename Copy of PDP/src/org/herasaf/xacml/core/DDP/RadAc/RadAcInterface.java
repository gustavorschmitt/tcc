package org.herasaf.xacml.core.DDP.RadAc;

public interface RadAcInterface {

	public boolean avaliarRisco();

	public Long calcularSecurityRisk() throws Exception;
	
	Long calcularRiscoOperacional();

}
