package org.herasaf.xacml.core.DDP.RadAc;

import java.util.List;

import org.herasaf.xacml.core.DDP.fator.entity.dto.FatorDto;
import org.herasaf.xacml.core.DDP.fator.mbean.FatorMBInterface;
import org.herasaf.xacml.core.DDP.peso.entity.dto.PesoDto;
import org.herasaf.xacml.core.DDP.politica.entity.dto.PoliticaDto;
import org.herasaf.xacml.core.DDP.politica.mbean.PoliticaMBInterface;
import org.springframework.stereotype.Component;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.exception.NaoHaRegistroException;
import com.tcc.generic.utils.MBUtils;
import com.tcc.sqlMaker.mapped.Restricao;
import com.tcc.usuario.entity.dto.UsuarioDto;
import com.tcc.usuario.mbean.UsuarioMBInterface;

@Component
public class RadAc {

	// public static boolean avaliarRisco() {
	//
	// try {
	// boolean radacDecision = true;
	// Long securityRisk = calcularSecurityRisk();
	// Long policyAcceptableSecurityRisk = 10l;
	// boolean policyRequiresVerificationOfOperationalNeed = true;
	//
	// // o risco � aceito pela pol�tica?
	// if (policyAcceptableSecurityRisk.longValue() > securityRisk.longValue())
	// {
	// // pol�tica necessita que verifique o risco operacional?
	// if (policyRequiresVerificationOfOperationalNeed) {
	// Long operationalNeed = 10l;
	// // necessidade operacional � suficiente para pol�tica?
	// if (operationalNeed.longValue() > securityRisk.longValue()) {
	// radacDecision = true;
	// } else {
	// radacDecision = true;
	// }
	// } else {
	// // pol�tica n�o necessita que verifique o risco operacional,
	// // portanto o acesso � garantido
	// radacDecision = true;
	// }
	//
	// } else {
	// radacDecision = false;
	// }
	// return radacDecision;
	// } catch (Exception e) {
	//
	// e.printStackTrace();
	// return false;
	// }
	// };
	//
	// public static Long calcularSecurityRisk() throws Exception {
	// // pegar valores do banco
	// Long pesoContexto = MBUtils.getPesoMB().recuperar(new
	// PesoDto("risco_contexto")).getNrPeso();
	// Long pesoconfidencialidadeIntegridadeDisponibilidade =
	// MBUtils.getPesoMB().recuperar(new
	// PesoDto("risco_confidencialidadeIntegridadeDisponibilidade")).getNrPeso();
	// Long pesoHistorico = MBUtils.getPesoMB().recuperar(new
	// PesoDto("risco_historico")).getNrPeso();
	//
	// // calcular valores
	// Long valorContexto = 0l;
	// Long valorIntegridadeConfidencialidadeIntegridade = 0l;
	// Long valorHistorico = 0l;
	//
	// Long securityRisk = (pesoContexto * valorContexto) +
	// (pesoconfidencialidadeIntegridadeDisponibilidade *
	// valorIntegridadeConfidencialidadeIntegridade) + (pesoHistorico *
	// valorHistorico);
	//
	// return securityRisk;
	// }
	//
	// public static Long calcularRiscoOperacional() {
	// // pegar valor estatico do banco
	// return 0l;
	// }
	//
	// public static Long calcularContexto() {
	// // pegar valor estatico do banco
	// return 0l;
	// }
	//
	// public static Long calcularConfidencialidadeIntegridadeDisponibilidade()
	// {
	// // pegar valor estatico do banco
	// return 0l;
	// }
	//
	// public static Long calcularHistorico() {
	// // pegar valor estatico do banco
	// return 0l;
	// }

	public boolean avaliarRisco(Long policyAcceptableSecurityRisk, Boolean policyRequiresVerificationOfOperationalNeed, Long securityRisk, Long operationalNeed, Boolean policyAllowsOperationalOverride) {

		try {
			boolean radacDecision = false;

			// o risco � aceito pela pol�tica?
			if (policyAcceptableSecurityRisk.longValue() > securityRisk.longValue()) {
				// pol�tica necessita que verifique o risco operacional?
				if (policyRequiresVerificationOfOperationalNeed) {

					// necessidade operacional � suficiente para pol�tica?
					if (operationalNeed.longValue() > securityRisk.longValue()) {
						radacDecision = true;
					} else {
						radacDecision = false;
					}
				} else {
					// pol�tica n�o necessita que verifique o risco operacional,
					// portanto o acesso � garantido
					radacDecision = true;
				}

			} else {
				// politica permite que a necessidade operacional sobrescreva o
				// risco de seguranca
				if (policyAllowsOperationalOverride) {
					// necessidade operacional � suficiente para pol�tica?
					if (operationalNeed.longValue() > securityRisk.longValue()) {
						radacDecision = true;
					} else {
						radacDecision = false;
					}
				} else
					radacDecision = false;
			}
			return radacDecision;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	};

	public boolean avaliarRisco() {
		try {
			PoliticaDto politicaFiltro = (PoliticaDto) CustomApplicationContextAware.getBean("politica");
			PoliticaDto politica = ((PoliticaMBInterface) CustomApplicationContextAware.getBean("politicaMB")).recuperar(politicaFiltro);
			Long securityRisk = calcularSecurityRisk();
			Long policyAcceptableSecurityRisk = politica.getNrRiscoAceitavel().longValue();
			boolean policyRequiresVerificationOfOperationalNeed = politica.getFlRequerVerificacao();
			Long operationalNeed = politica.getNrNecessidadeOperacional().longValue();
			boolean policyAllowsOperationalOverride = politica.getFlNecessidadeSobrescrever();

			return avaliarRisco(policyAcceptableSecurityRisk, policyRequiresVerificationOfOperationalNeed, securityRisk, operationalNeed, policyAllowsOperationalOverride);
		} catch (NaoHaRegistroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}

	public Long calcularSecurityRisk() {
		try {
			// pegar valores do banco
			Integer pesoContexto = MBUtils.getPesoMB().recuperar(new PesoDto("risco_contexto")).getNrPeso();
			Integer pesoconfidencialidadeIntegridadeDisponibilidade = MBUtils.getPesoMB().recuperar(new PesoDto("risco_confidencialidadeIntegridadeDisponibilidade")).getNrPeso();
			Integer pesoHistorico = MBUtils.getPesoMB().recuperar(new PesoDto("risco_historico")).getNrPeso();

			// calcular valores
			Long valorContexto = calcularContexto();
			Long valorIntegridadeConfidencialidadeIntegridade = calcularConfidencialidadeIntegridadeDisponibilidade();
			Long valorHistorico = calcularHistorico();
			Long securityRisk = (pesoContexto * valorContexto) + (pesoconfidencialidadeIntegridadeDisponibilidade * valorIntegridadeConfidencialidadeIntegridade) + (pesoHistorico * valorHistorico);

			System.out.println("**************CALCULO DO RISCO DE SEGURANCA******************");
			System.out.println("(" + pesoContexto + "*" + valorContexto + ")(" + pesoconfidencialidadeIntegridadeDisponibilidade + "*" + valorIntegridadeConfidencialidadeIntegridade + ") (" + pesoHistorico + "*" + valorHistorico
					+ ")");
			System.out.println((pesoContexto * valorContexto) + " + " + (pesoconfidencialidadeIntegridadeDisponibilidade * valorIntegridadeConfidencialidadeIntegridade) + " + " + (pesoHistorico * valorHistorico));
			System.out.println("TOTAL: " + securityRisk);
			System.out.println("*************************************************************");

			return securityRisk / 100;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 100l;
		}
	}

	public Long calcularRiscoOperacional() {
		// pegar valor eo do banco
		return 0l;
	}

	public Long calcularContexto() {
		try {
			PoliticaDto politica2 = (PoliticaDto) CustomApplicationContextAware.getBean("politica");
			PoliticaDto politicaDto = ((PoliticaMBInterface) CustomApplicationContextAware.getBean("politicaMB")).recuperar(politica2);
			FatorDto fatorDto = new FatorDto();
			fatorDto.addRestricao(Restricao.isNotNull("fkFator"));
			fatorDto.setFkPolitica(politicaDto.getCdId());
			List<FatorDto> listaFatores = ((FatorMBInterface) CustomApplicationContextAware.getBean("fatorMB")).listar(fatorDto);
			Double retorno = 0.0;
			for (FatorDto fatorDto2 : listaFatores) {
				retorno += fatorDto2.getNrPeso().doubleValue() * fatorDto2.getNrValor().doubleValue();
			}
			return retorno.longValue();
		} catch (NaoHaRegistroException e) {
			e.printStackTrace();
			return 0l;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0l;
		}
	}

	public Long calcularConfidencialidadeIntegridadeDisponibilidade() {
		return 250l;
	}

	public Long calcularHistorico() {
		UsuarioDto usuarioFiltro = (UsuarioDto) CustomApplicationContextAware.getBean("usuario");
		try {
			UsuarioDto usuario = ((UsuarioMBInterface) CustomApplicationContextAware.getBean("usuarioMB")).recuperar(usuarioFiltro);
			Long historico = usuario.getHistorico();
			return historico != null ? historico.longValue() * 100 : 0l;

		} catch (NaoHaRegistroException e) {
			try {
				usuarioFiltro = ((UsuarioMBInterface) CustomApplicationContextAware.getBean("usuarioMB")).salvar(usuarioFiltro);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return 0l;
		} catch (Exception e) {
			e.printStackTrace();
			return 0l;
		}

	}
}
