package org.herasaf.xacml.core.DDP;

import org.herasaf.xacml.core.DDP.config.entity.dto.ConfigDto;

import com.tcc.generic.exception.NaoHaRegistroException;
import com.tcc.generic.utils.MBUtils;

public class AlgoritmoDeCombinacao {

	private static boolean DenyOverrides(boolean xacml, boolean radac) {
		if (xacml && radac)
			return true;
		else
			return false;
	}

	private static boolean PermitOverrides(boolean xacml, boolean radac) {
		if (xacml || radac)
			return true;
		else
			return false;
	}

	private static boolean ABACPrecedence(boolean xacml, boolean radac) {
		if (!xacml)
			return false;
		else
			return true;
	}

	private static boolean RiskPrecedence(boolean xacml, boolean radac) {
		if (!radac)
			return false;
		else
			return true;
	}

	public static boolean agregar(boolean xacmlDecision, boolean radacDecision, boolean decisaoFinal) throws NaoHaRegistroException {
		Long algoritmoDecisao = new Long(MBUtils.getConfigMB().recuperar(new ConfigDto("algoritmoDecisao")).getNrConfig());
		AlgoritmoCombinacao.getByValor(algoritmoDecisao);

		AlgoritmoCombinacao Algortimo = AlgoritmoCombinacao.getByValor(algoritmoDecisao);
		switch (Algortimo) {
		case PERMITOVERRIDES:
			decisaoFinal = AlgoritmoDeCombinacao.PermitOverrides(xacmlDecision, radacDecision);
			break;
		case DENYOVERRIDES:
			decisaoFinal = AlgoritmoDeCombinacao.DenyOverrides(xacmlDecision, radacDecision);
			break;
		case ABACPRECEDENCE:
			decisaoFinal = AlgoritmoDeCombinacao.ABACPrecedence(xacmlDecision, radacDecision);
			break;
		case RISKPRECEDENCE:
			decisaoFinal = AlgoritmoDeCombinacao.RiskPrecedence(xacmlDecision, radacDecision);
			break;
		}
		return decisaoFinal;
	}

}
