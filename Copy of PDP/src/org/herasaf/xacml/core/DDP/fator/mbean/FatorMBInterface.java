package org.herasaf.xacml.core.DDP.fator.mbean;

import org.herasaf.xacml.core.DDP.fator.entity.dto.FatorDto;

import com.tcc.generic.mbean.GenericMBInterface;

public interface FatorMBInterface extends GenericMBInterface<FatorDto> {

}
