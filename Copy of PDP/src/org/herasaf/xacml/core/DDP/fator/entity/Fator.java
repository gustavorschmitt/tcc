package org.herasaf.xacml.core.DDP.fator.entity;

import org.herasaf.xacml.core.DDP.politica.entity.Politica;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Join;
import com.tcc.sqlMaker.annotations.Table;

@Table(schema = "public", nome = "fator")
public class Fator extends GenericBean<Fator> {

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "nm_fator", atributo = "nmFator", message = "entity")
	private String nmFator;

	@Column(coluna = "nr_peso", atributo = "nrPeso", message = "entity")
	private Double nrPeso;

	@Column(coluna = "nr_valor", atributo = "nrValor", message = "entity")
	private Double nrValor;

	@Column(coluna = "fk_fator", atributo = "fkFator", message = "entity")
	private Long fkFator;

	@Column(coluna = "fk_politica", atributo = "fkPolitica")
	private Long fkPolitica;

	@Join(foreignKey = "fkPolitica")
	private Politica politica;

	public Long getCdId() {
		return cdId;
	}

	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	public String getNmFator() {
		return nmFator;
	}

	public void setNmFator(String nmFator) {
		this.nmFator = nmFator;
	}

	public Double getNrPeso() {
		return nrPeso;
	}

	public void setNrPeso(Double nrPeso) {
		this.nrPeso = nrPeso;
	}

	public Double getNrValor() {
		return nrValor;
	}

	public void setNrValor(Double nrValor) {
		this.nrValor = nrValor;
	}

	public Long getFkFator() {
		return fkFator;
	}

	public void setFkFator(Long fkFator) {
		this.fkFator = fkFator;
	}

	public Long getFkPolitica() {
		return fkPolitica;
	}

	public void setFkPolitica(Long fkPolitica) {
		this.fkPolitica = fkPolitica;
	}

	public Politica getPolitica() {
		return politica;
	}

	public void setPolitica(Politica politica) {
		this.politica = politica;
	}

}
