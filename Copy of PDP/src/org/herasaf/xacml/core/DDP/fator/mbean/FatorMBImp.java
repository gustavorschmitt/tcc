package org.herasaf.xacml.core.DDP.fator.mbean;

import org.herasaf.xacml.core.DDP.fator.dao.FatorDaoInterface;
import org.herasaf.xacml.core.DDP.fator.entity.dto.FatorDto;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.mbean.GenericMBImp;

@Service("fatorMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class FatorMBImp extends GenericMBImp<FatorDto> implements FatorMBInterface {

	@Override
	public FatorDaoInterface getDaoBean() {
		return (FatorDaoInterface) CustomApplicationContextAware.getBean("fatorDao");
	}
}
