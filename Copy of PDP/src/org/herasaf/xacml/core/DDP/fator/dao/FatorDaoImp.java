package org.herasaf.xacml.core.DDP.fator.dao;

import org.herasaf.xacml.core.DDP.fator.entity.dto.FatorDto;
import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;

@Repository("fatorDao")
public class FatorDaoImp extends GenericDaoImp<FatorDto> implements FatorDaoInterface {

}
