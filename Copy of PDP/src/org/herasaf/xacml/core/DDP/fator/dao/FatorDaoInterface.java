package org.herasaf.xacml.core.DDP.fator.dao;

import org.herasaf.xacml.core.DDP.fator.entity.dto.FatorDto;

import com.tcc.generic.dao.GenericDaoInterface;

public interface FatorDaoInterface extends GenericDaoInterface<FatorDto> {

}
