package org.herasaf.xacml.core.DDP;

public enum AlgoritmoCombinacao {

	DENYOVERRIDES(1l), PERMITOVERRIDES(2l), ABACPRECEDENCE(3l), RISKPRECEDENCE(4l);
	public Long valor;

	AlgoritmoCombinacao(Long valor) {
		this.valor = valor;
	}

	public static AlgoritmoCombinacao getByValor(Long value) {
		for (AlgoritmoCombinacao e : AlgoritmoCombinacao.values()) {
			if (e.valor.intValue() == value.intValue()) {
				return e;
			}
		}
		return null;// not found
	}
}
