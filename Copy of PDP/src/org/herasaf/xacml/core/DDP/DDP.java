package org.herasaf.xacml.core.DDP;

import org.herasaf.xacml.core.DDP.RadAc.RadAc;
import org.springframework.beans.factory.annotation.Autowired;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.exception.NaoHaRegistroException;
import com.tcc.usuario.entity.dto.UsuarioDto;
import com.tcc.usuario.mbean.UsuarioMBInterface;

public class DDP {

	@Autowired
	private RadAc radac;

	public boolean avaliar(boolean xacmlDecision) throws NaoHaRegistroException {
		long currentTimeMillis1 = System.currentTimeMillis();

		boolean decisaoFinal = false;
		boolean radacDecision = radac.avaliarRisco();

		decisaoFinal = AlgoritmoDeCombinacao.agregar(xacmlDecision, radacDecision, decisaoFinal);

		long currentTimeMillis2 = System.currentTimeMillis();
		System.out.println("Tempo para avaliar no DDP" + ((currentTimeMillis2 - currentTimeMillis1) * 0.001));

		atualizarHistorico(decisaoFinal);
		return decisaoFinal;
	}

	public RadAc getRadac() {
		return radac;
	}

	public void setRadac(RadAc radac) {
		this.radac = radac;
	}

	private void atualizarHistorico(boolean decisaoFinal) {
		try {
			UsuarioDto usuarioFiltro = (UsuarioDto) CustomApplicationContextAware.getBean("usuario");
			UsuarioDto usuario = ((UsuarioMBInterface) CustomApplicationContextAware.getBean("usuarioMB")).recuperar(usuarioFiltro);
			Long valor = usuario.getHistorico() == null ? 0l : usuario.getHistorico();
			if (usuario.getHistorico() != null && (valor >= 0 && valor < 10))
				valor = valor.intValue() + (decisaoFinal == false ? 1l : -1l);

			usuario.setHistorico(valor);
			((UsuarioMBInterface) CustomApplicationContextAware.getBean("usuarioMB")).atualizar(usuario);
		} catch (NaoHaRegistroException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
