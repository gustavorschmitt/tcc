package org.herasaf.xacml.core.DDP.peso.dao;

import org.herasaf.xacml.core.DDP.peso.entity.dto.PesoDto;
import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;

@Repository("pesoDao")
public class PesoDaoImp extends GenericDaoImp<PesoDto> implements
		PesoDaoInterface {

}
