package org.herasaf.xacml.core.DDP.peso.entity;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Table;

@Table(schema = "public", nome = "peso")
public class Peso extends GenericBean<Peso> {

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "nm_peso", atributo = "nmPeso", message = "entity")
	private String nmPeso;

	@Column(coluna = "nr_peso", atributo = "nrPeso", message = "entity")
	private Integer nrPeso;

	@Column(coluna = "ds_identificador", atributo = "dsIdentificador")
	private String dsIdentificador;

	public Long getCdId() {
		return cdId;
	}

	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	public String getNmPeso() {
		return nmPeso;
	}

	public void setNmPeso(String nmPeso) {
		this.nmPeso = nmPeso;
	}

	public Integer getNrPeso() {
		return nrPeso;
	}

	public void setNrPeso(Integer nrPeso) {
		this.nrPeso = nrPeso;
	}

	public String getDsIdentificador() {
		return dsIdentificador;
	}

	public void setDsIdentificador(String dsIdentificador) {
		this.dsIdentificador = dsIdentificador;
	}

}
