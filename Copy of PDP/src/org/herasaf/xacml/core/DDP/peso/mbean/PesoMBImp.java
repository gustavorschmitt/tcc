package org.herasaf.xacml.core.DDP.peso.mbean;

import org.herasaf.xacml.core.DDP.peso.dao.PesoDaoInterface;
import org.herasaf.xacml.core.DDP.peso.entity.dto.PesoDto;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.mbean.GenericMBImp;

@Service("pesoMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class PesoMBImp extends GenericMBImp<PesoDto> implements PesoMBInterface {

	@Override
	public PesoDaoInterface getDaoBean() {
		return (PesoDaoInterface) CustomApplicationContextAware
				.getBean("pesoDao");
	}
}
