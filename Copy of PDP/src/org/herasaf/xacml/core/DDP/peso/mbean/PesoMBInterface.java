package org.herasaf.xacml.core.DDP.peso.mbean;

import org.herasaf.xacml.core.DDP.peso.entity.dto.PesoDto;

import com.tcc.generic.mbean.GenericMBInterface;

public interface PesoMBInterface extends GenericMBInterface<PesoDto> {

}
