package org.herasaf.xacml.core.DDP.peso.entity.dto;

import org.herasaf.xacml.core.DDP.peso.entity.Peso;

public class PesoDto extends Peso {

	public PesoDto() {

	}

	public PesoDto(String dsIdentificador) {
		setDsIdentificador(dsIdentificador);
	}

}
