package org.herasaf.xacml.core.DDP.peso.dao;

import org.herasaf.xacml.core.DDP.peso.entity.dto.PesoDto;

import com.tcc.generic.dao.GenericDaoInterface;

public interface PesoDaoInterface extends GenericDaoInterface<PesoDto> {

}
