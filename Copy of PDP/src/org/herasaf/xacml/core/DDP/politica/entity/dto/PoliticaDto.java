package org.herasaf.xacml.core.DDP.politica.entity.dto;

import org.herasaf.xacml.core.DDP.politica.entity.Politica;

public class PoliticaDto extends Politica {

	public PoliticaDto() {

	}

	public PoliticaDto(String dsIdentificador) {
		setDsIdentificador(dsIdentificador);
	}

}
