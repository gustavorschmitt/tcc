package org.herasaf.xacml.core.DDP.politica.entity;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Table;

@Table(schema = "public", nome = "politica")
public class Politica extends GenericBean<Politica> {

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "nm_politica", atributo = "nmPolitica", message = "entity")
	private String nmPolitica;

	@Column(coluna = "nr_politica", atributo = "nrPolitica", message = "entity")
	private Integer nrPolitica;

	@Column(coluna = "ds_identificador", atributo = "dsIdentificador")
	private String dsIdentificador;

	@Column(coluna = "tx_politica", atributo = "txPolitica")
	private String txPolitica;

	@Column(coluna = "nr_risco_aceitavel", atributo = "nrRiscoAceitavel")
	private Integer nrRiscoAceitavel;

	@Column(coluna = "fl_requer_verificacao", atributo = "flRequerVerificacao")
	private Boolean flRequerVerificacao;

	@Column(coluna = "nr_necessidade_operacional", atributo = "nrNecessidadeOperacional")
	private Integer nrNecessidadeOperacional;

	@Column(coluna = "fl_necessidade_Sobrescrever", atributo = "flNecessidadeSobrescrever")
	private Boolean flNecessidadeSobrescrever;

	public Long getCdId() {
		return cdId;
	}

	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	public String getNmPolitica() {
		return nmPolitica;
	}

	public void setNmPolitica(String nmPolitica) {
		this.nmPolitica = nmPolitica;
	}

	public Integer getNrPolitica() {
		return nrPolitica;
	}

	public void setNrPolitica(Integer nrPolitica) {
		this.nrPolitica = nrPolitica;
	}

	public String getDsIdentificador() {
		return dsIdentificador;
	}

	public void setDsIdentificador(String dsIdentificador) {
		this.dsIdentificador = dsIdentificador;
	}

	public String getTxPolitica() {
		return txPolitica;
	}

	public void setTxPolitica(String txPolitica) {
		this.txPolitica = txPolitica;
	}

	public Integer getNrRiscoAceitavel() {
		return nrRiscoAceitavel;
	}

	public void setNrRiscoAceitavel(Integer nrRiscoAceitavel) {
		this.nrRiscoAceitavel = nrRiscoAceitavel;
	}

	public Boolean getFlRequerVerificacao() {
		return flRequerVerificacao;
	}

	public void setFlRequerVerificacao(Boolean flRequerVerificacao) {
		this.flRequerVerificacao = flRequerVerificacao;
	}

	public Integer getNrNecessidadeOperacional() {
		return nrNecessidadeOperacional;
	}

	public void setNrNecessidadeOperacional(Integer nrNecessidadeOperacional) {
		this.nrNecessidadeOperacional = nrNecessidadeOperacional;
	}

	public Boolean getFlNecessidadeSobrescrever() {
		return flNecessidadeSobrescrever;
	}

	public void setFlNecessidadeSobrescrever(Boolean flNecessidadeSobrescrever) {
		this.flNecessidadeSobrescrever = flNecessidadeSobrescrever;
	}

}
