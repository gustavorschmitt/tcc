package org.herasaf.xacml.core.DDP.politica.mbean;

import org.herasaf.xacml.core.DDP.politica.dao.PoliticaDaoInterface;
import org.herasaf.xacml.core.DDP.politica.entity.dto.PoliticaDto;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.mbean.GenericMBImp;

@Service("politicaMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class PoliticaMBImp extends GenericMBImp<PoliticaDto> implements PoliticaMBInterface {

	@Override
	public PoliticaDaoInterface getDaoBean() {
		return (PoliticaDaoInterface) CustomApplicationContextAware.getBean("politicaDao");
	}
}
