package org.herasaf.xacml.core.DDP.politica.dao;

import org.herasaf.xacml.core.DDP.politica.entity.dto.PoliticaDto;

import com.tcc.generic.dao.GenericDaoInterface;

public interface PoliticaDaoInterface extends GenericDaoInterface<PoliticaDto> {

}
