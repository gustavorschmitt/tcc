package org.herasaf.xacml.core.DDP.politica.mbean;

import org.herasaf.xacml.core.DDP.politica.entity.dto.PoliticaDto;

import com.tcc.generic.mbean.GenericMBInterface;

public interface PoliticaMBInterface extends GenericMBInterface<PoliticaDto> {

}
