package org.herasaf.xacml.core.DDP.politica.dao;

import org.herasaf.xacml.core.DDP.politica.entity.dto.PoliticaDto;
import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;

@Repository("politicaDao")
public class PoliticaDaoImp extends GenericDaoImp<PoliticaDto> implements PoliticaDaoInterface {

}
