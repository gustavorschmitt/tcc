package org.herasaf.xacml.core.DDP.config.entity.dto;

import org.herasaf.xacml.core.DDP.config.entity.Config;

public class ConfigDto extends Config {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ConfigDto() {

	}

	public ConfigDto(String dsIdentificador) {
		setDsIdentificador(dsIdentificador);
	}
}
