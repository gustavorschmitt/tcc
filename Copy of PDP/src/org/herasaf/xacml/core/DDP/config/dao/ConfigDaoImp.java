package org.herasaf.xacml.core.DDP.config.dao;

import org.herasaf.xacml.core.DDP.config.entity.dto.ConfigDto;
import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;

@Repository("configDao")
public class ConfigDaoImp extends GenericDaoImp<ConfigDto> implements
		ConfigDaoInterface {
}