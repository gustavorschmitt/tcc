package org.herasaf.xacml.core.DDP.config.mbean;

import org.herasaf.xacml.core.DDP.config.entity.dto.ConfigDto;

import com.tcc.generic.mbean.GenericMBInterface;

public interface ConfigMBInterface extends GenericMBInterface<ConfigDto> {

}
