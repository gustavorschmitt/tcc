package org.herasaf.xacml.core.DDP.config.dao;

import org.herasaf.xacml.core.DDP.config.entity.dto.ConfigDto;

import com.tcc.generic.dao.GenericDaoInterface;

public interface ConfigDaoInterface extends GenericDaoInterface<ConfigDto> {
}