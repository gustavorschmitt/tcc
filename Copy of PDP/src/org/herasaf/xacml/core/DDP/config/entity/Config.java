package org.herasaf.xacml.core.DDP.config.entity;

import java.io.Serializable;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Table;

@Table(schema = "public", nome = "config")
public class Config extends GenericBean<Config> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "nm_config", atributo = "nmConfig", message = "entity")
	private String nmConfig;

	@Column(coluna = "nr_config", atributo = "nrConfig", message = "entity")
	private Integer nrConfig;

	@Column(coluna = "ds_identificador", atributo = "dsIdentificador")
	private String dsIdentificador;

	public Config() {
		super();
	}

	public Long getCdId() {
		return cdId;
	}

	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	public String getNmConfig() {
		return nmConfig;
	}

	public void setNmConfig(String nmConfig) {
		this.nmConfig = nmConfig;
	}

	public Integer getNrConfig() {
		return nrConfig;
	}

	public void setNrConfig(Integer nrConfig) {
		this.nrConfig = nrConfig;
	}

	public String getDsIdentificador() {
		return dsIdentificador;
	}

	public void setDsIdentificador(String dsIdentificador) {
		this.dsIdentificador = dsIdentificador;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
