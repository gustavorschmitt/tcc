package org.herasaf.xacml.core.DDP.config.mbean;

import org.herasaf.xacml.core.DDP.config.dao.ConfigDaoInterface;
import org.herasaf.xacml.core.DDP.config.entity.dto.ConfigDto;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.mbean.GenericMBImp;

@Service("configMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class ConfigMBImp extends GenericMBImp<ConfigDto> implements
		ConfigMBInterface {

	@Override
	public ConfigDaoInterface getDaoBean() {
		return (ConfigDaoInterface) CustomApplicationContextAware
				.getBean("configDao");
	}
}
