CREATE TABLE documento
(
cd_id bigserial,
nr_documento varchar,
nm_titulo varchar,

CONSTRAINT fk_documento PRIMARY KEY (cd_id)
);


CREATE TABLE usuario
(
cd_id bigserial,
nm_usuario varchar,
ds_email varchar,
ds_senha varchar,
fl_situacao integer,

CONSTRAINT fk_usuario PRIMARY KEY (cd_id)
);