<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<script src="<c:url value="/jsOLD/jquery/jquery-1.11.0.min.js"/>"></script>
    	 <script>
			$(function() {
				console.log('função')
				if (! sessionStorage.tabID)
					sessionStorage.tabID = Date.now();
				
// 				if(window.location.pathname.split('/')[2] == '' && window.location.pathname.split('/')[2] != sessionStorage.tabID)
// 					window.location.replace(window.location.origin + window.location.pathname+sessionStorage.tabID);
				
				$("a.formatHref").attr("href", function(index, oldValue) {
					return oldValue.replace('{idGuia}', sessionStorage.tabID);
				});
			});
		</script>
        <title>Pagina inicial</title>
       
    </head>

    <body>
		<div>
			Lista de usuarios <br />
            <c:forEach var="usuario" items="${listUsuarios}">
            	<a class="formatHref" href="<%= JSPUtils.getEnderecoUrl(pageContext) %>/{idGuia}/login/${usuario.cdId}">${usuario.nmUsuario} - ${usuario.flSituacaoString} - ${usuario.dsEmail} -</a> <br />
            </c:forEach>
        </div>
    </body>
</html>