<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">
	<link rel="stylesheet" href="<c:url value="/css/main.css"/>">
	<link rel="stylesheet" href="<c:url value="/css/utility.css"/>">
	<link rel="stylesheet" href="<c:url value="/js/_jReject/css/jquery.reject.css"/>">
	<link rel="stylesheet" href="<c:url value="/fonts/css/font-awesome.min.css"/>">
	<link rel="stylesheet" href="<c:url value="/js/_jReject/css/jquery.reject.css"/>">
	<!--[if IE 7]><link rel="stylesheet" href="<c:url value="/fonts/css/font-awesome-ie7.min.css"/>"><![endif]-->
	
	<!-- Java Scripts -->
	<!--[if lt IE 9]><script src="<c:url value="js/_browser/html5shiv.js"/>"></script><script src="<c:url value="js/_browser/respond.min.js"/>"></script><![endif]-->


	<style type="text/css">
		.formFieldError{ color:red; }
		.msgERRO{ background-color: red; }
		.msgALERTA{ background-color: yellow; }
		.msgSUCESSO{ background-color: green; }
	</style>
	<title>Venha fazer parte da rede |nomeNovoProduto|!</title>
	
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<form id="usuarioErroCadastrar" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/security/usuario/usuarioErroCadastrar" method="post">
					<table class="table table-striped table-hover">
						<tbody></tbody>
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('titulo_suporte')" /></td>
						</tr>	
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('titulo_telefone')" />: +55 48 3033-1001</td>
						</tr>	
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('titulo_skype')" />: suporte.tcc</td>
						</tr>	
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('titulo_email')" />: suporte@tcc.com.br</td>
						</tr>						
					</table>
					<table class="table table-striped table-hover">
						<tbody></tbody>
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('titulo_telefone')" />:</td>
							<td><input id="nrTelefone" type="text" name="nrTelefone"/></td>
						</tr>						
					</table>
					<input type="submit" value="Salvar"/>
				</form>
			</div>	
		</div>		
	</div>

</body>
</html>