<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
	<%@ include file="/WEB-INF/jsp/_fragment/head_old.jspf" %>

	<title>Convites</title>
	<%@ include file="conviteEditar_js.jspf" %>
</head>

<body> 
	<%@ include file="/WEB-INF/jsp/_fragment/header_old.jspf" %>
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div id="msg">
					${msg}
					<table id="msgTable"></table>
				</div>
				<h1>Página teste - Convidar usuário para ambiente</h1>
				<form id="conviteEditar" class="formatHref" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/convite/salvar" method="post">
					<table id="listUsuario" class="table table-striped table-hover">
						<thead>
							<tr>
								<td></td>
								<td><spring:eval expression="@messageSource.resolverMensagem('entity_usuario_nmUsuario')" /></td>
								<td><spring:eval expression="@messageSource.resolverMensagem('entity_usuario_dsEmail')" /></td>
								<td><spring:eval expression="@messageSource.resolverMensagem('sistema_entity_flSituacao')" /></td>
								<td><spring:eval expression="@messageSource.resolverMensagem('titulo_bloqueouAmbiente')" /></td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="usuario" items="${listUsuario}">
								<tr>
									<td><input  type="checkbox" name="convidados" value="${usuario.cdId}"/></td>
									<td>${usuario.nmUsuario}</td>
									<td>${usuario.dsEmail}</td>
									<td>${usuario.flSituacaoString}</td>
									<td>${usuario.respostaConvite.flSituacao == 7 ||usuario.respostaConvite.flSituacao == 9 }</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
						<h4>Texto adicional do convite</h4>
						<textarea placeholder="Gostaria que você aceitasse o convite para fazer parte do projeto XYZ." id="dsConvite" name="dsConvite" rows="10" cols="130">${convite.dsConvite}</textarea><br/>
						<div style="text-align: right; margin-top: 1%"><input type="submit" value="Convidar" /></div>
			  </form>
				  <table id="listRespostaConvite" class="table table-striped table-hover" style="empty-cells: show;">
					<thead>
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_convite_dtCadastramento')" /></td>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_convite_fkAnfitriao')" /></td>
							<td><spring:eval expression="@messageSource.resolverMensagem('sistema_entity_flSituacao')" /></td>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_respostaConvite_dtRespostaConvite')" /></td>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_usuario_nmUsuario')" /></td>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_usuario_dsEmail')" /></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="respostaConvite" items="${listRespostaConvite}">
							<tr>
								<td>${respostaConvite.convite.dtCadastramentoString}</td>
								<td>${respostaConvite.convite.anfitriao.nmUsuario}</td>
								<td>${respostaConvite.flSituacaoString}</td>
								<td>${respostaConvite.dtRespostaConviteString}</td>
								<td>${respostaConvite.convidado.nmUsuario}</td>
								<td>${respostaConvite.convidado.dsEmail}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>