<% /* NKC-99-R6, NKC-131-R9 */ %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<%@ include file="/WEB-INF/jsp/_fragment/head.jspf"%>
<title><spring:eval expression="@messageSource.resolverMensagem('titulo_nmProduto')" /></title>
</head>
<body>
	<header>
		<%@ include file="/WEB-INF/jsp/_fragment/header.jspf"%>
		<div class="clearfix"></div>
		<div class="container">
			<h3 class="page-header">Página Teste - Lista de usuários do ambiente</H3>
		</div>
	</header>	
		
	<div class="container conteudo">
		<div class="row">
			<div class="col-sm-9">
				<div id="msg">
					${msg}
					<table id="msgTable"></table>
				</div>
			
				<table class="table table-striped table-hover">
					<tbody>
						<tr>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_usuario_nmUsuario')" /></td>
							<td><spring:eval expression="@messageSource.resolverMensagem('entity_usuario_dsEmail')" /></td>
							<td>Vinculado?</td>
							<td>Situação último convite</td>
							<td>Data retorno último convite</td>
							<td>Grupos de ambiente</td>
						</tr>
					</tbody>
					<c:forEach var="usuario" items="${listUsuariosAmbiente}">
						<tr>
							<td>${usuario.nmUsuario}</td>
							<td>${usuario.dsEmail}</td>
							<td>${usuario.ambienteUsuario.cdId != null}</td>
							<td>${usuario.respostaConvite.flSituacaoString}</td>
							<td>${usuario.respostaConvite.dtRespostaConviteString}</td>
							<td>${usuario.listGrupoAmbienteString}</td>
						</tr>
					</c:forEach>
				</table>
				
				<br /><br />
				<c:choose>
					<c:when test="${podeConvidar}"><a class="formatHref" href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/convite">Link para página temporária de Convidar Usuários cadastrados</a></c:when>
				</c:choose>			
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/jsp/_fragment/footer.jspf"%>
</body>
</html>