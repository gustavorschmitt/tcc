<% /* NKC-131-R1 */ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
	<%@ include file="/WEB-INF/jsp/_fragment/head.jspf"%>
	<title><spring:eval expression="@messageSource.resolverMensagem('titulo_ambiente')" /></title>
</head>
<body>
	<header>
		<%@ include file="/WEB-INF/jsp/_fragment/header.jspf"%>
		<%@ include file="/WEB-INF/jsp/ambiente/grupoAmbiente_js.jsp"%>
	</header>
	
	<div class="container conteudo-interna">
		
		<div class="row">
			<div class="col-sm-9">
				<h4 class="page-header">
					<strong>Detalhes do ambiente</strong> <!-- TODO i18n -->
					<small>
						<ul class="pull-right list-unstyled list-inline" id="filter">
							<li><a class="formatHref" href="#">Informações</a> | </li>
							<li><a class="formatHref active" href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/ambiente/grupoAmbiente">Grupos</a> | </li>
							<li><a class="formatHref" href="#">Tramites de documento</a> | </li>
							<li>
								<div class="dropdown acao">
									<a id="nomeacao" data-toggle="dropdown"> Ações <i class="fa fa-caret-down"></i></a>
									<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="nomeacao">
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ação 1</a></li> <!-- TODO i18n -->
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ação 2</a></li> <!-- TODO i18n -->
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Ação 3</a></li> <!-- TODO i18n -->
									</ul>
								</div>
							</li>
						</ul>
					</small>
				</h4>
		
				<h3>${ambiente.nmAmbiente}</h3>
				<h4><small>${ambiente.dsDescricao}</small></h4>
				<div class="clearfix"></div>
				
				<div id="msg">
					${msg}
					<table id="msgTable"></table>
				</div>
				
				<form id="grupoNovo" class="formatHref" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/grupoAmbiente/novo" method="post">
					
					
				</form>
				
				<form id="grupoAtualizar" class="formatHref" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/grupoAmbiente/salvar" method="post">
					
					<c:forEach var="grupoAmbiente" items="${listGrupoAmbiente}">
						<div style="padding-bottom:10px;">
							<label>${grupoAmbiente.nmGrupoAmbiente}</label>
							<select id="grupoAmbiente_${grupoAmbiente.cdId}" name="grupoAmbiente_${grupoAmbiente.cdId}" class="chosen" multiple>
								<c:forEach var="ambienteUsuario" items="${grupoAmbiente.listAmbienteUsuario}">
									<option value="${ambienteUsuario.usuario.cdId}" ${ambienteUsuario.grupoAmbienteUsuario.cdId != null ? 'selected' : ''}>${ambienteUsuario.usuario.nmUsuario}</option>
								</c:forEach>
							</select>
						</div>
					</c:forEach>
					
					<table style="width:100%;">
						<tr>
							<td style="text-align: right;">
								<input type="button" value="Voltar" onclick="javascript:history.go(-1);"/>
								<input type="submit" value="Salvar" />
							</td>
						</tr>
					</table>
				</form>

			</div>
			
			<div class="col-sm-3"><%@ include file="/WEB-INF/jsp/_fragment/menuAcoesDocumento.jspf"%></div>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/jsp/_fragment/footer.jspf"%>
</body>
</html>