var app = {};

$(function() {

	
	app = {

		init: function() {
						
			// SVG FALLBACK
			app.svg();
			
			//INICIALIZA SELECT2
			app.initChosen();
			
			//INICIALIZA JREJECT
			app.rejeitarBrowser();

			//INICIALIZA TOOLTIPS
			app.initTooltip();

			//INICIALIZA DATEPICKER
			app.initDaterangepicker();
			
			//INICIALIZA WYISHTML5
			app.initWysihtml5();
			
			//FUNÇOES DE INTERFACE
			app.initInterface();
			
			//INICIALIZA ACORDEOM
			app.acordeom()
			
			//X-EDITABLE
			app.initEditable();
			
			//LAZY LOAD
			app.initInfiniteScroll();
			
			//TOGGLE FILTRO
			app.initTogglefiltro();
			
			//JQUERY UPLOAD
			//app.initUpload();
			
		},
		
		//JQUERY UPLOAD
		
		initUpload: function(){
			
				'use strict';
			
				// Initialize the jQuery File Upload widget:
				$('.upload').fileupload({
					dropZone: $('#dropzone'),
					autoUpload: true,
					url: 'js/file-upload/server/php/',
	
				});
                $('.upload').bind('fileuploadadd', function (e, data) {$(this).find('.arraste').addClass('out');})
                $('.upload').bind('fileuploaddestroyed', function (e, data) {
                        if($(this).find('.files').is(':empty')) {
                            $(this).find('.arraste').removeClass('out');
                        }
                })
				$(document).bind('dragover', function (e)
		                {
                    var dropZone = $('.arraste'),
                        foundDropzone,
                        timeout = window.dropZoneTimeout;
                        if (!timeout)
                        {
                            dropZone.addClass('in');
                        }
                        else
                        {
                            clearTimeout(timeout);
                        }
                        var found = false,
                        node = e.target;
                
                        do{
                
                            if ($(node).hasClass('arraste'))
                            {
                                found = true;
                                foundDropzone = $(node);
                                break;
                            }
                
                            node = node.parentNode;
                
                        }while (node != null);
                
                        dropZone.removeClass('in hover');
                
                        if (found)
                        {
                            foundDropzone.addClass('hover');
                        }
                
                        window.dropZoneTimeout = setTimeout(function ()
                        {
                            window.dropZoneTimeout = null;
                            dropZone.removeClass('in hover');
                        }, 100);
                        
		          });
		},
		
		// FUNÇOES TOGGLE FILTRO
		initTogglefiltro: function() {
			$('.toggle-filtro').click(function(){
				if ($(this).hasClass('fecha')){
					$('#filtro').animate({'left':-270});
					$(this).removeClass('fecha');
				}else{
					$('#filtro').animate({'left':0});
					$(this).addClass('fecha');	
				}
					
			})
		},
		
		// FUNÇOES DE INTERFACE
		initInterface: function() {
			
			$('.tramite-enviar').click(function(){
				$(this).addClass('ativo');
				$(this).siblings().removeClass('ativo');
				
			})
			
			
			$('input:radio[name="nova-versao"]').change(function(){
				if($(this).val() == 'sim'){
				   $('.nova-versao-ativo').slideDown();
				}else{
					$('.nova-versao-ativo').slideUp();
				}
			});
			
			
			$('input:checkbox.btn-acoes').change(function(){
				if($('input:checkbox.btn-acoes:checked').length > 0){
				   $('#acoes').animate({right:'0'})
				}else{
				   $('#acoes').animate({right:'-400px'})
				}
			});
			
            $('#salvar').click(function(){
                $('#acoes').animate({right:'0'})
            });
            $('#salvarFechar, #cancelar').click(function(){
                $('#acoes').animate({right:'-400px'})
            });
	
			$('.exclui').click(function(){
				$(this).parent().remove();	
			})
			
			$('#filter>li').on('click', 'a', function(){
				$("#filter .active").removeClass("active");
        		$(this).addClass("active");
			})
			
			
		},
		
		// SVG FALLBACK
		// toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script#update
		svg: function() {
			if(!Modernizr.svg) {
				$('img[src*="svg"]').attr('src', function() {
					return $(this).attr('src').replace('.svg', '.png');
				});
			}
		},
		
		
		//INFINITE-SCROLL
		//https://github.com/paulirish/infinite-scroll
		
		initInfiniteScroll: function(){
			
			$('#lista').infinitescroll({
				loading: {
				finishedMsg: "<em>Todos os documentos foram carregados!</em>",
				msgText: "<em>Carregando mais documentos!</em>",
				speed: 'slow',
				},
				animate: false,
				nextSelector: "#button-more a",
				navSelector: "#button-more",
				itemSelector: ".item",
				//debug		 	: true,
				dataType	 	: 'html',
				maxPage         : 3,
				path: function(index) { return "index" + index + ".html";},
				}, function(newElements, data, url){
					app.initInterface();
					app.initEditable();
					app.initUpload();
				});
		},
	
			
		// EDITABLE
		// http://vitalets.github.io/x-editable/demo-bs3.html#
		initEditable: function() {
		
			$.fn.editable.defaults.mode = 'inline';
			$('.editavel-inline').editable();
			
			$('.fa-pencil').click(function(e){    
			   e.stopPropagation();
			   $(this).prev('.editavel').editable('toggle');
			});
			$('.editavel').on('hidden', function(e, reason) {
				if(reason === 'save' || reason === 'cancel') {
					$(this).parent().find('.fa-pencil').css('opacity',1);
				} 
			});
			$('.editavel').on('shown', function(e, editable) {
					$(this).parent().find('.fa-pencil').css('opacity',0);
			});
						
			
		},
			
		// DATE PICKER & RANGER
		// https://github.com/dangrossman/bootstrap-daterangepicker
		initDaterangepicker: function() {
				
			$('.daterange').daterangepicker({
                    //startDate: moment().subtract('days', 29),
                    //endDate: moment(),
                    //minDate: '01/01/2012',
                    //maxDate: '31/12/2014',
                    //dateLimit: { days: 60 },
                    //showDropdowns: true,
                    showWeekNumbers: true,
                    //timePicker: false,
                    timePickerIncrement: 1,
                    //timePicker12Hour: true,
                    ranges: {
                       'Hoje': [moment(), moment()],
                       'Ontem': [moment().subtract('days', 1), moment().subtract('days', 1)],
                       'Últimos 7 dias': [moment().subtract('days', 6), moment()],
                       'Últimos 30 dias': [moment().subtract('days', 29), moment()],
                       'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                       'Mês Passado': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    //opens: 'left',
                    buttonClasses: ['btn btn-default'],
                    applyClass: 'btn-small btn-primary',
                    cancelClass: 'btn-small',
                    format: 'DD/MM/YYYY',
                    separator: ' até ',
                    locale: {
                        applyLabel: 'Enviar',
                        cancelLabel: 'Limpar',
                        fromLabel: 'De',
                        toLabel: 'Até',
                        customRangeLabel: 'Customizar',
                        daysOfWeek: ['D', 'S', 'T', 'Q', 'Q', 'S','S'],
                        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                        firstDay: 1
                    }
                  });
			  
		},
		
		
		acordeom: function () {
            $('.acordeom').collapse({
                toggle: false

            }).on('show.bs.collapse', function (e) {

                $(e.target).parent().find(".fa-plus").first().removeClass("fa-plus").addClass("fa-minus");


            }).on('hide.bs.collapse', function (e) {


                $(e.target).parent().find(".fa-minus").first().removeClass("fa-minus").addClass("fa-plus");

            });
        },
		
		
		
		//INICIALIZA WYSIHTML5
		//https://github.com/edicy/wysihtml5
		initWysihtml5: function(){
			
			
			var editors = [];

			  $('.ewrapper').each(function(idx, wrapper) {
				var e = new wysihtml5.Editor($(wrapper).find('.textarea').get(0), {
				  toolbar:        $(wrapper).find('.toolbar').get(0),
				  parserRules:    wysihtml5ParserRules
				  //showToolbarAfterInit: false
				});
				editors.push(e);
				
				e.on("showSource", function() {
				  alert(e.getValue(true));
				});
				
			  });
  
  
  
		},
		
		
		//INICIALIZA TOOLTIP
		//http://getbootstrap.com/javascript/#tooltips
		//http://getbootstrap.com/javascript/#popovers
		initTooltip: function() {
			$('[data-toggle=tooltip]').tooltip({html:true,container: 'body'});
			/*$("[data-toggle=popover]").popover({html:true,trigger:'click',container: 'body'});
			//fecha popover ao clicar fora dela
			$('body').on('click', function (e) {
				$('[data-toggle=popover]').each(function () {
					// hide any open popovers when the anywhere else in the body is clicked
					if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
						$(this).popover('hide');}
				});
			});*/
		},
		
		//INICIALIZA CHOSEN
		//https://github.com/harvesthq/chosen
		initChosen: function(){
			$('.chosen').chosen({
				no_results_text: 'Nenhum resultado encontrado',
				disable_search_threshold: 10,
			});
		},
		
		
		//ALERTA PARA BROWSERS ANTIGOS
		//http://jreject.turnwheel.com/
		rejeitarBrowser: function(){
		
			
	/* http://jreject.turnwheel.com/
		* Possibilities are endless... 
		* 
		* // MSIE Flags (Global, 5-8) 
		* msie, msie5, msie6, msie7, msie8, 
		* // Firefox Flags (Global, 1-3) 
		* firefox, firefox1, firefox2, firefox3, 
		* // Konqueror Flags (Global, 1-3) 
		* konqueror, konqueror1, konqueror2, konqueror3, 
		* // Chrome Flags (Global, 1-4) 
		* chrome, chrome1, chrome2, chrome3, chrome4, 
		* // Safari Flags (Global, 1-4) 
		* safari, safari2, safari3, safari4, 
		* // Opera Flags (Global, 7-10) 
		* opera, opera7, opera8, opera9, opera10, 
		* // Rendering Engines (Gecko, Webkit, Trident, KHTML, Presto) 
		* gecko, webkit, trident, khtml, presto, 
		* // Operating Systems (Win, Mac, Linux, Solaris, iPhone) 
		* win, mac, linux, solaris, iphone, 
		* unknown // Unknown covers everything else 
	*/
    $.reject({
                reject: {
                    all: false,
                    msie5: true,
                    msie6: true,
                    msie7: true,
                    msie8: true,
                    msie9: true
                },
                display: [],
                browserShow: true,
                browserInfo: {
                    firefox: {
                        text: 'Firefox 23',
                        url: 'http://www.mozilla.com/firefox/'
                    },
                    safari: {
                        text: 'Safari 6',
                        url: 'http://www.apple.com/safari/download/'
                    },
                    opera: {
                        text: 'Opera 16',
                        url: 'http://www.opera.com/download/'
                    },
                    chrome: {
                        text: 'Chrome 29',
                        url: 'http://www.google.com/chrome/'
                    },
                    msie: {
                        text: 'Internet Explorer 10',
                        url: 'http://www.microsoft.com/windows/Internet-explorer/'
                    },
                    gcf: {
                        text: 'Google Chrome Frame',
                        url: 'http://code.google.com/chrome/chromeframe/',
                        allow: {
                            all: false,
                            msie: true
                        }
                    }
                },

                header: 'Você sabia que seu browser esta desatualizado?',
                paragraph1: 'Seu browser esta desatualizado e pode não ser compatível com nosso website. Uma lista dos browsers mais populares podem ser encontrado abaixo.',
                paragraph2: 'Clique no icones para ir para a página de download do browser',
                close: true,
                closeMessage: 'Ao fechar esta janela, você reconhece que a sua experiência de uso neste site pode ser prejudicado',
                closeLink: 'Fechar esta janela',
                closeURL: '#',
                closeESC: true,

                closeCookie: false,
                cookieSettings: {
                    path: '/',
                    expires: 0
                },

                imagePath: '/js/jReject/images/',
                overlayBgColor: '#000',
                overlayOpacity: 0.8,

                fadeInTime: 'fast',
                fadeOutTime: 'fast',

                analytics: false
            });

            return false;
		},
	}
});




jQuery(document).ready(function(){
	
	app.init();
	
	
});