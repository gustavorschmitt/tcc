package com.tcc.generic.entity;

public enum TipoMensagem {
	ERRO, ALERTA, INFORMACAO, SUCESSO;
}