package com.tcc.generic.entity;

import java.util.Set;

import com.tcc.config.CustomMessageSource;

public class MensagemSistema {
	private TipoMensagem tipo;

	private boolean expandirColuna;

	private String resumo;

	private String detalhes;

	/*
	 * Construtor
	 * ***************************************************************
	 * ***********
	 * ***************************************************************
	 * *****************************************
	 */

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public MensagemSistema() {
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public MensagemSistema(TipoMensagem tipo, String resumo, String detalhes,
			String parametros, boolean expandirColuna) {
		setTipo(tipo);
		setResumo(CustomMessageSource.resolverMensagem(resumo));
		if (expandirColuna) {
			if (Validator.isValid(parametros))
				setResumo(getResumo().replace("{0}", parametros));
		} else if (Validator.isValid(detalhes)) {
			detalhes = CustomMessageSource.resolverMensagem(detalhes);
			if (Validator.isValid(parametros))
				detalhes = detalhes.replace("{0}", parametros);
		}
		setDetalhes(detalhes);
		setExpandirColuna(expandirColuna);
	}

	/*
	 * Factories
	 * ****************************************************************
	 * **********
	 * ****************************************************************
	 * *****************************************
	 */

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema Factory(TipoMensagem type, String resumo,
			String detalhes, String parametros, boolean expandirColuna) {
		return new MensagemSistema(type, resumo, detalhes, parametros,
				expandirColuna);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema ErrorFactory(String resumo) {
		return Factory(TipoMensagem.ERRO, resumo, null, null, true);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema ErrorFactory(String resumo, String detalhes) {
		return Factory(TipoMensagem.ERRO, resumo, detalhes, null, false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema ErrorFactory(String resumo, String detalhes,
			String parametros) {
		return Factory(TipoMensagem.ERRO, resumo, detalhes, parametros, false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema ErrorFactory(String resumo, String detalhes,
			String parametros, boolean expandirColuna) {
		return Factory(TipoMensagem.ERRO, resumo, detalhes, parametros,
				expandirColuna);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema WarningFactory(String resumo) {
		return Factory(TipoMensagem.ALERTA, resumo, null, null, true);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema WarningFactory(String resumo, String detalhes) {
		return Factory(TipoMensagem.ALERTA, resumo, detalhes, null, false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema WarningFactory(String resumo,
			String detalhes, String parametros) {
		return Factory(TipoMensagem.ALERTA, resumo, detalhes, parametros, false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema WarningFactory(String resumo,
			String detalhes, String parametros, boolean expandirColuna) {
		return Factory(TipoMensagem.ALERTA, resumo, detalhes, parametros,
				expandirColuna);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema SuccessFactory(String resumo) {
		return Factory(TipoMensagem.INFORMACAO, resumo, null, null, true);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema InfoFactory(String resumo, String detalhes) {
		return Factory(TipoMensagem.INFORMACAO, resumo, detalhes, null, false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema InfoFactory(String resumo, String detalhes,
			String parametros) {
		return Factory(TipoMensagem.INFORMACAO, resumo, detalhes, parametros,
				false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema InfoFactory(String resumo) {
		return Factory(TipoMensagem.SUCESSO, resumo, null, null, true);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema InfoFactory(String resumo,
			boolean expandirColuna) {
		return Factory(TipoMensagem.SUCESSO, resumo, null, null, expandirColuna);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema SuccessFactory(String resumo, String detalhes) {
		return Factory(TipoMensagem.SUCESSO, resumo, detalhes, null, false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema SuccessFactory(String resumo,
			boolean expandirColuna) {
		return Factory(TipoMensagem.SUCESSO, resumo, null, null, true);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema SuccessFactory(String resumo,
			String detalhes, boolean expandirColuna) {
		return Factory(TipoMensagem.SUCESSO, resumo, detalhes, null,
				expandirColuna);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema SuccessFactory(String resumo,
			String detalhes, String parametros) {
		return Factory(TipoMensagem.SUCESSO, resumo, detalhes, parametros,
				false);
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static MensagemSistema SuccessFactory(String resumo,
			String detalhes, String parametros, boolean expandirColuna) {
		return Factory(TipoMensagem.SUCESSO, resumo, detalhes, parametros,
				expandirColuna);
	}

	/*
	 * Metodos auxiliares
	 * *******************************************************
	 * *******************
	 * *******************************************************
	 * *****************************************
	 */

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public static boolean temMensagemErro(Set<MensagemSistema> listaMsg) {
		if (listaMsg == null)
			return false;

		listaMsg.remove(null);

		for (MensagemSistema sm : listaMsg)
			if (sm.getTipo() == TipoMensagem.ERRO)
				return true;
		return false;
	}

	/*
	 * Gets and Sets
	 * ************************************************************
	 * **************
	 * ************************************************************
	 * *****************************************
	 */

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public TipoMensagem getTipo() {
		return tipo;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void setTipo(TipoMensagem tipo) {
		this.tipo = tipo;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public String getResumo() {
		return resumo;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public String getDetalhes() {
		return detalhes;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public boolean isExpandirColuna() {
		return expandirColuna;
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void setExpandirColuna(boolean expandirColuna) {
		this.expandirColuna = expandirColuna;
	}
}