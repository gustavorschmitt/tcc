package com.tcc.generic.exception;

public class SemAcessoException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public SemAcessoException() {
	}

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public SemAcessoException(Exception e) {
	}
}