package com.tcc.generic.mbean;

import java.util.List;

import com.tcc.generic.entity.GenericBean;
import com.tcc.generic.exception.SemAcessoException;

public interface GenericMBInterface<D extends GenericBean<?>> {
	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D salvar(D entity) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void salvar(List<D> list) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D atualizar(D entity) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void atualizar(List<D> list) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public boolean excluir(D entity) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public boolean excluir(Long cdId) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public void excluir(List<D> entity) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D recuperar(Long fkEntity) throws SemAcessoException;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public D recuperar(D entity) throws SemAcessoException;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public Long contarRegistros(D entity) throws Exception;

	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 *
	 * @return MensagemDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public List<D> listar(D entity) throws Exception;
}