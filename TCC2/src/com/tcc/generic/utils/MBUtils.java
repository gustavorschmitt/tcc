package com.tcc.generic.utils;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.newKeepControl.documento.mbean.DocumentoMBInterface;
import com.tcc.usuario.mbean.UsuarioMBInterface;

public class MBUtils {

	public static UsuarioMBInterface getUsuarioMB() {
		return (UsuarioMBInterface) CustomApplicationContextAware
				.getBean("usuarioMB");
	}

	public static DocumentoMBInterface getDocumentoMB() {
		return (DocumentoMBInterface) CustomApplicationContextAware
				.getBean("documentoMB");
	}

}