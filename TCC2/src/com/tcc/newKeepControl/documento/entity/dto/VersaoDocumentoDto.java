package com.tcc.newKeepControl.documento.entity.dto;

import java.io.Serializable;

import com.tcc.newKeepControl.documento.entity.VersaoDocumento;

public class VersaoDocumentoDto extends VersaoDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Atributos sobrescritos da Entity
	 * *****************************************
	 * *********************************
	 * *****************************************
	 * *****************************************
	 */

	private DocumentoDto documento;

	/*
	 * Atributos auxiliares
	 * *****************************************************
	 * *********************
	 * *****************************************************
	 * *****************************************
	 */

	/*
	 * Construtores
	 * *************************************************************
	 * *************
	 * *************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public VersaoDocumentoDto() {
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public VersaoDocumentoDto(Long cdId) {
		setCdId(cdId);
	}

	/*
	 * Factories
	 * ****************************************************************
	 * **********
	 * ****************************************************************
	 * *****************************************
	 */

	/*
	 * Metodos auxiliares para VISUALIZAR no JSP
	 * ********************************
	 * ******************************************
	 * ********************************
	 * ******************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public String getDtCadastramentoString() {
		return super.getDataString(getDtCadastramento());
	}

	/*
	 * Get's and Set's
	 * **********************************************************
	 * ****************
	 * **********************************************************
	 * ******************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public DocumentoDto getDocumento() {
		return documento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setDocumento(DocumentoDto documento) {
		this.documento = documento;
	}
}