package com.tcc.newKeepControl.documento.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Join;
import com.tcc.sqlMaker.annotations.Table;
import com.tcc.usuario.entity.Usuario;

/**
 * @see <a
 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737208">Comentario</a>
 */
@Table(schema = "public", nome = "comentario")
public class Comentario extends GenericBean<Comentario> implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Atributos da tabela
	 * ******************************************************
	 * ********************
	 * ******************************************************
	 * *****************************************
	 */

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "fk_versao_documento", atributo = "fkVersaoDocumento", updatable = false)
	private Long fkVersaoDocumento;

	@Column(coluna = "fk_ambiente", atributo = "fkAmbiente", updatable = false)
	private Long fkAmbiente;

	@Column(coluna = "ds_comentario", atributo = "dsComentario", message = "entity_comentario_dsComentario")
	private String dsComentario;

	@Column(coluna = "fk_cadastrante", atributo = "fkCadastrante", updatable = false)
	private Long fkCadastrante;

	@Column(coluna = "dt_cadastramento", atributo = "dtCadastramento", updatable = false)
	private Timestamp dtCadastramento;

	/*
	 * Referencias cruzadas
	 * *****************************************************
	 * *********************
	 * *****************************************************
	 * *****************************************
	 */

	@Join(foreignKey = "fkVersaoDocumento")
	private VersaoDocumento versaoDocumento;

	@Join(foreignKey = "fkCadastrante")
	private Usuario cadastrante;

	/*
	 * Atributos de projecoes
	 * ***************************************************
	 * ***********************
	 * ***************************************************
	 * *****************************************
	 */

	// usado nas listagens de versao de documento para exibir a lista dos
	// arquivos para cada versao
	private String listaArquivosString;

	/*
	 * Construtor
	 * ***************************************************************
	 * ***********
	 * ***************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public Comentario() {
	}

	/*
	 * Gets and Sets
	 * ************************************************************
	 * **************
	 * ************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	@Override
	public Long getCdId() {
		return cdId;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	@Override
	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public Long getFkVersaoDocumento() {
		return fkVersaoDocumento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public void setFkVersaoDocumento(Long fkVersaoDocumento) {
		this.fkVersaoDocumento = fkVersaoDocumento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public String getDsComentario() {
		return dsComentario;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public void setDsComentario(String dsComentario) {
		this.dsComentario = dsComentario;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public Long getFkCadastrante() {
		return fkCadastrante;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public void setFkCadastrante(Long fkCadastrante) {
		this.fkCadastrante = fkCadastrante;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public Timestamp getDtCadastramento() {
		return dtCadastramento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public void setDtCadastramento(Timestamp dtCadastramento) {
		this.dtCadastramento = dtCadastramento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public VersaoDocumento getVersaoDocumento() {
		return versaoDocumento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public void setVersaoDocumento(VersaoDocumento versaoDocumento) {
		this.versaoDocumento = versaoDocumento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public Usuario getCadastrante() {
		return cadastrante;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736760">NKC-52-R3</a>
	 */
	public void setCadastrante(Usuario cadastrante) {
		this.cadastrante = cadastrante;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R10</a>
	 */
	public String getListaArquivosString() {
		return listaArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R10</a>
	 */
	public void setListaArquivosString(String listaArquivosString) {
		this.listaArquivosString = listaArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R10</a>
	 */
	public String getListaarquivosstring() {
		return listaArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736776">NKC-128-R10</a>
	 */
	public void setListaarquivosstring(String listaArquivosString) {
		this.listaArquivosString = listaArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R2</a>
	 */
	public Long getFkAmbiente() {
		return fkAmbiente;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737393">NKC-134-R2</a>
	 */
	public void setFkAmbiente(Long fkAmbiente) {
		this.fkAmbiente = fkAmbiente;
	}
}