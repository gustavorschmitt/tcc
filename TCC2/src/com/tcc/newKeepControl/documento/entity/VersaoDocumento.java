package com.tcc.newKeepControl.documento.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Join;
import com.tcc.sqlMaker.annotations.Table;
import com.tcc.usuario.entity.Usuario;

/**
 * @see <a
 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114302">Versao
 *      do documento</a>
 */
@Table(schema = "public", nome = "versao_documento")
public class VersaoDocumento extends GenericBean<VersaoDocumento> implements
		Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Atributos da tabela
	 * ******************************************************
	 * ********************
	 * ******************************************************
	 * *****************************************
	 */

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "fk_documento", atributo = "fkDocumento", updatable = false)
	private Long fkDocumento;

	@Column(coluna = "fk_ambiente", atributo = "fkAmbiente", updatable = false)
	private Long fkAmbiente;

	@Column(coluna = "nr_versao", atributo = "nrVersao", message = "entity_versaoDocumento_nrVersao", maxLength = 20)
	private String nrVersao;

	@Column(coluna = "nr_sequencia", atributo = "nrSequencia", updatable = false)
	private Integer nrSequencia;

	@Column(coluna = "fk_cadastrante", atributo = "fkCadastrante", updatable = false)
	private Long fkCadastrante;

	@Column(coluna = "dt_cadastramento", atributo = "dtCadastramento", updatable = false)
	private Timestamp dtCadastramento;

	/*
	 * Referencias cruzadas
	 * *****************************************************
	 * *********************
	 * *****************************************************
	 * *****************************************
	 */

	@Join(foreignKey = "fkDocumento")
	private Documento documento;

	@Join(foreignKey = "fkCadastrante")
	private Usuario usuario;

	/*
	 * Atributos de projecoes
	 * ***************************************************
	 * ***********************
	 * ***************************************************
	 * *****************************************
	 */

	// usado nas listagens para apresentar se eh a ultima versao do documento.
	// (sim/nao)
	private Boolean ehUltima;

	// usado nas listagens de versao de documento para exibir a lista dos
	// arquivos para cada versao
	private String listArquivosString;

	/*
	 * Construtor
	 * ***************************************************************
	 * ***********
	 * ***************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public VersaoDocumento() {
	}

	/*
	 * Gets and Sets
	 * ************************************************************
	 * **************
	 * ************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	@Override
	public Long getCdId() {
		return cdId;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	@Override
	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public Long getFkDocumento() {
		return fkDocumento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setFkDocumento(Long fkDocumento) {
		this.fkDocumento = fkDocumento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public String getNrVersao() {
		return nrVersao;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setNrVersao(String nrVersao) {
		this.nrVersao = nrVersao;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public Integer getNrSequencia() {
		return nrSequencia;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setNrSequencia(Integer nrSequencia) {
		this.nrSequencia = nrSequencia;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public Long getFkCadastrante() {
		return fkCadastrante;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setFkCadastrante(Long fkCadastrante) {
		this.fkCadastrante = fkCadastrante;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public Timestamp getDtCadastramento() {
		return dtCadastramento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setDtCadastramento(Timestamp dtCadastramento) {
		this.dtCadastramento = dtCadastramento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public Documento getDocumento() {
		return documento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public Usuario getUsuario() {
		return usuario;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R5</a>
	 */
	public Boolean getEhUltima() {
		return ehUltima;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R5</a>
	 */
	public void setEhUltima(Boolean ehUltima) {
		this.ehUltima = ehUltima;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R5</a>
	 */
	public Boolean getEhultima() {
		return ehUltima;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R5</a>
	 */
	public void setEhultima(Boolean ehUltima) {
		this.ehUltima = ehUltima;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114153">NKC-90-R3</a>
	 */
	public Long getFkAmbiente() {
		return fkAmbiente;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114153">NKC-90-R3</a>
	 */
	public void setFkAmbiente(Long fkAmbiente) {
		this.fkAmbiente = fkAmbiente;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736717">NKC-87-R1</a>
	 */
	public String getListArquivosString() {
		return listArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736717">NKC-87-R1</a>
	 */
	public void setListArquivosString(String listArquivosString) {
		this.listArquivosString = listArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736717">NKC-87-R1</a>
	 */
	public String getListarquivosstring() {
		return listArquivosString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736717">NKC-87-R1</a>
	 */
	public void setListarquivosstring(String listArquivosString) {
		this.listArquivosString = listArquivosString;
	}
}