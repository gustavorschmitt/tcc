package com.tcc.newKeepControl.documento.entity;

import java.io.Serializable;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Table;

@Table(schema = "public", nome = "documento")
public class Documento extends GenericBean<Documento> implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "nr_documento", atributo = "nrDocumento", message = "entity_documento_nrDocumento", maxLength = 100)
	private String nrDocumento;

	@Column(coluna = "nm_titulo", atributo = "nmTitulo", message = "entity_documento_nmTitulo", maxLength = 250)
	private String nmTitulo;

	public Documento() {
	}

	@Override
	public Long getCdId() {
		return this.cdId;
	}

	@Override
	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	public String getNrDocumento() {
		return nrDocumento;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public String getNmTitulo() {
		return nmTitulo;
	}

	public void setNmTitulo(String nmTitulo) {
		this.nmTitulo = nmTitulo;
	}

}