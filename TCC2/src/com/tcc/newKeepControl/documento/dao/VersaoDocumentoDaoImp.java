package com.tcc.newKeepControl.documento.dao;

import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;
import com.tcc.newKeepControl.documento.entity.dto.VersaoDocumentoDto;

@Repository("versaoDocumentoDao")
public class VersaoDocumentoDaoImp extends GenericDaoImp<VersaoDocumentoDto>
		implements VersaoDocumentoDaoInterface {
}