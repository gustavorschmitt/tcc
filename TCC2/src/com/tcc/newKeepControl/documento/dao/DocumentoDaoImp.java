package com.tcc.newKeepControl.documento.dao;

import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;
import com.tcc.newKeepControl.documento.entity.dto.DocumentoDto;

@Repository("documentoDao")
public class DocumentoDaoImp extends GenericDaoImp<DocumentoDto> implements
		DocumentoDaoInterface {
}