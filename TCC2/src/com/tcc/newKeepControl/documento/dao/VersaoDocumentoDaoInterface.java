package com.tcc.newKeepControl.documento.dao;

import com.tcc.generic.dao.GenericDaoInterface;
import com.tcc.newKeepControl.documento.entity.dto.VersaoDocumentoDto;

public interface VersaoDocumentoDaoInterface extends
		GenericDaoInterface<VersaoDocumentoDto> {
}