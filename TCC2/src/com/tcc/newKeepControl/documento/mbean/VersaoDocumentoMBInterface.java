package com.tcc.newKeepControl.documento.mbean;

import com.tcc.generic.mbean.GenericMBInterface;
import com.tcc.newKeepControl.documento.entity.dto.VersaoDocumentoDto;

public interface VersaoDocumentoMBInterface extends
		GenericMBInterface<VersaoDocumentoDto> {

}