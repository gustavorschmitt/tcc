package com.tcc.newKeepControl.documento.mbean;

import com.tcc.generic.mbean.GenericMBInterface;
import com.tcc.newKeepControl.documento.entity.dto.DocumentoDto;

public interface DocumentoMBInterface extends GenericMBInterface<DocumentoDto> {
	/**
	 * Metodo que valida se existe um ou mais documentos cadastrados com o mesmo
	 * nrDocumento
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114334">NKC-34-R4</a>
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114153">NKC-90-R10</a>
	 *
	 * @param nrDocumento
	 *            usado para fazer a selecao
	 * @param fkAmbiente
	 *            usado para fazer a selecao
	 *
	 * @return DocumentoDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	public boolean validarUnicoNrDocumentoNoAmbiente(String nrDocumento,
			Long fkAmbiente);
}