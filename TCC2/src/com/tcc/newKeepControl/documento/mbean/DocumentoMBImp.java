package com.tcc.newKeepControl.documento.mbean;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.mbean.GenericMBImp;
import com.tcc.newKeepControl.documento.dao.DocumentoDaoInterface;
import com.tcc.newKeepControl.documento.entity.dto.DocumentoDto;
import com.tcc.sqlMaker.mapped.Restricao;

@Service("documentoMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class DocumentoMBImp extends GenericMBImp<DocumentoDto> implements
		DocumentoMBInterface {
	/**
	 * Metodo de injecao de dependencia do Spring, usado pela camada MB.
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114288">NKC-17-R1</a>
	 *
	 * @return DocumentoDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Override
	public DocumentoDaoInterface getDaoBean() {
		return (DocumentoDaoInterface) CustomApplicationContextAware
				.getBean("documentoDao");
	}

	/**
	 * Metodo que valida se existe um ou mais documentos cadastrados com o mesmo
	 * nrDocumento
	 * 
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114334">NKC-34-R4</a>
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114153">NKC-90-R10</a>
	 *
	 * @param nrDocumento
	 *            usado para fazer a selecao
	 * @param fkAmbiente
	 *            usado para fazer a selecao
	 *
	 * @return DocumentoDaoInterface instancia retirada do pool de objetos do
	 *         Spring
	 */
	@Override
	public boolean validarUnicoNrDocumentoNoAmbiente(String nrDocumento,
			Long fkAmbiente) {
		try {
			DocumentoDto dto = new DocumentoDto();
			dto.addRestricao(Restricao.equal("nrDocumento", nrDocumento));
			return super.contarRegistros(dto) == 0;
		} catch (Exception e) {
			// TODO Logger.error.internalMethod
			return false;
		}
	}
}