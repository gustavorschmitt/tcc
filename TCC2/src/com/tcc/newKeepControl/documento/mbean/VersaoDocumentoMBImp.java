package com.tcc.newKeepControl.documento.mbean;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.exception.SemAcessoException;
import com.tcc.generic.mbean.GenericMBImp;
import com.tcc.newKeepControl.documento.dao.VersaoDocumentoDaoInterface;
import com.tcc.newKeepControl.documento.entity.dto.VersaoDocumentoDto;

@Service("versaoDocumentoMB")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class VersaoDocumentoMBImp extends GenericMBImp<VersaoDocumentoDto>
		implements VersaoDocumentoMBInterface {

	@Override
	public VersaoDocumentoDaoInterface getDaoBean() {
		return (VersaoDocumentoDaoInterface) CustomApplicationContextAware
				.getBean("versaoDocumentoDao");
	}

	@Override
	public VersaoDocumentoDto recuperar(Long fkVersaoDocumento)
			throws SemAcessoException {
		VersaoDocumentoDto dto = new VersaoDocumentoDto(fkVersaoDocumento);
		dto.setFields("*", "documento.*");
		dto = super.recuperar(dto);
		if (dto == null)
			throw new SemAcessoException();

		return dto;
	}

}