package com.tcc.newKeepControl.servicePage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tcc.newKeepControl.documento.entity.dto.DocumentoDto;
import com.tcc.newKeepControl.documento.entity.dto.VersaoDocumentoDto;

@RestController
@RequestMapping("/documento")
public class DocumentoEditarServicePage {

	@RequestMapping(value = { "/novo" }, method = RequestMethod.GET)
	public ModelAndView novo(HttpSession session, HttpServletRequest request) {

		ModelAndView modelAndView = new ModelAndView(
				"documento/documentoEditar");
		VersaoDocumentoDto versaoDocumento = new VersaoDocumentoDto();
		versaoDocumento.setDocumento(new DocumentoDto());
		modelAndView.addObject("versaoDocumento", versaoDocumento);
		return modelAndView;

	}

	// @PreAuthorize("@JSPSecurity.teste3()")
	// @RequestMapping(value = "/salvar", method = { RequestMethod.POST })
	// public @ResponseBody VersaoDocumentoJSON save(
	// HttpSession session,
	// HttpServletRequest request,
	// @PathVariable String idGuia,
	// @ModelAttribute("versaoDocumento") VersaoDocumentoDto versaoDocumento) {
	// Set<MensagemSistema> listMsg = new HashSet<MensagemSistema>();
	//
	// MBUtils.getDocumentoMB().salvar(entity);
	// REGISTRADOR.cadastrar(session, versaoDocumento.getCdId());
	// StringBuilder msgRetorno = new StringBuilder("<table>");
	//
	// for (MensagemSistema sm : listMsg) {
	// if (sm.isExpandirColuna())
	// msgRetorno
	// .append("<tr class=\"msg" + sm.getTipo().toString()
	// + "\"><td>").append(sm.getResumo())
	// .append("</td></tr>");
	// else
	// msgRetorno
	// .append("<tr class=\"msg" + sm.getTipo().toString()
	// + "\"><td>").append(sm.getResumo())
	// .append(":</td><td>").append(sm.getDetalhes())
	// .append("</td></tr>");
	// }
	//
	// msgRetorno.append("</table>");
	// return new VersaoDocumentoJSON(msgRetorno.toString(), versaoDocumento);
	// }
	//
	// @PreAuthorize("@JSPSecurity.teste3()")
	// @RequestMapping(value = "/salvar2", method = { RequestMethod.POST })
	// public @ResponseBody VersaoDocumentoJSON save2(
	// HttpSession session,
	// HttpServletRequest request,
	// @PathVariable String idGuia,
	// @ModelAttribute("versaoDocumento") VersaoDocumentoDto versaoDocumento) {
	// Set<MensagemSistema> listMsg = new HashSet<MensagemSistema>();
	// versaoDocumento.setFkAmbiente(JSPUtils.getFkAmbienteDaGuia(request,
	// idGuia));
	// versaoDocumento.getDocumento().setFkAmbiente(
	// JSPUtils.getFkAmbienteDaGuia(request, idGuia));
	//
	// if (Validator.isValid(versaoDocumento.getCdId())
	// && Validator.isValid(versaoDocumento.getDocumento().getCdId()))
	// versaoDocumento = DocumentoService.salvarEditar(session, listMsg,
	// versaoDocumento);
	// else if (!Validator.isValid(versaoDocumento.getCdId())
	// && !Validator.isValid(versaoDocumento.getDocumento().getCdId()))
	// versaoDocumento = DocumentoService.salvarNovo(session, listMsg,
	// versaoDocumento, request.getParameter("idTempPath"));
	// else if (!Validator.isValid(versaoDocumento.getCdId())
	// && Validator.isValid(versaoDocumento.getDocumento().getCdId()))
	// versaoDocumento = DocumentoService.salvarNovaVersao(session,
	// listMsg, versaoDocumento,
	// request.getParameter("idTempPath"));
	//
	// StringBuilder msgRetorno = new StringBuilder("<table>");
	//
	// for (MensagemSistema sm : listMsg) {
	// if (sm.isExpandirColuna())
	// msgRetorno
	// .append("<tr class=\"msg" + sm.getTipo().toString()
	// + "\"><td>").append(sm.getResumo())
	// .append("</td></tr>");
	// else
	// msgRetorno
	// .append("<tr class=\"msg" + sm.getTipo().toString()
	// + "\"><td>").append(sm.getResumo())
	// .append(":</td><td>").append(sm.getDetalhes())
	// .append("</td></tr>");
	// }
	//
	// msgRetorno.append("</table>");
	// return new VersaoDocumentoJSON(msgRetorno.toString(), versaoDocumento);
	// }
}