package com.tcc.newKeepControl.servicePage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.controleAcesso.ControleAcesso;
import com.tcc.generic.exception.SemAcessoException;
import com.tcc.newKeepControl.documento.entity.dto.DocumentoDto;
import com.tcc.newKeepControl.documento.mbean.DocumentoMBInterface;
import com.tcc.security.GenericService;

@RestController
@RequestMapping("/documento")
public class DocumentoVisualizarController extends GenericService {

	@RequestMapping(value = { "/visualizar/{cdId}" })
	public ModelAndView visualizar(HttpSession session, HttpServletRequest request, @PathVariable Long cdId) {

		ModelAndView modelAndView = new ModelAndView();

		try {
			DocumentoDto documento = ((DocumentoMBInterface) CustomApplicationContextAware.getBean("documentoMB")).recuperar(cdId);

			if (ControleAcesso.temPermissaoVisualizarDocumento("Gustavo", documento.getNmTitulo())) {
				modelAndView.setViewName("documento/documentoVisualizar");
				modelAndView.addObject("versaoDocumento", documento);
			} else {
				modelAndView.setViewName("paginaDeMensagem");
				modelAndView.addObject("msg", "Permiss�o negada");
			}
		} catch (SemAcessoException e) {
			modelAndView.setViewName("paginaDeMensagem");
			modelAndView.addObject("msg", "erro");
		}

		return modelAndView;
	}
}