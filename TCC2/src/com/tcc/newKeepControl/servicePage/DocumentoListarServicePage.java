package com.tcc.newKeepControl.servicePage;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tcc.config.CustomMessageSource;
import com.tcc.generic.utils.MBUtils;
import com.tcc.newKeepControl.documento.entity.dto.DocumentoDto;

@RestController
@RequestMapping("/documento")
public class DocumentoListarServicePage {

	@RequestMapping(value = { "", "/" })
	public ModelAndView listar(HttpSession session, HttpServletRequest request)
			throws Exception {

		ModelAndView modelAndView = new ModelAndView(
				"documento/documentoListar", "msg",
				CustomMessageSource.resolverMensagem("sistema_violacaoAcesso"));

		List<DocumentoDto> listDocumento = MBUtils.getDocumentoMB().listar(
				new DocumentoDto());

		modelAndView.addObject("versaoDocumentos", listDocumento);
		return modelAndView;
	}
}