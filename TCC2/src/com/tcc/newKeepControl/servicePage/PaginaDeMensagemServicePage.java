package com.tcc.newKeepControl.servicePage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tcc.config.CustomMessageSource;

@RestController
public class PaginaDeMensagemServicePage {
	// NKC-96-R10, NKC-96-R11, NKC-125

	// NKC-128-R1, NKC-131-R7
	@RequestMapping(value = { "/paginaMensagem", "/{idGuia}/paginaMensagem" })
	public String paginaMensagem(ModelAndView model, HttpSession session,
			HttpServletRequest request, @RequestParam String chaveMsg,
			@RequestParam(required = false) String chaveMsgDetalhes) {
		String msg = chaveMsg != null ? CustomMessageSource
				.resolverMensagem(chaveMsg) : "";
		String detalhes = chaveMsgDetalhes != null ? CustomMessageSource
				.resolverMensagem(chaveMsgDetalhes) : "";

		return "<table> <tr><td>" + msg + "<td></tr><tr>" + detalhes
				+ "</tr></table>";
	}
}