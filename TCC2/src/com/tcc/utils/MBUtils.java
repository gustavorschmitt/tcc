package com.tcc.utils;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.newKeepControl.documento.mbean.DocumentoMBInterface;
import com.tcc.usuario.mbean.UsuarioMBInterface;

public class MBUtils {

	public static DocumentoMBInterface getDocumentoMB() {
		return (DocumentoMBInterface) CustomApplicationContextAware
				.getBean("documentoMB");
	}

	public static UsuarioMBInterface getUsuarioMB() {
		return (UsuarioMBInterface) CustomApplicationContextAware
				.getBean("usuarioMB");
	}

}