package com.tcc.RESTful.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tcc.config.CustomMessageSource;
import com.tcc.generic.utils.JSPUtils;
import com.tcc.generic.utils.MBUtils;
import com.tcc.usuario.entity.dto.UsuarioDto;

@RestController
@RequestMapping(value = "")
public class IndexService {
	@RequestMapping(value = { "", "/" }, method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView index(HttpSession session) {

		return index(session, null);
	}

	@RequestMapping(value = { "{idGuia}", "{idGuia}/" }, method = {
			RequestMethod.GET, RequestMethod.POST })
	public ModelAndView index(HttpSession session, @PathVariable String idGuia) {
		try {
			UsuarioDto usuario = new UsuarioDto();
			usuario.addOrdem("flSituacao");
			usuario.addOrdem("nmUsuario");
			List<UsuarioDto> listUsuarios = MBUtils.getUsuarioMB().listar(
					usuario);
//			JSPUtils.ordenar(listUsuarios, Arrays.asList("flSituacaoString"));


			return new ModelAndView("index", "listUsuarios", listUsuarios);
		} catch (Exception e) {
			e.printStackTrace();
			return new ModelAndView("paginaDeMensagem", "msg",
					CustomMessageSource
							.resolverMensagem("sistema_violacaoAcesso"));
		}
	}
}