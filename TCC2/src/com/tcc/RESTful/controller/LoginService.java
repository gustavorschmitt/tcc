package com.tcc.RESTful.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.tcc.config.CustomApplicationContextAware;
import com.tcc.generic.exception.SemAcessoException;
import com.tcc.generic.utils.JSPUtils;
import com.tcc.generic.utils.MBUtils;
import com.tcc.security.AuthenticationProviderImpl;
import com.tcc.usuario.entity.dto.UsuarioDto;

@RestController
public class LoginService {

	@Autowired
	private AuthenticationProviderImpl authenticationProviderImpl;

	@RequestMapping(value = "/login", method = { RequestMethod.GET,
			RequestMethod.POST })
	public ModelAndView index(HttpSession session) {
		try {
			CustomApplicationContextAware.getBean("authProviderImpl");

			UsuarioDto usuario = new UsuarioDto();
			usuario.addOrdem("flSituacao");
			usuario.addOrdem("nmUsuario");
			List<UsuarioDto> listUsuarios = MBUtils.getUsuarioMB().listar(
					usuario);
//			JSPUtils.ordenar(listUsuarios, Arrays.asList("flSituacaoString"));

			return new ModelAndView("login", "listUsuarios", listUsuarios);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

		// return index(session, null);
	}

	@RequestMapping(value = "/logar/{cdId}", method = { RequestMethod.GET,
			RequestMethod.POST })
	public void logar(HttpSession session, @PathVariable Long cdId)
			throws SemAcessoException {
		System.out.println(cdId);

		UsuarioDto usuario = MBUtils.getUsuarioMB().recuperar(cdId);

		// Collection<GrantedAuthority> authorities = new
		// ArrayList<GrantedAuthority>();
		// authorities.add(new GrantedAuthorityImpl("ROLE_USER"));
		//
		// UserDetails user = new User("Gustavo", "Gustavo", authorities);
		//
		// Authentication authentication = new
		// UsernamePasswordAuthenticationToken(user, user.getPassword(),
		// user.getAuthorities());
		// SecurityContextHolder.getContext().setAuthentication(authentication);

		try {
			Authentication request = new UsernamePasswordAuthenticationToken(
					usuario.getDsEmail(), usuario.getDsSenha());
			Authentication result = authenticationProviderImpl
					.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);

		} catch (AuthenticationException e) {
			System.out.println("Authentication failed: " + e.getMessage());
		}

		// Collection<GrantedAuthority> authorities =
		// (Collection<GrantedAuthority>)
		// SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		// authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		// SecurityContextHolder.getContext().getAuthentication().setAuthenticated(true);
	}

	@RequestMapping(value = "/logar/", method = { RequestMethod.GET,
			RequestMethod.POST })
	public void logarForm(HttpSession session, @RequestParam String user,
			@RequestParam String pass) throws SemAcessoException {
		try {
			Authentication request = new UsernamePasswordAuthenticationToken(
					user, pass);
			Authentication result = authenticationProviderImpl
					.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);

		} catch (AuthenticationException e) {
			System.out.println("Authentication failed: " + e.getMessage());
		}

		// Collection<GrantedAuthority> authorities =
		// (Collection<GrantedAuthority>)
		// SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		// authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		// SecurityContextHolder.getContext().getAuthentication().setAuthenticated(true);
	}

	private void doAutoLogin(String username, String password,
			HttpServletRequest request) {

	}

	public AuthenticationProviderImpl getAuthenticationProviderImpl() {
		return authenticationProviderImpl;
	}

	public void setAuthenticationProviderImpl(
			AuthenticationProviderImpl authenticationProviderImpl) {
		this.authenticationProviderImpl = authenticationProviderImpl;
	}

}
//
// class SampleAuthenticationManager implements AuthenticationManager
// {
// static final List<GrantedAuthority> AUTHORITIES = new
// ArrayList<GrantedAuthority>();
//
// static
// {
// AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_USER"));
// }
//
// public Authentication authenticate(Authentication auth)
// throws AuthenticationException
// {
// UsuarioDto usuarioDto = new UsuarioDto();
// usuarioDto.setDsEmail(auth.getName());
// UsuarioDto usuario = null;
// try
// {
// usuario = MBUtils.getUsuarioMB().recuperar(usuarioDto);
// }
// catch(SemAcessoException e)
// {
// System.out.println("Login invalido");
// e.printStackTrace();
// }
// if(usuario.getDsSenha().equals(CreateSha.SHA2((String)
// auth.getCredentials())))
// {
// return new UsernamePasswordAuthenticationToken(auth.getName(),
// auth.getCredentials());
// }
// throw new BadCredentialsException("Bad Credentials");
// }
// }
