package com.tcc.sqlMaker.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface JoinMultiplo {
	String foreignId1();

	String foreignKey1();

	String foreignId2();

	String foreignKey2();
}