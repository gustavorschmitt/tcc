package com.tcc.sqlMaker.type;

public enum SortOrder {
	ASCENDING, DESCENDING
}
