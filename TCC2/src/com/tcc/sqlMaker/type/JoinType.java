package com.tcc.sqlMaker.type;

public enum JoinType {
	INNER, LEFT, RIGHT, FULL
}
