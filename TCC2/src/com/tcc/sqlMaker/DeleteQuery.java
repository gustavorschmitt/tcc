package com.tcc.sqlMaker;

import com.tcc.sqlMaker.entity.EntitySqlMaker;

public class DeleteQuery extends Query {
	public static String create(EntitySqlMaker<?> bean) throws Exception {
		montarTables(bean, false);
		montarColunas(bean, false);
		montarWhere(bean, bean, "");
		montarRestricoes(bean, false);

		StringBuilder query = new StringBuilder("DELETE ");

		FROM(query, bean, false);
		WHERE(query, bean);
		return query.toString();
	}
}