package com.tcc.sqlMaker.mapped;

import com.tcc.sqlMaker.type.FunctionType;

public class ColumnMapped {
	private String nmColuna;

	private String nmColunaAux;

	private String pathAtributo;

	private String pathAtributoAux;

	private String pathEntity;

	private String pathEntityAux;

	private String nmAlias;

	private FunctionType functionType = FunctionType.NONE;

	public String getNmColuna() {
		return nmColuna;
	}

	public void setNmColuna(String nmColuna) {
		this.nmColuna = nmColuna;
	}

	public String getPathAtributo() {
		return pathAtributo;
	}

	public void setPathAtributo(String pathAtributo) {
		this.pathAtributo = pathAtributo;
	}

	public String getNmAlias() {
		return nmAlias;
	}

	public void setNmAlias(String nmAlias) {
		this.nmAlias = nmAlias;
	}

	public String getPathEntity() {
		return pathEntity;
	}

	public void setPathEntity(String pathEntity) {
		this.pathEntity = pathEntity;
	}

	public String getNmColunaAux() {
		return nmColunaAux;
	}

	public void setNmColunaAux(String nmColunaAux) {
		this.nmColunaAux = nmColunaAux;
	}

	public String getPathAtributoAux() {
		return pathAtributoAux;
	}

	public void setPathAtributoAux(String pathAtributoAux) {
		this.pathAtributoAux = pathAtributoAux;
	}

	public String getPathEntityAux() {
		return pathEntityAux;
	}

	public void setPathEntityAux(String pathEntityAux) {
		this.pathEntityAux = pathEntityAux;
	}

	public FunctionType getFunctionType() {
		return functionType;
	}

	public void setFunctionType(FunctionType functionType) {
		this.functionType = functionType;
	}

	public boolean equals(Object obj) {
		if (obj != null
				&& this.pathAtributo.equals(((ColumnMapped) obj)
						.getPathAtributo()))
			return true;
		return false;
	}
}