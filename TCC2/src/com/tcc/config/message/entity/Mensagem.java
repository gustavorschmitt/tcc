package com.tcc.config.message.entity;

import java.io.Serializable;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Table;

// Se alterar o endereco do banco para esta tabela, deve ser alterado no appContex.xml tambem
/**
 * @see <a
 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737049">Mensagem</a>
 */
@Table(schema = "public", nome = "mensagem")
public class Mensagem extends GenericBean<Mensagem> implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Atributos da tabela
	 * ******************************************************
	 * ********************
	 * ******************************************************
	 * *****************************************
	 */

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "ds_chave", atributo = "dsChave", message = "entity_message_dsKey")
	private String dsChave;

	@Column(coluna = "ds_item_pt_br", atributo = "dsItemPtBr", message = "entity_message_dsItemPtBr")
	private String dsItemPtBr;

	@Column(coluna = "ds_item_en_us", atributo = "dsItemEnUs", message = "entity_message_dsItemEnUs")
	private String dsItemEnUs;

	@Column(coluna = "ds_item_es", atributo = "dsItemEs", message = "entity_message_dsItemEs")
	private String dsItemEs;

	@Column(coluna = "ds_item_fr_fr", atributo = "dsItemFrFr", message = "entity_message_dsItemFrFr")
	private String dsItemFrFr;

	/*
	 * Atributos de projecoes
	 * ***************************************************
	 * ***********************
	 * ***************************************************
	 * *****************************************
	 */

	/*
	 * Construtor
	 * ***************************************************************
	 * ***********
	 * ***************************************************************
	 * *****************************************
	 */
	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public Mensagem() {
	}

	/*
	 * Gets and Sets
	 * ************************************************************
	 * **************
	 * ************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	@Override
	public Long getCdId() {
		return cdId;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	@Override
	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public String getDsChave() {
		return dsChave;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public void setDsChave(String dsChave) {
		this.dsChave = dsChave;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public String getDsItemPtBr() {
		return dsItemPtBr;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public void setDsItemPtBr(String dsItemPtBr) {
		this.dsItemPtBr = dsItemPtBr;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public String getDsItemEnUs() {
		return dsItemEnUs;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public void setDsItemEnUs(String dsItemEnUs) {
		this.dsItemEnUs = dsItemEnUs;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public String getDsItemEs() {
		return dsItemEs;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public void setDsItemEs(String dsItemEs) {
		this.dsItemEs = dsItemEs;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public String getDsItemFrFr() {
		return dsItemFrFr;
	}

	/**
	 * @see <a href="http://192.168.200.2:8080/jira/browse/NKC-24">NKC-24</a>
	 */
	public void setDsItemFrFr(String dsItemFrFr) {
		this.dsItemFrFr = dsItemFrFr;
	}
}