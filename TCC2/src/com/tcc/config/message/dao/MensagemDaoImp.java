package com.tcc.config.message.dao;

import org.springframework.stereotype.Repository;

import com.tcc.config.message.entity.dto.MensagemDto;
import com.tcc.generic.dao.GenericDaoImp;

@Repository("mensagemDao")
public class MensagemDaoImp extends GenericDaoImp<MensagemDto> implements
		MensagemDaoInterface {
}