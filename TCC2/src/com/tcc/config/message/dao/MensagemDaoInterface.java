package com.tcc.config.message.dao;

import com.tcc.config.message.entity.dto.MensagemDto;
import com.tcc.generic.dao.GenericDaoInterface;

public interface MensagemDaoInterface extends GenericDaoInterface<MensagemDto> {
}