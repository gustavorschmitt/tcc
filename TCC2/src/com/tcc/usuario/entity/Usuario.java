package com.tcc.usuario.entity;

import java.io.Serializable;

import com.tcc.generic.entity.GenericBean;
import com.tcc.sqlMaker.annotations.Column;
import com.tcc.sqlMaker.annotations.Table;

/**
 * @see <a
 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114322">Usuario</a>
 */
@Table(schema = "public", nome = "usuario")
public class Usuario extends GenericBean<Usuario> implements Serializable {
	private static final long serialVersionUID = 1L;

	/*
	 * Atributos da tabela
	 * ******************************************************
	 * ********************
	 * ******************************************************
	 * *****************************************
	 */

	@Column(id = true, coluna = "cd_id", atributo = "cdId", updatable = false)
	private Long cdId;

	@Column(coluna = "nm_usuario", atributo = "nmUsuario", message = "entity_usuario_nmUsuario", maxLength = 254, minLength = 3)
	private String nmUsuario;

	@Column(coluna = "ds_email", atributo = "dsEmail", message = "entity_usuario_dsEmail", maxLength = 254, mascara = "email", minLength = 5)
	private String dsEmail;

	@Column(coluna = "ds_senha", atributo = "dsSenha", message = "entity_usuario_dsSenha", minLength = 5, maxLength = 256)
	private String dsSenha;

	@Column(coluna = "fl_situacao", atributo = "flSituacao", message = "sistema_entity_flSituacao")
	private Integer flSituacao;

	/*
	 * Referencias cruzadas
	 * *****************************************************
	 * *********************
	 * *****************************************************
	 * *****************************************
	 */

	/*
	 * Atributos de projecoes
	 * *s*************************************************
	 * ***********************
	 * ***************************************************
	 * ******************************************
	 */

	// usada para montar a 'lista' de grupos do ambiente que o usuario faz parte
	// (nkc-131-r9)
	private String listGrupoAmbienteString;

	/*
	 * Construtor
	 * ***************************************************************
	 * ***********
	 * ***************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4-R4</a>
	 */
	public Usuario() {
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1114250">NKC-99-R7</a>
	 */
	public Usuario(Long cdId) {
		setCdId(cdId);
	}

	/*
	 * Gets and Sets
	 * ************************************************************
	 * **************
	 * ************************************************************
	 * *****************************************
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	@Override
	public Long getCdId() {
		return cdId;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	@Override
	public void setCdId(Long cdId) {
		this.cdId = cdId;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public String getDsSenha() {
		return dsSenha;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public void setDsSenha(String dsSenha) {
		this.dsSenha = dsSenha;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public String getNmUsuario() {
		return nmUsuario;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public void setNmUsuario(String nmUsuario) {
		this.nmUsuario = nmUsuario;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public String getDsEmail() {
		return dsEmail;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */
	public void setDsEmail(String dsEmail) {
		this.dsEmail = dsEmail;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1736762">NKC-55-R4</a>
	 */

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=622641">NKC-93-R7</a>
	 */
	public Integer getFlSituacao() {
		return flSituacao;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=622641">NKC-93-R7</a>
	 */
	public void setFlSituacao(Integer flSituacao) {
		this.flSituacao = flSituacao;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737237">NKC-131-R9</a>
	 */
	public String getListgrupoambientestring() {
		return listGrupoAmbienteString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737237">NKC-131-R9</a>
	 */
	public void setListgrupoambientestring(String listGrupoAmbienteString) {
		this.listGrupoAmbienteString = listGrupoAmbienteString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737237">NKC-131-R9</a>
	 */
	public String getListGrupoAmbienteString() {
		return listGrupoAmbienteString;
	}

	/**
	 * @see <a
	 *      href="http://192.168.200.2:8080/confluence-5.5.3/pages/viewpage.action?pageId=1737237">NKC-131-R9</a>
	 */
	public void setListGrupoAmbienteString(String listGrupoAmbienteString) {
		this.listGrupoAmbienteString = listGrupoAmbienteString;
	}
}