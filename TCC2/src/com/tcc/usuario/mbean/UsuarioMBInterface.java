package com.tcc.usuario.mbean;

import com.tcc.generic.mbean.GenericMBInterface;
import com.tcc.usuario.entity.dto.UsuarioDto;

public interface UsuarioMBInterface extends GenericMBInterface<UsuarioDto> {

}