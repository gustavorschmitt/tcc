package com.tcc.usuario.dao;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Repository;

import com.tcc.generic.dao.GenericDaoImp;
import com.tcc.usuario.entity.dto.UsuarioDto;

@Repository("usuarioDao")
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public class UsuarioDaoImp extends GenericDaoImp<UsuarioDto> implements
		UsuarioDaoInterface {
}