package com.tcc.usuario.service;

import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;

import com.tcc.usuario.entity.dto.UsuarioDto;

public class UsuarioService {
	// NKC-93-R2
	public static void novo(HttpSession session, ModelAndView modelAndView) {
		UsuarioDto usuarioDto = new UsuarioDto();
		modelAndView.addObject("usuario", usuarioDto);
	}

}