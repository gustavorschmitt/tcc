package com.tcc.controleAcesso;

import java.util.HashMap;

import com.tcc.RESTful.utils.FAKE_SESSION;

public class ControleAcesso {

	public static boolean temPermissaoVisualizarDocumento(String nmUsuario, String nmTitulo) {
		boolean decision = false;
		try {

			HashMap<String, String> hashMap = new HashMap<String, String>();
			hashMap.put("nmUsuario", nmUsuario);
			hashMap.put("nmDocumento", nmTitulo);

			long currentTimeMillis1 = System.currentTimeMillis();
			String sendGet = ConexaoHTTP.sendGet(FAKE_SESSION.URL_PEP + "/temPermissaoVisualizar", hashMap);
			long currentTimeMillis2 = System.currentTimeMillis();
			System.out.println("Tempo Total" + ((currentTimeMillis2 - currentTimeMillis1)*0.001));

			decision = sendGet != null && sendGet.equals("true") ? true : false;

			return decision;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return decision;
	}

}
