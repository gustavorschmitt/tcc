CREATE TABLE documento
(
cd_id bigserial,
nr_documento varchar,
nm_titulo varchar,

CONSTRAINT fk_documento PRIMARY KEY (cd_id)
);


CREATE TABLE usuario
(
cd_id bigserial,
nm_usuario varchar,
ds_email varchar,
ds_senha varchar,
fl_situacao integer,

CONSTRAINT fk_usuario PRIMARY KEY (cd_id)
);

ALTER TABLE usuario add column historico bigint;

CREATE TABLE peso
(
cd_id bigserial NOT NULL, 
nm_peso varchar (150) NOT NULL,
nr_peso smallint NOT NULL,
ds_identificador varchar(150) NOT NULL,
constraint fk_peso primary key (cd_id)
);

INSERT INTO peso (nm_peso, nr_peso, ds_identificador) VALUES ('risco de contexto', 4,'risco_contexto');
INSERT INTO peso (nm_peso, nr_peso, ds_identificador) VALUES ('risco de confidencialidade, integridade e disponibilidade', 4 ,'risco_confidencialidadeIntegridadeDisponibilidade');
INSERT INTO peso (nm_peso, nr_peso, ds_identificador) VALUES ('risco do historico', 2, 'risco_historico');


CREATE TABLE config
(
cd_id bigserial NOT NULL, 
nm_config varchar (150) NOT NULL,
nr_config smallint NOT NULL,
ds_identificador varchar(150) NOT NULL,
constraint fk_config primary key (cd_id)
);


insert into config (nm_config, nr_config, ds_identificador)  values ('Avaliar risco', 1 ,'avaliarRisco');
insert into config (nm_config, nr_config, ds_identificador)  values ('Algoritmo decisao', 1 ,'algoritmoDecisao');


CREATE TABLE politica
(
cd_id bigserial NOT NULL, 
nm_politica varchar (150),
nr_politica smallint,
ds_identificador varchar(150) ,
tx_politica text ,
nr_risco_aceitavel smallint ,
fl_requer_verificacao boolean ,
nr_necessidade_operacional smallint ,
constraint fk_politica primary key (cd_id)
);

INSERT INTO politica VALUES (nextval('peso_cd_id_seq'), 'politica acesso a documento', 1, 'acessoDocumento', '<?xml version="1.0" encoding="UTF-8"?>
<Policy
      xmlns="urn:oasis:names:tc:xacml:2.0:policy:schema:os"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="urn:oasis:names:tc:xacml:2.0:policy:schema:os
        access_control-xacml-2.0-policy-schema-os.xsd"
      PolicyId="urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:policy"
      RuleCombiningAlgId="urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:deny-overrides">
    <Description>
       </Description>
    <Target/>
    <Rule
          RuleId="urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:rule"
          Effect="Permit">
        <Description>
        </Description>
        <Target>
            <Subjects>
                <Subject>
                    <SubjectMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Gustavo</AttributeValue>
                        <SubjectAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </SubjectMatch>
                </Subject>
            </Subjects>
            <Resources>
                <Resource>
                    <ResourceMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Documento Estrutural</AttributeValue>
                        <ResourceAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </ResourceMatch>
                </Resource>
            </Resources>
            <Actions>
                <Action>
                    <ActionMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Visualizar</AttributeValue>
                        <ActionAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </ActionMatch>
                </Action>
                <Action>
                    <ActionMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Listar</AttributeValue>
                        <ActionAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </ActionMatch>
                </Action>
            </Actions>
        </Target>
    </Rule>
</Policy>
', 50, true, 50);

alter table politica add column fl_necessidade_sobrescrever boolean;

/********************************************************************************************************************************************************************************************
 * 
 * MODELO
 * 
 *******************************************************************************************************************************************************************************************/

insert into fator values (nextval('fator_cd_id_seq'), 'Characteristics of Requester', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Role', 2.7, null , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Rank', 2.7, null , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Clearance Level', 2.7, null , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Access Level', 2.7, null , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Previous Violations', 2.7, null , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Education Level', 2.7, null , 1 , 9);
	
insert into fator values (nextval('fator_cd_id_seq'), 'Characteristics of IT Components', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Machine Type', 2.3, null , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Application', 2.3, null , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Connection Type', 2.3, null , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Authentication Type', 2.3, null , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Network', 2.3, null , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'QoP/Encryption Level', 2.3, null , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Distance from requester to source', 2.3, null , 2 , 9);

insert into fator values (nextval('fator_cd_id_seq'), 'Heuristics', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Risk Knowledge', 8.3, null , 3 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Trust Level', 8.3, null , 3 , 9);

insert into fator values (nextval('fator_cd_id_seq'), 'Situational Factors', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Specific Mission Role', 3.3, null , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Time Sensitivity of Information', 3.3, null , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Transaction Type', 3.3, null , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Auditable or Non-auditable', 3.3, null , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Audience Size', 3.3, null , 4 , 9);

insert into fator values (nextval('fator_cd_id_seq'), 'Environment Factors', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Current Location', 8.3, null , 5 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Operational Environment Threat Level', 8.3, null , 5 , 9);
	
insert into fator values (nextval('fator_cd_id_seq'), 'Characteristics of Information Requested', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Classification Level', 3.3, null , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Encryption Level', 3.3, null , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Network Classification Level', 3.3, null , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Permission Level', 3.3, null , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Perishable', 3.3, null , 6 , 9);
	
	
	
	
	
	
/********************************************************************************************************************************************************************************************
 * 
 * POLITICA
 * 
 *******************************************************************************************************************************************************************************************/
insert into fator values (nextval('fator_cd_id_seq'), 'Characteristics of Requester', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Role', 2.7, 7 , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Rank', 2.7, 8 , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Clearance Level', 2.7, 4 , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Access Level', 2.7, 10 , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Previous Violations', 2.7, 0 , 1 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Education Level', 2.7, 2 , 1 , 9);
	
insert into fator values (nextval('fator_cd_id_seq'), 'Characteristics of IT Components', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Machine Type', 2.3, 10 , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Application', 2.3, 4 , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Connection Type', 2.3, 8 , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Authentication Type', 2.3, 7 , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Network', 2.3, 9 , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'QoP/Encryption Level', 2.3, 5 , 2 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Distance from requester to source', 2.3, 8 , 2 , 9);

insert into fator values (nextval('fator_cd_id_seq'), 'Heuristics', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Risk Knowledge', 8.3, 10 , 3 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Trust Level', 8.3, 9 , 3 , 9);

insert into fator values (nextval('fator_cd_id_seq'), 'Situational Factors', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Specific Mission Role', 3.3, 2 , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Time Sensitivity of Information', 3.3, 2 , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Transaction Type', 3.3, 2 , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Auditable or Non-auditable', 3.3, 2 , 4 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Audience Size', 3.3, 2 , 4 , 9);

insert into fator values (nextval('fator_cd_id_seq'), 'Environment Factors', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Current Location', 8.3, 10 , 5 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Operational Environment Threat Level', 8.3, 10 , 5 , 9);
	
insert into fator values (nextval('fator_cd_id_seq'), 'Characteristics of Information Requested', 16.6 ,null ,null , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Classification Level', 3.3, 10 , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Encryption Level', 3.3, 1 , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Network Classification Level', 3.3, 9 , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Permission Level', 3.3, 9 , 6 , 9);
	insert into fator values (nextval('fator_cd_id_seq'), 'Perishable', 3.3, 9 , 6 , 9);	
	
	
	
update politica set tx_politica = '<?xml version="1.0" encoding="UTF-8"?>
<Policy
      xmlns="urn:oasis:names:tc:xacml:2.0:policy:schema:os"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="urn:oasis:names:tc:xacml:2.0:policy:schema:os
        access_control-xacml-2.0-policy-schema-os.xsd"
      PolicyId="urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:policy"
      RuleCombiningAlgId="urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:deny-overrides">
    <Description>acessoDocumento</Description>
    <Target/>
    <Rule
          RuleId="urn:oasis:names:tc:xacml:2.0:conformance-test:IIA1:rule"
          Effect="Permit">
        <Description>
        </Description>
        <Target>
            <Subjects>
                <Subject>
                    <SubjectMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Gustavo</AttributeValue>
                        <SubjectAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:subject:subject-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </SubjectMatch>
                </Subject>
            </Subjects>
            <Resources>
                <Resource>
                    <ResourceMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Documento Estrutural</AttributeValue>
                        <ResourceAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:resource:resource-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </ResourceMatch>
                </Resource>
            </Resources>
            <Actions>
                <Action>
                    <ActionMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Visualizar</AttributeValue>
                        <ActionAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </ActionMatch>
                </Action>
                <Action>
                    <ActionMatch
                          MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
                        <AttributeValue
                              DataType="http://www.w3.org/2001/XMLSchema#string">Listar</AttributeValue>
                        <ActionAttributeDesignator
                              AttributeId="urn:oasis:names:tc:xacml:1.0:action:action-id"
                              DataType="http://www.w3.org/2001/XMLSchema#string"/>
                    </ActionMatch>
                </Action>
            </Actions>
        </Target>
    </Rule>
</Policy>'
	


	
/********************************************************************************************************************************************************************************************
 * 
 * CASO DE USO 1
 * 
 *******************************************************************************************************************************************************************************************/
update peso set nr_peso = 5 where ds_identificador = 'risco_contexto';
update peso set nr_peso = 3 where ds_identificador = 'risco_confidencialidadeIntegridadeDisponibilidade';
update peso set nr_peso = 2 where ds_identificador = 'risco_historico';

update config set nr_config = 1 where ds_identificador = 'algoritmoDecisao' ;

update usuario set historico = 6;

