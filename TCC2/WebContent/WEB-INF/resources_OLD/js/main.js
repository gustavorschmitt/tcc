function getUrl(url) {
	return window.location.protocol + "//" + window.location.host + "/" + window.location.pathname.split('/')[1] + "/" + url;
}

//NKC-71-R4
function validarModificacoesVersaoDocumento(elementForm)
{
	return elementForm.elements["documento.nmTitulo"].value != elementForm.elements["documento.nmTitulo_temp"].value ||
	       elementForm.elements["nrVersao"].value != elementForm.elements["nrVersao_temp"].value ||
		   elementForm.elements["documento.nrDocumento"].value != elementForm.elements["documento.nrDocumento_temp"].value;
}

// NKC-71-R4
function validarModificacoesComentario(elementForm)
{
	return elementForm.elements["dsComentario"].value != elementForm.elements["dsComentario_temp"].value;
}