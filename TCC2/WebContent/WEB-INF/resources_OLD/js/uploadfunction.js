$(function() {
	var progressbar = $("#progressbar"), progressLabel = $(".progress-label");

	progressbar.progressbar({
		value : false,
		change : function() {
			progressLabel.text(progressbar.progressbar("value") + "%");
			window.document.getElementById('progressingBar').style.width = progressbar.progressbar("value") + "%";
		},
		complete : function() {
			progressLabel.text("Processando");
		}
	});

	$('#fileupload').fileupload(
			{
				dataType : 'json',
				formAcceptCharset : 'UTF-8',
				limitMultiFileUploads : 100 * 1024,

				add : function(e, data) {
					var count = 0;
					if (data.files[0]['size'] && data.files[0]['size'] > 104857600) {
						count++;
						// document.getElementById('msg').innerHTML = 'teste';

						$("#msg").append($('<div/>').html('<table><tr class=\"msgERROR\"><td>'+erro_anexoExcedeTamanhoLimite+'</td></tr></table>'));
					}
					if (count == 0) {
						data.submit();
					}
				},
				
				fail : function(e, data) {
					// $("#uploaded-files-error tr:has(td)").remove();
					$.each(data.result, function(index, arquivoJSON) {
						$("#uploaded-files-error").append($('<tr/>').append($('<td />').text(arquivoJSON.nmArquivo)).append($('<td />').text(arquivoJSON.mensagem)))
					});
				},
				start : function(e, data) {
					window.document.getElementById('progressbar').style.display = 'block';
				},
				done : function(e, data) {
					if (data.result.length == 0) {
						window.location.href = getUrl('paginaMensagem?chaveMsg=sistema_violacaoAcesso');
					}
					if(window.document.getElementById('uploaded-files').summary)
					{
						document.getElementById('uploaded-files').style.display = 'table';
						document.getElementById('uploaded-files').style.width = '100%'
					}
					$("#uploaded-files tr:has(td)").remove();
					$.each(data.result, function(index, arquivoJSON) {

						$("#uploaded-files").append($('<tr/>')
//							.append($('<td />').html('<a href="'+getUrl('arquivo/download/versaoDocumento/0/'+file.arquivo.cdId)+'">'+file.arquivo.nmArquivo+'</a>'))
							.append($('<td />').text(arquivoJSON.nmArquivo))
							.append($('<td />').text(arquivoJSON.nrTamanhoMbString + ' MB'))
							.append($('<td />').text(arquivoJSON.dtCriacaoString))
						// .append($('<td style="width:15%;"/>').html("<a
						// href='"+getUrl('arquivo/download/'+index)+"'>Click</a>"))
						)
					});
					window.document.getElementById('progressbar').style.display = 'none';
				},

				progressall : function(e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					progressbar.progressbar("value", progress);
				},

				dropZone : $('#dropzone')
			});
});