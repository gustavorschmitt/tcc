<% /* NKC-137 */ %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<%@ include file="/WEB-INF/jsp/_fragment/head.jspf"%>
<title><spring:eval expression="@messageSource.resolverMensagem('titulo_nmProduto')" /></title>

<style>
.conteudo-interna{margin-top: 180px;margin-bottom: 40px;}
</style>
</head>
<body>
	<header>
		<%@ include file="/WEB-INF/jsp/_fragment/header.jspf"%>
		
		<div class="clearfix"></div>
		<div class="container">
			<h3 class="page-header">
				<strong><spring:eval expression="@messageSource.resolverMensagem('titulo_notificacoesAmbiente')" /></strong>
				<span class="total">(12 de 50)</span>
				<small>
					<ul class="pull-right list-unstyled list-inline" id="filter">
						<li><a class="formatHref ${titulo_notificacao_naoLidas}" href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/notificacao/naoLidas"><spring:eval expression="@messageSource.resolverMensagem('titulo_notificacao_naoLidas')" /></a> | </li>
						<li><a class="formatHref ${titulo_notificacao_importantes}" href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/notificacao/importantes"><spring:eval expression="@messageSource.resolverMensagem('titulo_notificacao_importantes')" /></a> | </li>
						<li><a class="formatHref ${titulo_notificacao_geradasPorMim}" href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/notificacao/geradasPorMim"><spring:eval expression="@messageSource.resolverMensagem('titulo_notificacao_geradasPorMim')" /></a> | </li>
						<li><a class="formatHref ${titulo_notificacao_todas}" href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/notificacao/todas"><spring:eval expression="@messageSource.resolverMensagem('titulo_notificacao_todas')" /></a></li>
					</ul>
				</small>
			</h3>
		</div>
	</header>
	
	<div class="container conteudo-interna">
		<H5 id="dicas">
			<spring:eval expression="@messageSource.resolverMensagem('titulo_dicas')" /><br />
			${msg_dicas}
		</H5>
		
		<table class="table table-striped" id="lista">
			<thead class="lista-head">
				<tr>
					<th style="width:60px;text-align: center;">
						<c:choose>
							<c:when test="${titulo_notificacao_geradasPorMim == null}">
								<div class="dropdown acoes" onmouseover="$('#acoesEmLote').attr('aria-expanded', 'true');$('#acoesEmLote').parent().addClass('open');" onmouseout="$('#acoesEmLote').attr('aria-expanded', 'false');$('#acoesEmLote').parent().removeClass('open');">
									<a id="acoesEmLote" href="#" data-toggle="dropdown"><i class="fa fa-bars" style="color:white;"></i></a>
									<ul class="dropdown-menu dropdown-menu-left" role="menu" aria-labelledby="acoesEmLote">
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#" onclick="marcarTodosComoLido();" style="color:black;"><spring:eval expression="@messageSource.resolverMensagem('titulo_marcarTodasComoLidas')" /></a></li>
									</ul>
								</div>
							</c:when>
						</c:choose>
					</th>
					<th style="min-width: 175px;">
						<spring:eval expression="@messageSource.resolverMensagem('titulo_origem')" /><i class="fa fa-sort pull-right"></i><br/>
						<small><spring:eval expression="@messageSource.resolverMensagem('titulo_quem')" /> <spring:eval expression="@messageSource.resolverMensagem('titulo_e')" /> <spring:eval expression="@messageSource.resolverMensagem('titulo_quando')" /> <spring:eval expression="@messageSource.resolverMensagem('titulo_gerou')" /></small>
					</th>
					<c:choose>
						<c:when test="${titulo_notificacao_naoLidas == null}">
							<th style="min-width: 175px;">
								<spring:eval expression="@messageSource.resolverMensagem('titulo_destino')" /><i class="fa fa-sort pull-right"></i><br/>
								<small><spring:eval expression="@messageSource.resolverMensagem('titulo_quem')" /> <spring:eval expression="@messageSource.resolverMensagem('titulo_e')" /> <spring:eval expression="@messageSource.resolverMensagem('titulo_quando')" /> <spring:eval expression="@messageSource.resolverMensagem('titulo_leu')" /></small>
							</th>
						</c:when>
					</c:choose>
					<th style="width:125px"><spring:eval expression="@messageSource.resolverMensagem('titulo_relativaA')" /><i class="fa fa-sort pull-right"></i></th>
					<th><spring:eval expression="@messageSource.resolverMensagem('titulo_acaoDaOrigem')" /><i class="fa fa-sort pull-right"></i></th>
					<th style="min-width: 200px;"><spring:eval expression="@messageSource.resolverMensagem('titulo_emQualDocumento')" /><i class="fa fa-sort pull-right"></i></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${listaNotificacoes != null && listaNotificacoes.size() > 0}">
						<c:forEach var="notificacao" items="${listaNotificacoes}">
							<tr class="status4 item ${notificacao.fkNotificante != fkUsuarioLogado && notificacao.notificacaoUsuario.dtLido == null ? 'bold' : ''}">
								<td>
									<c:choose>
										<c:when test="${notificacao.fkNotificante != fkUsuarioLogado && notificacao.notificacaoUsuario.dtLido == null}">
											<a id="envelope_${notificacao.notificacaoUsuario.cdId}" href="#" onclick="marcarComoLida(${notificacao.notificacaoUsuario.cdId})" ><i class="fa fa-envelope"></i></a>
										</c:when>
									</c:choose>
									<c:choose>
										<c:when test="${notificacao.fkNotificante == fkUsuarioLogado}">
											<a id="flag_notificacao_${notificacao.cdId}" href="#" onclick="marcarComoImportante('notificacao', ${notificacao.cdId});" class="${notificacao.flImportante ? 'importante' : ''}"><i class="fa fa-flag"></i></a>
										</c:when>
										<c:otherwise>
											<a id="flag_notificacaoUsuario_${notificacao.notificacaoUsuario.cdId}" href="#" onclick="marcarComoImportante('notificacaoUsuario', ${notificacao.notificacaoUsuario.cdId});" class="${notificacao.notificacaoUsuario.flImportante ? 'importante' : ''}"><i class="fa fa-flag"></i></a>
										</c:otherwise>
									</c:choose>
								</td>
								<td>
									${notificacao.notificante.nmUsuario} <br/>
									${notificacao.dtCadastramentoString}
								</td>
								<c:choose>
									<c:when test="${titulo_notificacao_naoLidas == null}">
										<td>
											${notificacao.notificacaoUsuario.notificado.nmUsuario} <br/>
											<c:choose>
												<c:when test="${notificacao.fkNotificante != fkUsuarioLogado}">
													<div id="dtLido_${notificacao.notificacaoUsuario.cdId}">${notificacao.notificacaoUsuario.dtLidoString}</div>
												</c:when>
											</c:choose>
										</td>
									</c:when>
								</c:choose>
								<td>
									${notificacao.nrEntidadeNotificadaString}
								</td>
								<td>
									${notificacao.getAcaoDaOrigem()}
								</td>
								<td>
									${notificacao.getEmQualDocumento(pageContext)}
								</td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr class="status4 item">
							<td colspan="6">${msg_lista_vazia}</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<div id="button-more">
			<a href="index2.html">Load more items</a>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/jsp/_fragment/footer.jspf"%>
	<script>
		var id;
		function marcarComoLida(cdId) {
            $.ajax({
                type: 'POST',
                url: getUrl('notificacao/marcarComoLida/'+cdId), 
                success: function(retorno) {
                	$("#envelope_"+cdId).css( "display", "none" );
                	$("#envelope_"+cdId).parent().parent().removeClass("bold");
                	$("#dtLido_"+cdId).text(retorno);
                	
                	$("#nrNotificacao").html($("#nrNotificacao").html()-1);
                	if($("#nrNotificacao").html() == 0) {
                		$("#nrNotificacao").css("display", "none");
                		$('.fa-bell').css('margin-left', '9px');
                	}
                },
                error: function (data) {
                    alert('error: ' + data);
                }
            });
            return false;
        }
		
		function marcarTodosComoLido()
		{
			var retorno = confirm('<spring:eval expression="@messageSource.resolverMensagem('confirmacao_marcarTodasComoLidas')" />'); 
			if(retorno) {
				$.ajax({
	                type: 'POST',
	                url: getUrl('notificacao/marcarTodasComoLida'), 
	                success: function(retorno) {
                		$("tr.item").each(function(i, row) {
                			var $row = $(row);
                			$row.removeClass("bold");
                			$("#"+$row.find('td:first').find('a:first').attr('id')).css('display', 'none');
                		});
                		
                		for(var i=0; i < retorno.length;i++)
                			$("#dtLido_"+retorno[i].cdId).text(retorno[i].dtLidoString);
                		
                		$("#nrNotificacao").css("display", "none");
                		$('.fa-bell').css('margin-left', '9px');
	                },
	                error: function (data) {
	                    alert('error: ' + data);
	                }
	            });
			}
			return false;
		}
		
		function marcarComoImportante(tipo, cdId) {
            $.ajax({
                type: 'POST',
                url: getUrl('notificacao/marcarImportante/'+tipo+'/'+cdId), 
                success: function(retorno) {
                	if($( "#flag_"+tipo+"_"+cdId ).hasClass( "importante" ))
                		$( "#flag_"+tipo+"_"+cdId ).removeClass( "importante" );
               		else
               			$( "#flag_"+tipo+"_"+cdId ).addClass( "importante" );
                	
                	if(${titulo_notificacao_importantes != null})
						$("#flag_"+tipo+"_"+cdId).parent().parent().remove();
                	
                	if($('#lista').children()[1].children.length == 0)
                		$('#lista tr:last').after('<tr class="status4 item"><td colspan="6">${msg_lista_vazia}</td></tr>');
                },
                error: function (data) {
                    alert('error: ' + data);
                }
            });
            return false;
        }
	</script>
</body>
</html>