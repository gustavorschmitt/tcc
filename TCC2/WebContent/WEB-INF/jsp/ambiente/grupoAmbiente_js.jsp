<% /* NKC-131-R1 */ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script>
	$(document).ready(function() {
        $('#grupoAtualizar').submit(function() {
            $.ajax({
                data: $(this ).serialize(),
                type: $(this).attr('method'),
                url: $(this).attr('action'), 
                success: function(mensagem) {
                	window.document.getElementById('msg').innerHTML = mensagem;
                },
                error: function (data) {
                	window.document.getElementById('msg').innerHTML = data;
                }
            });
            return false;
        });
    });
</script>