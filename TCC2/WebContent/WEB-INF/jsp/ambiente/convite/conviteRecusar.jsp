<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
<head>
	<title>Convites</title>

</head>
<body> 
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div id="msg">
					${msg}
					<table id="msgTable"></table>
				</div>
				<h2>${h2}<br/></h2>
				${labelRecusar}<br/>
				${labelFornecerJustificativa}<br/>
				<form id="conviteEditar" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/convite/recusar/salvar" method="post">
					<input type="hidden" name="hash" value="${hash}"> 
					<textarea id="dsConvite" name="dsConvite" rows="10" cols="137" placeholder="Digite aqui sua justificativa">${respostaConvite.dsObservacao}</textarea><br/>
					<input type="checkbox" name="bloquear" value="true"/><spring:eval expression="@messageSource.resolverMensagem('titulo_naoDesejoReceberConvitesAmbiente')" /><br/>
					<div style="text-align: center; margin-top: 1%; margin-left: 11%;"><input type="submit" value="Recusar convite" /></div>
				</form>
				<span style="margin-left: 15%;"><a href="${urlAceitar}"><spring:eval expression="@messageSource.resolverMensagem('titulo_mudarOpiniaoAceitarConvite')" /></a></span>
			</div>
		</div>
	</div>
</body>
</html>