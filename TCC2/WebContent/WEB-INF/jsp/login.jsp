<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
    <head>
    
    	
        <title>Pagina inicial</title>
       
    </head>

    <body>
		<div>
			Lista de usuarios <br />
            <c:forEach var="usuario" items="${listUsuarios}">
            	<a href="<%= JSPUtils.getEnderecoUrl(pageContext) %>/logar/${usuario.cdId}">${usuario.nmUsuario} - ${usuario.flSituacaoString} - ${usuario.dsEmail} -</a> <br />
            </c:forEach>
            
         <form action="<%=JSPUtils.getEnderecoUrl(pageContext) %>/logar/" method="post">
		    <input type="text" name="user" placeholder="Username">
		    <input type="password" name="pass" placeholder="Password">
		    <input type="submit" name="login" value="login">
 		 </form>
        </div>
    </body>
</html>