<% /* UC3-R2, UC3-R3, UC3-R6, 
      UC4-R2, UC4-R3, UC4-R6,
      UC5-R2, UC5-R3, UC5-R6,
      NKC-71-R2, NKC-71-R3, NKC-71-R4, NKC-71-R7,
      NKC-84-R1, NKC-84-R2, NKC-84-R3, NKC-87-R1, NKC-102-R1 */ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
	<%@ include file="/WEB-INF/jsp/_fragment/head.jspf"%>
	<title>
	Documento
	</title>
</head>
<body
	
>

	
	
	<div class="container conteudo-interna">
		<div class="row">
			<div class="col-sm-8">
				<h3 class="page-header"><strong>
					Cadastrar Documento
				</strong></h3>
				
				<div id="msg">
					${msg}
					<table id="msgTable"></table>
				</div>
				
				<form id="documentoEditar" class="formatHref" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/documento/salvar" method="post">
					<input type="hidden" id="idTempPath" name="idTempPath" value="${idTempPath}" />
					<input type="hidden" id="cdId" name="cdId" value="${versaoDocumento.cdId}" />
					<input type="hidden" id="documento.cdId" name="documento.cdId" value="${versaoDocumento.documento.cdId}" />
					
					<input type="hidden" id="documento.nmTitulo_temp" value="${versaoDocumento.documento.nmTitulo}" />
					<input type="hidden" id="nrVersao_temp" value="${versaoDocumento.nrVersao}" />
					<input type="hidden" id="documento.nrDocumento_temp" value="${versaoDocumento.documento.nrDocumento}" />
					
					<div class="form-group">
						<label>Titulo</label>
						<input id="nmTitulo" name="nmTitulo" value="${versaoDocumento.documento.nmTitulo}" type="text" class="form-control" placeholder="Título">
					</div>
					
					<div class="form-group" style="display:none;">
						<label>Descrição</label> <!-- TODO i18n -->
						<div class="ewrapper">
							<div class="toolbar" style="display:none">
								<div class="btn-group"> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="bold" title="CTRL+B"><i class="fa fa-bold"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="italic" title="CTRL+I"><i class="fa fa-italic"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="underline" title="CTRL+U"><i class="fa fa-underline"></i></a> </div>
								<div class="btn-group"> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="insertUnorderedList" title="Unordered list"><i class="fa fa-list-ul"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="insertOrderedList" title="Ordered list"><i class="fa fa-list-ol"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="Outdent" title="Outdent"><i class="fa fa-outdent"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="Indent" title="Indent"><i class="fa fa-indent"></i></a> </div>
								<div class="btn-group"> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="createLink" title="Criar Link"><i class="fa fa-link"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="removeLink" title="Remover link"><i class="fa fa-unlink"></i></a> <a tabindex="-1" class="btn  btn-sm btn-default" data-wysihtml5-command="insertImage" title="Inserir Imagem"><i class="fa fa-image"></i></a> </div>
								<div data-wysihtml5-dialog="createLink" style="display: none;">
									<div class="input-group">
										<input type="text" class="form-control input-sm" data-wysihtml5-dialog-field="href" value="http://">
										<div class="input-group-btn"> <a tabindex="-1" class="btn btn-sm btn-default" data-wysihtml5-dialog-action="save">OK</a> <a tabindex="-1" class="btn btn-sm btn-default" data-wysihtml5-dialog-action="cancel">Cancelar</a> </div>
									</div>
								</div>
								<div data-wysihtml5-dialog="insertImage" style="display: none;">
									<div class="input-group">
										<input type="text" class="form-control input-sm" data-wysihtml5-dialog-field="src" value="http://">
										<div class="input-group-btn"> <a tabindex="-1" class="btn btn-sm btn-default" data-wysihtml5-dialog-action="save">OK</a> <a tabindex="-1" class="btn btn-sm btn-default" data-wysihtml5-dialog-action="cancel">Cancelar</a> </div>
									</div>
								</div>
							</div>
							<textarea class="form-control textarea margin-bottom20" rows="5"></textarea>
						</div>
					</div>
					
					<div class="form-group">
						<div class="radio">
							<label style="width:100%">
								<input type="radio" name="nova-versao" value="nao" checked> Cadastrar documento pelo código<br> <!-- TODO i18n -->
								<input id="documento.nrDocumento" name="documento.nrDocumento" value="${versaoDocumento.documento.nrDocumento}" class="form-control" type="text" placeholder="Código Documento"/>
							</label>
						</div>
						<div class="radio" style="display:none;">
							<label style="width:100%">
								<input type="radio" name="nova-versao" value="sim"> Cadastrar o documento pelos campos da norma<br> <!-- TODO i18n -->
								<div class="nova-versao-ativo" style="display:none;">
									<div class="form-group">
										<label class="no-padding">Natureza</label>
										<select class="form-control chosen">
											<option value="1">Natureza 1</option>
											<option class="2">Natureza 2</option>
											<option value="3">Natureza 3</option>
										</select>
									</div>
									<div class="form-group">
										<label class="no-padding">Estrutura</label>
										<select class="chosen">
											<option value="1">Estrutura 1</option>
											<option class="2">Estrutura 2</option>
											<option value="3">Estrutura 3</option>
										</select>
									</div>
									<div class="form-group">
										<label class="no-padding">Área</label>
										<select class="chosen">
											<option value="1">Área 1</option>
											<option class="2">Área 2</option>
											<option value="3">Área 3</option>
										</select>
									</div>
									<div class="form-group">
										<label class="no-padding">Sub-Área</label>
										<select class="chosen">
											<option value="1">Sub-Área 1</option>
											<option class="2">Sub-Área 2</option>
											<option value="3">Sub-Área 3</option>
										</select>
									</div>
								</div>
							</label>
						</div>
					</div>

					<div style="margin-bottom: 4px">
						<label><br/><span style="font-size: 12px; font-weight: normal;"></span><br/></label>
					</div>
					<div class="btn-group" style="float: right;">
						<button type="submit" class="btn btn-default" id="salvar" onclick="javascript:exibirNotificacao = false;">Salvar</button>
						<button type="submit" class="btn btn-default" id="salvarFechar" onclick="javascript:exibirNotificacao = false;fecharAoSalvar = true;">Salvar e voltar</button>
						<button type="button" class="btn btn-default" id="cancelar" onclick="javascript:history.go(-1);">Voltar</button>
					</div>
				</form>
			</div>
			
			
		</div>
	</div>
	
	
	<%@ include file="documentoEditar_js.jspf" %>
</body>
</html>