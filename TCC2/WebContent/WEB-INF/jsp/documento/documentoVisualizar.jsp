<% /* UC2-R2, UC2-R3, UC4-R1, UC5-R7, UC6-R1, UC6-R2, UC7-R1, UC7-R2, UC7-R3, UC7-R8,
      NKC-71-R1, NKC-71-R2, NKC-71-R5, NKC-71-R7, NKC-87-R2, NKC-102-R1 */ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
	<%@ include file="/WEB-INF/jsp/_fragment/head.jspf"%>
	<title>${versaoDocumento.nrDocumento}</title>
</head>
<style>
tr {
	background-color: white !important;
}

</style>
<body>
	<div class="container conteudo-interna">
		<div class="row">
			<div class="col-sm-9">
				<h3>Documento</h3>
				<span>${msg}</span>
				<table>
					<tr>
						<td style="text-align: left;font-weight: bold;"><h4>${versaoDocumento.nrDocumento} - ${versaoDocumento}</h4></td>
					</tr>
					<tr>
						<td><h5>${versaoDocumento.nmTitulo}</h5></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</body>
</html>