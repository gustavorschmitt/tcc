<% /* UC6-R3, UC6-R4, UC6-R6,
      NKC-71-R2, NKC-71-R3, NKC-71-R4, NKC-71-R7 */ %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html>
<!--<![endif]-->
	<head>
		<%@ include file="/WEB-INF/jsp/_fragment/head_old.jspf" %>
		<title>
			<c:choose>
				<c:when test="${versaoDocumento.cdId == null}"><spring:eval expression="@messageSource.resolverMensagem('titulo_cadastrarDocumento')" /></c:when>
				<c:otherwise><spring:eval expression="@messageSource.resolverMensagem('titulo_editarDocumento')" /></c:otherwise>
			</c:choose>
		</title>
		<%@ include file="comentarioEditar_js.jspf" %>
		<script src="<c:url value="/js/main.js"/>"></script>
	</head>

	<body> 
		<%@ include file="/WEB-INF/jsp/_fragment/header_old.jspf" %>
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<h3><spring:eval expression="@messageSource.resolverMensagem('titulo_cadastrarComentario')" />
					</h3>
					<div id="msg">${msg}</div>
					<form id="comentarioEditar" class="formatHref" action="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/documento/comentario/salvar" method="post">
						<input type="hidden" id="fkVersaoDocumento" name="fkVersaoDocumento" value="${comentario.versaoDocumento.cdId}" />
			 			<input type="hidden" id="fkDocumento" name="fkDocumento" value="${comentario.versaoDocumento.fkDocumento}" />
						<input type="hidden" id="dsComentario_temp" value="${comentario.dsComentario}" />
						<input type="hidden" id="idTempPath" name="idTempPath" value="${idTempPath}" />
						
						<table class="table table-striped table-hover">
							<tr>
								<td><spring:eval expression="@messageSource.resolverMensagem('entity_documento_nmTitulo')" />:</td>
								<td>${comentario.versaoDocumento.documento.nmTitulo}</td>
							</tr>
							<tr>
								<td><spring:eval expression="@messageSource.resolverMensagem('entity_versaoDocumento_nrVersao')" />:</td>
								<td>${comentario.versaoDocumento.nrVersao}</td>
							</tr>
							<tr>
								<td><spring:eval expression="@messageSource.resolverMensagem('entity_documento_nrDocumento')" />:</td>
								<td>${comentario.versaoDocumento.documento.nrDocumento}</td>
							</tr>
							<tr>
								<td><spring:eval expression="@messageSource.resolverMensagem('sistema_campoObrigatorio')" /> <spring:eval expression="@messageSource.resolverMensagem('entity_comentario_dsComentario')" />:</td>
								<td><textarea id="dsComentario" name="dsComentario">${comentario.dsComentario}</textarea></td>
							</tr>
						</table>
						
						<div style="margin-bottom: 4px">
							<label><br/><span style="font-size: 12px; font-weight: normal;"><spring:eval expression="@messageSource.resolverMensagem('sistema_campoObrigatorio_detalhes')" /></span><br/></label>
						</div>
				
						<table style="width:100%;">
							<tr>
								<td style="text-align: right;">
									<input type="submit" value="Salvar" onclick="javascript:exibirNotificacao = false; window['voltar'] = false"/>
									<input type="submit" value="Salvar & Voltar" onclick="javascript:exibirNotificacao = false; window['voltar'] = true"/>
									<input type="button" value="Voltar" onclick="javascript:history.go(-1);"/>
								</td>
							</tr>
						</table>
					</form>
					
			<div style="width:100%;padding:20px">
			 	<form id="fileupload" class="formatHref" data-url="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/arquivoComentario/upload">
			 		<input type="hidden" name="idTempPath" value="${idTempPath}" />
			 		<input type="hidden" id="fkComentario" name="fkComentario" value="${comentario.cdId}" />
			 		<input type="hidden" id="fkVersaoDocumento" name="fkVersaoDocumento" value="${comentario.versaoDocumento.cdId}" />
			 		<input type="hidden" id="fkDocumento" name="fkDocumento" value="${comentario.versaoDocumento.fkDocumento}" />
			    	
			    	<span><spring:eval expression="@messageSource.resolverMensagem('titulo_anexarArquivoComentario')" /></span><br/>
			    	<span><small><spring:eval expression="@messageSource.resolverMensagem('titulo_anexarArquivo_detalhe')" /></small></span>
				 	<div id="dropzone" style="width:100%;height:50px; background-color: white; border: 1px solid black;">
				 		<input type="file" name="files[]" multiple />
					</div>
			 	</form>
				
				<div id="progressbar" class="progress" style="display:none;">
					<div class="progress-label">Enviando para o servidor</div>
					<div style="width: 100%; height: 20px;">
						<div id="progressingBar" class="ui-progressbar-overlay"style="width: 0%;"></div>
					</div>
	    		</div>
			    
			    <br />
			    <table id="uploaded-files-error" style="width:100%;">
			    	<tr></tr>
			    </table>
			    
			    <br/>
			    <table id="uploaded-files" style="width:100%;" summary="${listArquivoComentario != null && !listArquivoComentario.isEmpty()}">
			        <tr>
			            <th style="font-weight: bold;"><spring:eval expression="@messageSource.resolverMensagem('entity_arquivo_nmArquivo')" /></th>
			            <th style="width:110px;font-weight: bold;"><spring:eval expression="@messageSource.resolverMensagem('entity_arquivo_nrTamanho')" /></th>
			            <th style="width:140px;font-weight: bold;"><spring:eval expression="@messageSource.resolverMensagem('entity_arquivo_dtCriacao')" /></th>
			        </tr>
		        	<c:forEach var="avd" items="${listArquivoComentario}" >
						<tr id="adoc_${ac.cdId}">
							<!--<td style="text-align: left;"><span class="nmArquivo-doc"><a href="<%=JSPUtils.getEnderecoUrl(pageContext)%>/{idGuia}/arquivoVersaoDocumento/download/versaoDocumento/${versaoDocumento.cdId}/${avd.cdId}">${avd.arquivo.nmArquivo}</a></span></td>-->
							<td style="text-align: left;"><span class="nmArquivo-doc">${avd.arquivo.nmArquivo}</span></td>
							<td style="text-align: center;"><span class="nrTamanho-doc">${avd.arquivo.nrTamanhoMbString} MB</span></td>
							<td style="text-align: center;"><span class="dtCriacao-doc">${avd.arquivo.dtCriacaoString}</span></td>
						</tr>
					</c:forEach>
		   		</table>			
			</div>
				</div>
				
				<div class="col-sm-3"><%@ include file="/WEB-INF/jsp/_fragment/rightBar_document_old.jspf" %></div>
			</div>
		</div>
	</body>
</html>