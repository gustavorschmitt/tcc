<% /* UC3-R2, UC3-R3, UC3-R6, 
      UC4-R2, UC4-R3, UC4-R6,
      UC5-R2, UC5-R3, UC5-R6,
      NKC-71-R2, NKC-71-R3, NKC-71-R4, NKC-71-R7,
      NKC-84-R1, NKC-84-R2, NKC-84-R3, NKC-87-R1, NKC-102-R1 */ %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.tcc.generic.utils.JSPUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<%@ include file="/WEB-INF/jsp/_fragment/head.jspf"%>
<title>TCC</title>
</head>
<body>	

	<div class="container conteudo">
		<table class="table table-striped" id="lista">
			<thead class="lista-head">
				<tr>
					<th><a href="#">Titulo</a></th>
					<th width="100px;"><a href="#">Versão do documento</a></th>
					<th><a href="#">Código</a></th>
					<th><a href="#">Autor</a></th>
				</tr>
			</thead>
			<tbody>
				<c:choose>
					<c:when test="${versaoDocumentos != null && versaoDocumentos.size() > 0}">
						<c:forEach var="versaoDocumento" items="${versaoDocumentos}">
							<tr class="status4 item">
								<td>
									<span class="edit"><a class="formatHref titulo editavel" href="<%= JSPUtils.getEnderecoUrl(pageContext) %>/documento/visualizar/${versaoDocumento.cdId}" data-type="text"
										data-toggle="manual" href="#">${versaoDocumento.nmTitulo}</a> 
								</td>
								<td><span class="situacao">2A</span></td>
								<td><span class="situacao">${versaoDocumento.nrDocumento}</span></td>
								<td><span class="situacao">2A</span></td>
							</tr>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<tr class="status4 item">
							<td colspan="5"></td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>
		<div id="button-more">
			<a href="index2.html">Load more items</a>
		</div>
	</div>
	
	<%@ include file="/WEB-INF/jsp/_fragment/footer.jspf"%>
</body>
</html>